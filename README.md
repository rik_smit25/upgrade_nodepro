[![MEAN.JS Logo](http://meanjs.org/img/logo-small.png)](http://meanjs.org/)

[![Build Status](https://travis-ci.org/meanjs/mean.svg?branch=master)](https://travis-ci.org/meanjs/mean)
[![Dependencies Status](https://david-dm.org/meanjs/mean.svg)](https://david-dm.org/meanjs/mean)

MEAN.JS is a full-stack JavaScript open-source solution, which provides a solid starting point for [MongoDB](http://www.mongodb.org/), [Node.js](http://www.nodejs.org/), [Express](http://expressjs.com/), and [AngularJS](http://angularjs.org/) based applications. The idea is to solve the common issues with connecting those frameworks, build a robust framework to support daily development needs, and help developers use better practices while working with popular JavaScript components. 

## Before You Begin 
Before you begin we recommend you read about the basic building blocks that assemble a MEAN.JS application: 
* MongoDB - Go through [MongoDB Official Website](http://mongodb.org/) and proceed to their [Official Manual](http://docs.mongodb.org/manual/), which should help you understand NoSQL and MongoDB better.
* Express - The best way to understand express is through its [Official Website](http://expressjs.com/), which has a [Getting Started](http://expressjs.com/starter/installing.html) guide, as well as an [ExpressJS Guide](http://expressjs.com/guide/error-handling.html) guide for general express topics. You can also go through this [StackOverflow Thread](http://stackoverflow.com/questions/8144214/learning-express-for-node-js) for more resources.
* AngularJS - Angular's [Official Website](http://angularjs.org/) is a great starting point. You can also use [Thinkster Popular Guide](http://www.thinkster.io/), and the [Egghead Videos](https://egghead.io/).
* Node.js - Start by going through [Node.js Official Website](http://nodejs.org/) and this [StackOverflow Thread](http://stackoverflow.com/questions/2353818/how-do-i-get-started-with-node-js), which should get you going with the Node.js platform in no time.


## Prerequisites
Make sure you have installed all these prerequisites on your development machine.
* Node.js - [Download & Install Node.js](http://www.nodejs.org/download/) and the npm package manager, if you encounter any problems, you can also use this [GitHub Gist](https://gist.github.com/isaacs/579814) to install Node.js.
* MongoDB - [Download & Install MongoDB](http://www.mongodb.org/downloads), and make sure it's running on the default port (27017).
* Bower - You're going to use the [Bower Package Manager](http://bower.io/) to manage your front-end packages, in order to install it make sure you've installed Node.js and npm, then install bower globally using npm:

```
$ npm install -g bower
```

* Grunt - You're going to use the [Grunt Task Runner](http://gruntjs.com/) to automate your development process, in order to install it make sure you've installed Node.js and npm, then install grunt globally using npm:

```
$ sudo npm install -g grunt-cli
```

## Downloading MEAN.JS
There are several ways you can get the MEAN.JS boilerplate: 

### Yo Generator 
The recommended way would be to use the [Official Yo Generator](http://meanjs.org/generator.html) which will generate the latest stable copy of the MEAN.JS boilerplate and supplies multiple sub-generators to ease your daily development cycles.

### Cloning The GitHub Repository
You can also use Git to directly clone the MEAN.JS repository:
```
$ git clone https://github.com/meanjs/mean.git meanjs
```
This will clone the latest version of the MEAN.JS repository to a **meanjs** folder.

### Downloading The Repository Zip File
Another way to use the MEAN.JS boilerplate is to download a zip copy from the [master branch on GitHub](https://github.com/meanjs/mean/archive/master.zip). You can also do this using `wget` command:
```
$ wget https://github.com/meanjs/mean/archive/master.zip -O meanjs.zip; unzip meanjs.zip; rm meanjs.zip
```
Don't forget to rename **mean-master** after your project name.

## Quick Install
Once you've downloaded the boilerplate and installed all the prerequisites, you're just a few steps away from starting to develop you MEAN application.

The first thing you should do is install the Node.js dependencies. The boilerplate comes pre-bundled with a package.json file that contains the list of modules you need to start your application, to learn more about the modules installed visit the NPM & Package.json section.

To install Node.js dependencies you're going to use npm again, in the application folder run this in the command-line:

```
$ npm install
```

This command does a few things:
* First it will install the dependencies needed for the application to run.
* If you're running in a development environment, it will then also install development dependencies needed for testing and running your application.
* Finally, when the install process is over, npm will initiate a bower install command to install all the front-end modules needed for the application

## Running Your Application
After the install process is over, you'll be able to run your application using Grunt, just run grunt default task:

```
$ grunt
```

Your application should run on the 3000 port so in your browser just go to [http://localhost:3000](http://localhost:3000)
                            
That's it! your application should be running by now, to proceed with your development check the other sections in this documentation. 
If you encounter any problem try the Troubleshooting section.

## Development and deployment With Docker

* Install [Docker](http://www.docker.com/)
* Install [Fig](https://github.com/orchardup/fig)

* Local development and testing with fig: 
```bash
$ fig up
```

* Local development and testing with just Docker:
```bash
$ docker build -t mean .
$ docker run -p 27017:27017 -d --name db mongo
$ docker run -p 3000:3000 --link db:db_1 mean
$
```

* To enable live reload forward 35729 port and mount /app and /public as volumes:
```bash
$ docker run -p 3000:3000 -p 35729:35729 -v /Users/mdl/workspace/mean-stack/mean/public:/home/mean/public -v /Users/mdl/workspace/mean-stack/mean/app:/home/mean/app --link db:db_1 mean
```

## Running in a secure environment
To run your application in a secure manner you'll need to use OpenSSL and generate a set of self-signed certificates. Unix-based users can use the following commnad: 
```
$ sh generate-ssl-certs.sh
```
Windows users can follow instructions found [here](http://www.websense.com/support/article/kbarticle/How-to-use-OpenSSL-and-Microsoft-Certification-Authority)
To generate the key and certificate and place them in the *config/sslcert* folder.

## Getting Started With MEAN.JS
You have your application running but there are a lot of stuff to understand, we recommend you'll go over the [Official Documentation](http://meanjs.org/docs.html). 
In the docs we'll try to explain both general concepts of MEAN components and give you some guidelines to help you improve your development process. We tried covering as many aspects as possible, and will keep update it by your request, you can also help us develop the documentation better by checking out the *gh-pages* branch of this repository.

## Community
* Use to [Offical Website](http://meanjs.org) to learn about changes and the roadmap.
* Join #meanjs on freenode.
* Discuss it in the new [Google Group](https://groups.google.com/d/forum/meanjs)
* Ping us on [Twitter](http://twitter.com/meanjsorg) and [Facebook](http://facebook.com/meanjs)

## Live Example
Browse the live MEAN.JS example on [http://meanjs.herokuapp.com](http://meanjs.herokuapp.com).

## Credits
Inspired by the great work of [Madhusudhan Srinivasa](https://github.com/madhums/)
The MEAN name was coined by [Valeri Karpov](http://blog.mongodb.org/post/49262866911/the-mean-stack-mongodb-expressjs-angularjs-and)

## License
(The MIT License)

Permission is hereby granted, free of charge, to any person obtaining
a copy of this software and associated documentation files (the
'Software'), to deal in the Software without restriction, including
without limitation the rights to use, copy, modify, merge, publish,
distribute, sublicense, and/or sell copies of the Software, and to
permit persons to whom the Software is furnished to do so, subject to
the following conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.



Last login: Wed Jan 13 09:32:30 on ttys000
Danniss-Mac-mini:~ dannis_kenny1115$ cd /
Danniss-Mac-mini:/ dannis_kenny1115$ cd /Volumes/
Danniss-Mac-mini:Volumes dannis_kenny1115$ ls
Macintosh HD 2	Yosemit System
Danniss-Mac-mini:Volumes dannis_kenny1115$ cd Macintosh\ HD\ 2/
Danniss-Mac-mini:Macintosh HD 2 dannis_kenny1115$ ls
Programs	Sandroworks	Source
Danniss-Mac-mini:Macintosh HD 2 dannis_kenny1115$ cd Programs/wanstudio333/ref/Python-Idp-Proxy/
Danniss-Mac-mini:Python-Idp-Proxy dannis_kenny1115$ ls
Danniss-Mac-mini:Python-Idp-Proxy dannis_kenny1115$ touch investigation.txt
Danniss-Mac-mini:Python-Idp-Proxy dannis_kenny1115$ open -e investigation.txt 
Danniss-Mac-mini:Python-Idp-Proxy dannis_kenny1115$ clear




















Danniss-Mac-mini:Python-Idp-Proxy dannis_kenny1115$ ifconfig
lo0: flags=8049<UP,LOOPBACK,RUNNING,MULTICAST> mtu 16384
	options=3<RXCSUM,TXCSUM>
	inet6 ::1 prefixlen 128 
	inet 127.0.0.1 netmask 0xff000000 
	inet6 fe80::1%lo0 prefixlen 64 scopeid 0x1 
	nd6 options=1<PERFORMNUD>
gif0: flags=8010<POINTOPOINT,MULTICAST> mtu 1280
stf0: flags=0<> mtu 1280
en0: flags=8863<UP,BROADCAST,SMART,RUNNING,SIMPLEX,MULTICAST> mtu 1500
	options=10b<RXCSUM,TXCSUM,VLAN_HWTAGGING,AV>
	ether 68:5b:35:85:f0:29 
	inet6 fe80::6a5b:35ff:fe85:f029%en0 prefixlen 64 scopeid 0x4 
	inet 192.168.3.208 netmask 0xffffff00 broadcast 192.168.3.255
	nd6 options=1<PERFORMNUD>
	media: autoselect (100baseTX <full-duplex,flow-control>)
	status: active
en1: flags=8823<UP,BROADCAST,SMART,SIMPLEX,MULTICAST> mtu 1500
	ether 28:cf:e9:10:40:01 
	nd6 options=1<PERFORMNUD>
	media: autoselect (<unknown type>)
	status: inactive
en2: flags=963<UP,BROADCAST,SMART,RUNNING,PROMISC,SIMPLEX> mtu 1500
	options=60<TSO4,TSO6>
	ether 32:00:1a:d9:67:e0 
	media: autoselect <full-duplex>
	status: inactive
fw0: flags=8863<UP,BROADCAST,SMART,RUNNING,SIMPLEX,MULTICAST> mtu 4078
	lladdr 44:fb:42:ff:fe:ad:96:7e 
	nd6 options=1<PERFORMNUD>
	media: autoselect <full-duplex>
	status: inactive
p2p0: flags=8802<BROADCAST,SIMPLEX,MULTICAST> mtu 2304
	ether 0a:cf:e9:10:40:01 
	media: autoselect
	status: inactive
awdl0: flags=8902<BROADCAST,PROMISC,SIMPLEX,MULTICAST> mtu 1484
	ether b6:e5:34:7d:18:7c 
	nd6 options=1<PERFORMNUD>
	media: autoselect
	status: inactive
bridge0: flags=8863<UP,BROADCAST,SMART,RUNNING,SIMPLEX,MULTICAST> mtu 1500
	options=63<RXCSUM,TXCSUM,TSO4,TSO6>
	ether 6a:5b:35:58:26:00 
	Configuration:
		id 0:0:0:0:0:0 priority 0 hellotime 0 fwddelay 0
		maxage 0 holdcnt 0 proto stp maxaddr 100 timeout 1200
		root id 0:0:0:0:0:0 priority 0 ifcost 0 port 0
		ipfilter disabled flags 0x2
	member: en2 flags=3<LEARNING,DISCOVER>
	        ifmaxaddr 0 port 6 priority 0 path cost 0
	nd6 options=1<PERFORMNUD>
	media: <unknown type>
	status: inactive
ppp0: flags=8051<UP,POINTOPOINT,RUNNING,MULTICAST> mtu 1444
	inet 10.0.0.28 --> 10.0.0.1 netmask 0xff000000 
Danniss-Mac-mini:Python-Idp-Proxy dannis_kenny1115$ clear

Danniss-Mac-mini:Python-Idp-Proxy dannis_kenny1115$ 
Danniss-Mac-mini:Python-Idp-Proxy dannis_kenny1115$ 
Danniss-Mac-mini:Python-Idp-Proxy dannis_kenny1115$ 
Danniss-Mac-mini:Python-Idp-Proxy dannis_kenny1115$ 












































Danniss-Mac-mini:Python-Idp-Proxy dannis_kenny1115$ 
Danniss-Mac-mini:Python-Idp-Proxy dannis_kenny1115$ 
Danniss-Mac-mini:Python-Idp-Proxy dannis_kenny1115$ 
Danniss-Mac-mini:Python-Idp-Proxy dannis_kenny1115$ clear

Danniss-Mac-mini:Python-Idp-Proxy dannis_kenny1115$ ifconfig
lo0: flags=8049<UP,LOOPBACK,RUNNING,MULTICAST> mtu 16384
	options=3<RXCSUM,TXCSUM>
	inet6 ::1 prefixlen 128 
	inet 127.0.0.1 netmask 0xff000000 
	inet6 fe80::1%lo0 prefixlen 64 scopeid 0x1 
	nd6 options=1<PERFORMNUD>
gif0: flags=8010<POINTOPOINT,MULTICAST> mtu 1280
stf0: flags=0<> mtu 1280
en0: flags=8863<UP,BROADCAST,SMART,RUNNING,SIMPLEX,MULTICAST> mtu 1500
	options=10b<RXCSUM,TXCSUM,VLAN_HWTAGGING,AV>
	ether 68:5b:35:85:f0:29 
	inet6 fe80::6a5b:35ff:fe85:f029%en0 prefixlen 64 scopeid 0x4 
	inet 192.168.3.208 netmask 0xffffff00 broadcast 192.168.3.255
	nd6 options=1<PERFORMNUD>
	media: autoselect (100baseTX <full-duplex,flow-control>)
	status: active
en1: flags=8823<UP,BROADCAST,SMART,SIMPLEX,MULTICAST> mtu 1500
	ether 28:cf:e9:10:40:01 
	nd6 options=1<PERFORMNUD>
	media: autoselect (<unknown type>)
	status: inactive
en2: flags=963<UP,BROADCAST,SMART,RUNNING,PROMISC,SIMPLEX> mtu 1500
	options=60<TSO4,TSO6>
	ether 32:00:1a:d9:67:e0 
	media: autoselect <full-duplex>
	status: inactive
fw0: flags=8863<UP,BROADCAST,SMART,RUNNING,SIMPLEX,MULTICAST> mtu 4078
	lladdr 44:fb:42:ff:fe:ad:96:7e 
	nd6 options=1<PERFORMNUD>
	media: autoselect <full-duplex>
	status: inactive
p2p0: flags=8802<BROADCAST,SIMPLEX,MULTICAST> mtu 2304
	ether 0a:cf:e9:10:40:01 
	media: autoselect
	status: inactive
awdl0: flags=8902<BROADCAST,PROMISC,SIMPLEX,MULTICAST> mtu 1484
	ether b6:e5:34:7d:18:7c 
	nd6 options=1<PERFORMNUD>
	media: autoselect
	status: inactive
bridge0: flags=8863<UP,BROADCAST,SMART,RUNNING,SIMPLEX,MULTICAST> mtu 1500
	options=63<RXCSUM,TXCSUM,TSO4,TSO6>
	ether 6a:5b:35:58:26:00 
	Configuration:
		id 0:0:0:0:0:0 priority 0 hellotime 0 fwddelay 0
		maxage 0 holdcnt 0 proto stp maxaddr 100 timeout 1200
		root id 0:0:0:0:0:0 priority 0 ifcost 0 port 0
		ipfilter disabled flags 0x2
	member: en2 flags=3<LEARNING,DISCOVER>
	        ifmaxaddr 0 port 6 priority 0 path cost 0
	nd6 options=1<PERFORMNUD>
	media: <unknown type>
	status: inactive
ppp0: flags=8051<UP,POINTOPOINT,RUNNING,MULTICAST> mtu 1444
	inet 10.0.0.28 --> 10.0.0.1 netmask 0xff000000 
Danniss-Mac-mini:Python-Idp-Proxy dannis_kenny1115$ clear

Danniss-Mac-mini:Python-Idp-Proxy dannis_kenny1115$ 
Danniss-Mac-mini:Python-Idp-Proxy dannis_kenny1115$ 
Danniss-Mac-mini:Python-Idp-Proxy dannis_kenny1115$ 
Danniss-Mac-mini:Python-Idp-Proxy dannis_kenny1115$ 
Danniss-Mac-mini:Python-Idp-Proxy dannis_kenny1115$ clear

























Danniss-Mac-mini:Python-Idp-Proxy dannis_kenny1115$ 
Danniss-Mac-mini:Python-Idp-Proxy dannis_kenny1115$ 
Danniss-Mac-mini:Python-Idp-Proxy dannis_kenny1115$ 
Danniss-Mac-mini:Python-Idp-Proxy dannis_kenny1115$ clear


























Danniss-Mac-mini:Python-Idp-Proxy dannis_kenny1115$ 
Danniss-Mac-mini:Python-Idp-Proxy dannis_kenny1115$ 
Danniss-Mac-mini:Python-Idp-Proxy dannis_kenny1115$ pwd
/Volumes/Macintosh HD 2/Programs/wanstudio333/ref/Python-Idp-Proxy
Danniss-Mac-mini:Python-Idp-Proxy dannis_kenny1115$ clear

























Danniss-Mac-mini:Python-Idp-Proxy dannis_kenny1115$ pwd
/Volumes/Macintosh HD 2/Programs/wanstudio333/ref/Python-Idp-Proxy
Danniss-Mac-mini:Python-Idp-Proxy dannis_kenny1115$ cd ..
Danniss-Mac-mini:ref dannis_kenny1115$ cd ..
Danniss-Mac-mini:wanstudio333 dannis_kenny1115$ cd ..
Danniss-Mac-mini:Programs dannis_kenny1115$ ls
wanstudio333
Danniss-Mac-mini:Programs dannis_kenny1115$ cd ..
Danniss-Mac-mini:Macintosh HD 2 dannis_kenny1115$ ls
OJH		Programs	Sandroworks	Source
Danniss-Mac-mini:Macintosh HD 2 dannis_kenny1115$ clear



















Danniss-Mac-mini:Macintosh HD 2 dannis_kenny1115$ cd OJH/
Danniss-Mac-mini:OJH dannis_kenny1115$ clear




























Danniss-Mac-mini:OJH dannis_kenny1115$ pwd
/Volumes/Macintosh HD 2/OJH
Danniss-Mac-mini:OJH dannis_kenny1115$ ls
Danniss-Mac-mini:OJH dannis_kenny1115$ clear


























Danniss-Mac-mini:OJH dannis_kenny1115$ node -v
v5.4.1
Danniss-Mac-mini:OJH dannis_kenny1115$ npm -v
3.3.12
Danniss-Mac-mini:OJH dannis_kenny1115$ clear

























Danniss-Mac-mini:OJH dannis_kenny1115$ sudo npm install grunt-cli
Password:
/Volumes/Macintosh HD 2/OJH
└─┬ grunt-cli@0.1.13 
  ├─┬ findup-sync@0.1.3 
  │ ├─┬ glob@3.2.11 
  │ │ ├── inherits@2.0.1 
  │ │ └─┬ minimatch@0.3.0 
  │ │   ├── lru-cache@2.7.3 
  │ │   └── sigmund@1.0.1 
  │ └── lodash@2.4.2 
  ├─┬ nopt@1.0.10 
  │ └── abbrev@1.0.7 
  └── resolve@0.3.1 

npm WARN ENOENT ENOENT: no such file or directory, open '/Volumes/Macintosh HD 2/OJH/package.json'
npm WARN EPACKAGEJSON OJH No description
npm WARN EPACKAGEJSON OJH No repository field.
npm WARN EPACKAGEJSON OJH No README data
npm WARN EPACKAGEJSON OJH No license field.
Danniss-Mac-mini:OJH dannis_kenny1115$ sudo sudo npm install grunt-concurrent
/Volumes/Macintosh HD 2/OJH
└─┬ grunt-concurrent@2.1.0 
  ├── async@1.5.2 
  ├─┬ indent-string@2.1.0 
  │ └─┬ repeating@2.0.0 
  │   └─┬ is-finite@1.0.1 
  │     └── number-is-nan@1.0.0 
  └─┬ pad-stream@1.2.0 
    ├─┬ meow@3.7.0 
    │ ├─┬ camelcase-keys@2.0.0 
    │ │ └── camelcase@2.0.1 
    │ ├─┬ decamelize@1.1.2 
    │ │ └── escape-string-regexp@1.0.4 
    │ ├─┬ loud-rejection@1.2.0 
    │ │ └── signal-exit@2.1.2 
    │ ├── map-obj@1.0.1 
    │ ├── minimist@1.2.0 
    │ ├─┬ normalize-package-data@2.3.5 
    │ │ ├── hosted-git-info@2.1.4 
    │ │ ├─┬ is-builtin-module@1.0.0 
    │ │ │ └── builtin-modules@1.1.1 
    │ │ ├── semver@5.1.0 
    │ │ └─┬ validate-npm-package-license@3.0.1 
    │ │   ├─┬ spdx-correct@1.0.2 
    │ │   │ └── spdx-license-ids@1.2.0 
    │ │   └─┬ spdx-expression-parse@1.0.2 
    │ │     └── spdx-exceptions@1.0.4 
    │ ├── object-assign@4.0.1 
    │ ├─┬ read-pkg-up@1.0.1 
    │ │ ├─┬ find-up@1.1.0 
    │ │ │ ├── path-exists@2.1.0 
    │ │ │ └─┬ pinkie-promise@2.0.0 
    │ │ │   └── pinkie@2.0.1 
    │ │ └─┬ read-pkg@1.1.0 
    │ │   ├─┬ load-json-file@1.1.0 
    │ │   │ ├── graceful-fs@4.1.2 
    │ │   │ ├─┬ parse-json@2.2.0 
    │ │   │ │ └─┬ error-ex@1.3.0 
    │ │   │ │   └── is-arrayish@0.2.1 
    │ │   │ ├── pify@2.3.0 
    │ │   │ └─┬ strip-bom@2.0.0 
    │ │   │   └── is-utf8@0.2.1 
    │ │   └── path-type@1.1.0 
    │ ├─┬ redent@1.0.0 
    │ │ └─┬ strip-indent@1.0.1 
    │ │   └── get-stdin@4.0.1 
    │ └── trim-newlines@1.0.0 
    ├─┬ pumpify@1.3.3 
    │ ├─┬ duplexify@3.4.2 
    │ │ └── end-of-stream@1.0.0 
    │ └─┬ pump@1.0.1 
    │   ├── end-of-stream@1.1.0 
    │   └─┬ once@1.3.3 
    │     └── wrappy@1.0.1 
    ├── split2@1.1.1 
    └─┬ through2@2.0.0 
      ├─┬ readable-stream@2.0.5 
      │ ├── core-util-is@1.0.2 
      │ ├── isarray@0.0.1 
      │ ├── process-nextick-args@1.0.6 
      │ ├── string_decoder@0.10.31 
      │ └── util-deprecate@1.0.2 
      └── xtend@4.0.1 

npm WARN ENOENT ENOENT: no such file or directory, open '/Volumes/Macintosh HD 2/OJH/package.json'
npm WARN EPACKAGEJSON OJH No description
npm WARN EPACKAGEJSON OJH No repository field.
npm WARN EPACKAGEJSON OJH No README data
npm WARN EPACKAGEJSON OJH No license field.
Danniss-Mac-mini:OJH dannis_kenny1115$ sudo npm install grunt-contrib-concat
/Volumes/Macintosh HD 2/OJH
├── UNMET PEER DEPENDENCY grunt@>=0.4.0
└─┬ grunt-contrib-concat@0.5.1 
  ├─┬ chalk@0.5.1 
  │ ├── ansi-styles@1.1.0 
  │ ├─┬ has-ansi@0.1.0 
  │ │ └── ansi-regex@0.2.1 
  │ ├── strip-ansi@0.3.0 
  │ └── supports-color@0.2.0 
  └─┬ source-map@0.3.0 
    └── amdefine@1.0.0 

npm WARN ENOENT ENOENT: no such file or directory, open '/Volumes/Macintosh HD 2/OJH/package.json'
npm WARN EPEERINVALID grunt-contrib-concat@0.5.1 requires a peer of grunt@>=0.4.0 but none was installed.
npm WARN EPACKAGEJSON OJH No description
npm WARN EPACKAGEJSON OJH No repository field.
npm WARN EPACKAGEJSON OJH No README data
npm WARN EPACKAGEJSON OJH No license field.
Danniss-Mac-mini:OJH dannis_kenny1115$ sudo npm install grunt-contrib-csslint
/Volumes/Macintosh HD 2/OJH
├── UNMET PEER DEPENDENCY grunt@>=0.4.0
└─┬ grunt-contrib-csslint@0.5.0 
  ├─┬ chalk@1.1.1 
  │ ├── ansi-styles@2.1.0 
  │ ├─┬ has-ansi@2.0.0 
  │ │ └── ansi-regex@2.0.0 
  │ ├── strip-ansi@3.0.0 
  │ └── supports-color@2.0.0 
  ├─┬ csslint@0.10.0 
  │ └── parserlib@0.2.5 
  ├── lodash@3.10.1 
  └── strip-json-comments@1.0.4 

npm WARN ENOENT ENOENT: no such file or directory, open '/Volumes/Macintosh HD 2/OJH/package.json'
npm WARN EPEERINVALID grunt-contrib-concat@0.5.1 requires a peer of grunt@>=0.4.0 but none was installed.
npm WARN EPEERINVALID grunt-contrib-csslint@0.5.0 requires a peer of grunt@>=0.4.0 but none was installed.
npm WARN EPACKAGEJSON OJH No description
npm WARN EPACKAGEJSON OJH No repository field.
npm WARN EPACKAGEJSON OJH No README data
npm WARN EPACKAGEJSON OJH No license field.
Danniss-Mac-mini:OJH dannis_kenny1115$ sudo npm install grunt-contrib-cssmin
/Volumes/Macintosh HD 2/OJH
├── UNMET PEER DEPENDENCY grunt@>=0.4.0
├─┬ grunt-contrib-csslint@0.5.0
│ └─┬ chalk@1.1.1 
│   ├── ansi-styles@2.1.0 
│   ├─┬ has-ansi@2.0.0 
│   │ └── ansi-regex@2.0.0 
│   ├── strip-ansi@3.0.0 
│   └── supports-color@2.0.0 
└─┬ grunt-contrib-cssmin@0.14.0 
  ├─┬ chalk@1.1.1 
  │ ├── ansi-styles@2.1.0 
  │ ├─┬ has-ansi@2.0.0 
  │ │ └── ansi-regex@2.0.0 
  │ ├── strip-ansi@3.0.0 
  │ └── supports-color@2.0.0 
  ├─┬ clean-css@3.4.9 
  │ ├─┬ commander@2.8.1 
  │ │ └── graceful-readlink@1.0.1 
  │ └── source-map@0.4.4 
  └─┬ maxmin@1.1.0 
    ├─┬ chalk@1.1.1 
    │ ├── ansi-styles@2.1.0 
    │ ├─┬ has-ansi@2.0.0 
    │ │ └── ansi-regex@2.0.0 
    │ ├── strip-ansi@3.0.0 
    │ └── supports-color@2.0.0 
    ├── figures@1.4.0 
    ├─┬ gzip-size@1.0.0 
    │ ├─┬ browserify-zlib@0.1.4 
    │ │ └── pako@0.2.8 
    │ └─┬ concat-stream@1.5.1 
    │   └── typedarray@0.0.6 
    └── pretty-bytes@1.0.4 

npm WARN ENOENT ENOENT: no such file or directory, open '/Volumes/Macintosh HD 2/OJH/package.json'
npm WARN EPEERINVALID grunt-contrib-concat@0.5.1 requires a peer of grunt@>=0.4.0 but none was installed.
npm WARN EPEERINVALID grunt-contrib-csslint@0.5.0 requires a peer of grunt@>=0.4.0 but none was installed.
npm WARN EPEERINVALID grunt-contrib-cssmin@0.14.0 requires a peer of grunt@>=0.4.0 but none was installed.
npm WARN EPACKAGEJSON OJH No description
npm WARN EPACKAGEJSON OJH No repository field.
npm WARN EPACKAGEJSON OJH No README data
npm WARN EPACKAGEJSON OJH No license field.
Danniss-Mac-mini:OJH dannis_kenny1115$ sudo npm install grunt-contrib-jshint
/Volumes/Macintosh HD 2/OJH
├── UNMET PEER DEPENDENCY grunt@>=0.4.0
└─┬ grunt-contrib-jshint@0.11.3 
  ├── hooker@0.2.3 
  └─┬ jshint@2.8.0 
    ├── cli@0.6.6 
    ├─┬ console-browserify@1.1.0 
    │ └── date-now@0.1.4 
    ├── exit@0.1.2 
    ├─┬ htmlparser2@3.8.3 
    │ ├── domelementtype@1.3.0 
    │ ├── domhandler@2.3.0 
    │ ├─┬ domutils@1.5.1 
    │ │ └─┬ dom-serializer@0.1.0 
    │ │   ├── domelementtype@1.1.3 
    │ │   └── entities@1.1.1 
    │ ├── entities@1.0.0 
    │ └── readable-stream@1.1.13 
    ├── lodash@3.7.0 
    ├─┬ minimatch@2.0.10 
    │ └─┬ brace-expansion@1.1.2 
    │   ├── balanced-match@0.3.0 
    │   └── concat-map@0.0.1 
    └── shelljs@0.3.0 

npm WARN ENOENT ENOENT: no such file or directory, open '/Volumes/Macintosh HD 2/OJH/package.json'
npm WARN EPEERINVALID grunt-contrib-concat@0.5.1 requires a peer of grunt@>=0.4.0 but none was installed.
npm WARN EPEERINVALID grunt-contrib-csslint@0.5.0 requires a peer of grunt@>=0.4.0 but none was installed.
npm WARN EPEERINVALID grunt-contrib-cssmin@0.14.0 requires a peer of grunt@>=0.4.0 but none was installed.
npm WARN EPEERINVALID grunt-contrib-jshint@0.11.3 requires a peer of grunt@>=0.4.0 but none was installed.
npm WARN EPACKAGEJSON OJH No description
npm WARN EPACKAGEJSON OJH No repository field.
npm WARN EPACKAGEJSON OJH No README data
npm WARN EPACKAGEJSON OJH No license field.
Danniss-Mac-mini:OJH dannis_kenny1115$ sudo npm install grunt-contrib-uglify
/Volumes/Macintosh HD 2/OJH
├── UNMET PEER DEPENDENCY grunt@>=0.4.0
├─┬ grunt-contrib-csslint@0.5.0
│ ├─┬ chalk@1.1.1 
│ │ ├── ansi-styles@2.1.0 
│ │ ├─┬ has-ansi@2.0.0 
│ │ │ └── ansi-regex@2.0.0 
│ │ ├── strip-ansi@3.0.0 
│ │ └── supports-color@2.0.0 
│ └── lodash@3.10.1 
├─┬ grunt-contrib-cssmin@0.14.0
│ ├─┬ chalk@1.1.1 
│ │ ├── ansi-styles@2.1.0 
│ │ ├─┬ has-ansi@2.0.0 
│ │ │ └── ansi-regex@2.0.0 
│ │ ├── strip-ansi@3.0.0 
│ │ └── supports-color@2.0.0 
│ └─┬ maxmin@1.1.0
│   └─┬ chalk@1.1.1 
│     ├── ansi-styles@2.1.0 
│     ├─┬ has-ansi@2.0.0 
│     │ └── ansi-regex@2.0.0 
│     ├── strip-ansi@3.0.0 
│     └── supports-color@2.0.0 
└─┬ grunt-contrib-uglify@0.11.0 
  ├─┬ chalk@1.1.1 
  │ ├── ansi-styles@2.1.0 
  │ ├─┬ has-ansi@2.0.0 
  │ │ └── ansi-regex@2.0.0 
  │ ├── strip-ansi@3.0.0 
  │ └── supports-color@2.0.0 
  ├── lodash@3.10.1 
  ├─┬ uglify-js@2.6.1 
  │ ├── async@0.2.10 
  │ ├── source-map@0.5.3 
  │ ├── uglify-to-browserify@1.0.2 
  │ └─┬ yargs@3.10.0 
  │   ├── camelcase@1.2.1 
  │   ├─┬ cliui@2.1.0 
  │   │ ├─┬ center-align@0.1.2 
  │   │ │ ├─┬ align-text@0.1.3 
  │   │ │ │ ├─┬ kind-of@2.0.1 
  │   │ │ │ │ └── is-buffer@1.1.1 
  │   │ │ │ ├── longest@1.0.1 
  │   │ │ │ └── repeat-string@1.5.2 
  │   │ │ └── lazy-cache@0.2.7 
  │   │ ├── right-align@0.1.3 
  │   │ └── wordwrap@0.0.2 
  │   └── window-size@0.1.0 
  └── uri-path@1.0.0 

npm WARN ENOENT ENOENT: no such file or directory, open '/Volumes/Macintosh HD 2/OJH/package.json'
npm WARN EPEERINVALID grunt-contrib-concat@0.5.1 requires a peer of grunt@>=0.4.0 but none was installed.
npm WARN EPEERINVALID grunt-contrib-csslint@0.5.0 requires a peer of grunt@>=0.4.0 but none was installed.
npm WARN EPEERINVALID grunt-contrib-cssmin@0.14.0 requires a peer of grunt@>=0.4.0 but none was installed.
npm WARN EPEERINVALID grunt-contrib-jshint@0.11.3 requires a peer of grunt@>=0.4.0 but none was installed.
npm WARN EPEERINVALID grunt-contrib-uglify@0.11.0 requires a peer of grunt@>=0.4.0 but none was installed.
npm WARN EPACKAGEJSON OJH No description
npm WARN EPACKAGEJSON OJH No repository field.
npm WARN EPACKAGEJSON OJH No README data
npm WARN EPACKAGEJSON OJH No license field.
Danniss-Mac-mini:OJH dannis_kenny1115$ sudo npm install grunt-contrib-watch
npm WARN deprecated lodash@1.0.2: lodash@<2.0.0 is no longer maintained. Upgrade to lodash@^3.0.0
/Volumes/Macintosh HD 2/OJH
├── UNMET PEER DEPENDENCY grunt@>=0.4.0
├─┬ grunt-contrib-uglify@0.11.0
│ └─┬ uglify-js@2.6.1
│   └── async@0.2.10 
└─┬ grunt-contrib-watch@0.6.1 
  ├── async@0.2.10 
  ├─┬ gaze@0.5.2 
  │ └─┬ globule@0.1.0 
  │   ├─┬ glob@3.1.21 
  │   │ ├── graceful-fs@1.2.3 
  │   │ └── inherits@1.0.2 
  │   ├── lodash@1.0.2 
  │   └── minimatch@0.2.14 
  └─┬ tiny-lr-fork@0.0.5 
    ├── debug@0.7.4 
    ├── faye-websocket@0.4.4 
    ├─┬ noptify@0.0.3 
    │ └── nopt@2.0.0 
    └── qs@0.5.6 

npm WARN ENOENT ENOENT: no such file or directory, open '/Volumes/Macintosh HD 2/OJH/package.json'
npm WARN EPEERINVALID grunt-contrib-concat@0.5.1 requires a peer of grunt@>=0.4.0 but none was installed.
npm WARN EPEERINVALID grunt-contrib-csslint@0.5.0 requires a peer of grunt@>=0.4.0 but none was installed.
npm WARN EPEERINVALID grunt-contrib-cssmin@0.14.0 requires a peer of grunt@>=0.4.0 but none was installed.
npm WARN EPEERINVALID grunt-contrib-jshint@0.11.3 requires a peer of grunt@>=0.4.0 but none was installed.
npm WARN EPEERINVALID grunt-contrib-uglify@0.11.0 requires a peer of grunt@>=0.4.0 but none was installed.
npm WARN EPEERINVALID grunt-contrib-watch@0.6.1 requires a peer of grunt@~0.4.0 but none was installed.
npm WARN EPACKAGEJSON OJH No description
npm WARN EPACKAGEJSON OJH No repository field.
npm WARN EPACKAGEJSON OJH No README data
npm WARN EPACKAGEJSON OJH No license field.
Danniss-Mac-mini:OJH dannis_kenny1115$ grunt -version
-bash: grunt: command not found
Danniss-Mac-mini:OJH dannis_kenny1115$ sudo npm install grunt-cli -g
/usr/local/bin/grunt -> /usr/local/lib/node_modules/grunt-cli/bin/grunt
/usr/local/lib
└─┬ grunt-cli@0.1.13 
  ├─┬ findup-sync@0.1.3 
  │ ├─┬ glob@3.2.11 
  │ │ ├── inherits@2.0.1 
  │ │ └─┬ minimatch@0.3.0 
  │ │   ├── lru-cache@2.7.3 
  │ │   └── sigmund@1.0.1 
  │ └── lodash@2.4.2 
  ├─┬ nopt@1.0.10 
  │ └── abbrev@1.0.7 
  └── resolve@0.3.1 

Danniss-Mac-mini:OJH dannis_kenny1115$ grunt -version
grunt-cli v0.1.13
Danniss-Mac-mini:OJH dannis_kenny1115$ npm install grunt -g
loadRequestedDeps → netwo ▄ ╢██████████████████████████████████████████░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░╟
^C
Danniss-Mac-mini:OJH dannis_kenny1115$ sudo npm install grunt -g
npm WARN deprecated lodash@0.9.2: lodash@<2.0.0 is no longer maintained. Upgrade to lodash@^3.0.0
/usr/local/lib
└─┬ grunt@0.4.5 
  ├── async@0.1.22 
  ├── coffee-script@1.3.3 
  ├── colors@0.6.2 
  ├── dateformat@1.0.2-1.2.3 
  ├── eventemitter2@0.4.14 
  ├── exit@0.1.2 
  ├─┬ findup-sync@0.1.3 
  │ ├─┬ glob@3.2.11 
  │ │ ├── inherits@2.0.1 
  │ │ └── minimatch@0.3.0 
  │ └── lodash@2.4.2 
  ├── getobject@0.1.0 
  ├─┬ glob@3.1.21 
  │ ├── graceful-fs@1.2.3 
  │ └── inherits@1.0.2 
  ├─┬ grunt-legacy-log@0.1.3 
  │ ├─┬ grunt-legacy-log-utils@0.1.1 
  │ │ ├── lodash@2.4.2 
  │ │ └── underscore.string@2.3.3 
  │ ├── lodash@2.4.2 
  │ └── underscore.string@2.3.3 
  ├── grunt-legacy-util@0.2.0 
  ├── hooker@0.2.3 
  ├── iconv-lite@0.2.11 
  ├─┬ js-yaml@2.0.5 
  │ ├─┬ argparse@0.1.16 
  │ │ ├── underscore@1.7.0 
  │ │ └── underscore.string@2.4.0 
  │ └── esprima@1.0.4 
  ├── lodash@0.9.2 
  ├─┬ minimatch@0.2.14 
  │ ├── lru-cache@2.7.3 
  │ └── sigmund@1.0.1 
  ├─┬ nopt@1.0.10 
  │ └── abbrev@1.0.7 
  ├── rimraf@2.2.8 
  ├── underscore.string@2.2.1 
  └── which@1.0.9 

Danniss-Mac-mini:OJH dannis_kenny1115$ grunt -version
grunt-cli v0.1.13
Danniss-Mac-mini:OJH dannis_kenny1115$ clear

Danniss-Mac-mini:OJH dannis_kenny1115$ sudo npm install grunt-contrib-watch
npm WARN deprecated lodash@1.0.2: lodash@<2.0.0 is no longer maintained. Upgrade to lodash@^3.0.0
/Volumes/Macintosh HD 2/OJH
├── UNMET PEER DEPENDENCY grunt@>=0.4.0
└── grunt-contrib-watch@0.6.1 

npm WARN ENOENT ENOENT: no such file or directory, open '/Volumes/Macintosh HD 2/OJH/package.json'
npm WARN EPEERINVALID grunt-contrib-concat@0.5.1 requires a peer of grunt@>=0.4.0 but none was installed.
npm WARN EPEERINVALID grunt-contrib-csslint@0.5.0 requires a peer of grunt@>=0.4.0 but none was installed.
npm WARN EPEERINVALID grunt-contrib-cssmin@0.14.0 requires a peer of grunt@>=0.4.0 but none was installed.
npm WARN EPEERINVALID grunt-contrib-jshint@0.11.3 requires a peer of grunt@>=0.4.0 but none was installed.
npm WARN EPEERINVALID grunt-contrib-uglify@0.11.0 requires a peer of grunt@>=0.4.0 but none was installed.
npm WARN EPEERINVALID grunt-contrib-watch@0.6.1 requires a peer of grunt@~0.4.0 but none was installed.
npm WARN EPACKAGEJSON OJH No description
npm WARN EPACKAGEJSON OJH No repository field.
npm WARN EPACKAGEJSON OJH No README data
npm WARN EPACKAGEJSON OJH No license field.
Danniss-Mac-mini:OJH dannis_kenny1115$ sudo npm install grunt-env
/Volumes/Macintosh HD 2/OJH
├── UNMET PEER DEPENDENCY grunt@>=0.4.0
└─┬ grunt-env@0.4.4 
  └── ini@1.3.4 

npm WARN ENOENT ENOENT: no such file or directory, open '/Volumes/Macintosh HD 2/OJH/package.json'
npm WARN EPEERINVALID grunt-contrib-concat@0.5.1 requires a peer of grunt@>=0.4.0 but none was installed.
npm WARN EPEERINVALID grunt-contrib-csslint@0.5.0 requires a peer of grunt@>=0.4.0 but none was installed.
npm WARN EPEERINVALID grunt-contrib-cssmin@0.14.0 requires a peer of grunt@>=0.4.0 but none was installed.
npm WARN EPEERINVALID grunt-contrib-jshint@0.11.3 requires a peer of grunt@>=0.4.0 but none was installed.
npm WARN EPEERINVALID grunt-contrib-uglify@0.11.0 requires a peer of grunt@>=0.4.0 but none was installed.
npm WARN EPEERINVALID grunt-contrib-watch@0.6.1 requires a peer of grunt@~0.4.0 but none was installed.
npm WARN EPACKAGEJSON OJH No description
npm WARN EPACKAGEJSON OJH No repository field.
npm WARN EPACKAGEJSON OJH No README data
npm WARN EPACKAGEJSON OJH No license field.
Danniss-Mac-mini:OJH dannis_kenny1115$ sudo npm install grunt
npm WARN deprecated lodash@0.9.2: lodash@<2.0.0 is no longer maintained. Upgrade to lodash@^3.0.0
/Volumes/Macintosh HD 2/OJH
├─┬ grunt@0.4.5 
│ ├── async@0.1.22 
│ ├── coffee-script@1.3.3 
│ ├── colors@0.6.2 
│ ├── dateformat@1.0.2-1.2.3 
│ ├── eventemitter2@0.4.14 
│ ├── getobject@0.1.0 
│ ├─┬ glob@3.1.21 
│ │ ├── graceful-fs@1.2.3 
│ │ └── inherits@1.0.2 
│ ├─┬ grunt-legacy-log@0.1.3 
│ │ ├─┬ grunt-legacy-log-utils@0.1.1 
│ │ │ └── underscore.string@2.3.3 
│ │ └── underscore.string@2.3.3 
│ ├─┬ grunt-legacy-util@0.2.0 
│ │ ├── async@0.1.22 
│ │ └── lodash@0.9.2 
│ ├── iconv-lite@0.2.11 
│ ├─┬ js-yaml@2.0.5 
│ │ ├─┬ argparse@0.1.16 
│ │ │ ├── underscore@1.7.0 
│ │ │ └── underscore.string@2.4.0 
│ │ └── esprima@1.0.4 
│ ├── lodash@0.9.2 
│ ├── minimatch@0.2.14 
│ ├── rimraf@2.2.8 
│ ├── underscore.string@2.2.1 
│ └── which@1.0.9 
└─┬ grunt-contrib-watch@0.6.1
  └─┬ gaze@0.5.2
    └─┬ globule@0.1.0
      ├─┬ glob@3.1.21 
      │ ├── graceful-fs@1.2.3 
      │ └── inherits@1.0.2 
      └── minimatch@0.2.14 

npm WARN ENOENT ENOENT: no such file or directory, open '/Volumes/Macintosh HD 2/OJH/package.json'
npm WARN EPACKAGEJSON OJH No description
npm WARN EPACKAGEJSON OJH No repository field.
npm WARN EPACKAGEJSON OJH No README data
npm WARN EPACKAGEJSON OJH No license field.
Danniss-Mac-mini:OJH dannis_kenny1115$ clear

Danniss-Mac-mini:OJH dannis_kenny1115$ sudo npm install grunt-env
/Volumes/Macintosh HD 2/OJH
└── grunt-env@0.4.4 

npm WARN ENOENT ENOENT: no such file or directory, open '/Volumes/Macintosh HD 2/OJH/package.json'
npm WARN EPACKAGEJSON OJH No description
npm WARN EPACKAGEJSON OJH No repository field.
npm WARN EPACKAGEJSON OJH No README data
npm WARN EPACKAGEJSON OJH No license field.
Danniss-Mac-mini:OJH dannis_kenny1115$ sudo npm install grunt-forever
/Volumes/Macintosh HD 2/OJH
├─┬ grunt-contrib-uglify@0.11.0
│ └─┬ uglify-js@2.6.1
│   └── async@0.2.10 
├─┬ grunt-contrib-watch@0.6.1
│ └── async@0.2.10 
└─┬ grunt-forever@0.4.7 
  └─┬ forever@0.14.2 
    ├─┬ cliff@0.1.10 
    │ ├── colors@1.0.3 
    │ └── eyes@0.1.8 
    ├─┬ flatiron@0.4.3 
    │ ├─┬ broadway@0.3.6 
    │ │ ├── cliff@0.1.9 
    │ │ └─┬ winston@0.8.0 
    │ │   └── async@0.2.10 
    │ ├── director@1.2.7 
    │ ├─┬ optimist@0.6.0 
    │ │ └── minimist@0.0.10 
    │ └─┬ prompt@0.2.14 
    │   ├─┬ read@1.0.7 
    │   │ └── mute-stream@0.0.5 
    │   └── revalidator@0.1.8 
    ├─┬ forever-monitor@1.5.2 
    │ ├── minimatch@1.0.0 
    │ ├─┬ ps-tree@0.0.3 
    │ │ └─┬ event-stream@0.5.3 
    │ │   └── optimist@0.2.8 
    │ └── watch@0.13.0 
    ├─┬ nconf@0.6.9 
    │ ├── async@0.2.9 
    │ └─┬ optimist@0.6.0 
    │   └── minimist@0.0.10 
    ├─┬ nssocket@0.5.3 
    │ └── lazy@1.0.11 
    ├─┬ optimist@0.6.1 
    │ └── minimist@0.0.10 
    ├── timespan@2.3.0 
    ├─┬ utile@0.2.1 
    │ ├── async@0.2.10 
    │ ├── deep-equal@1.0.1 
    │ ├── i@0.3.3 
    │ ├─┬ mkdirp@0.5.1 
    │ │ └── minimist@0.0.8 
    │ └── ncp@0.4.2 
    └─┬ winston@0.8.3 
      ├── async@0.2.10 
      ├── cycle@1.0.3 
      ├── isstream@0.1.2 
      ├── pkginfo@0.3.1 
      └── stack-trace@0.0.9 

npm WARN ENOENT ENOENT: no such file or directory, open '/Volumes/Macintosh HD 2/OJH/package.json'
npm WARN EPACKAGEJSON OJH No description
npm WARN EPACKAGEJSON OJH No repository field.
npm WARN EPACKAGEJSON OJH No README data
npm WARN EPACKAGEJSON OJH No license field.
Danniss-Mac-mini:OJH dannis_kenny1115$ sudo npm install grunt-karama
npm ERR! Darwin 15.2.0
npm ERR! argv "/usr/local/bin/node" "/usr/local/bin/npm" "install" "grunt-karama"
npm ERR! node v5.4.1
npm ERR! npm  v3.3.12
npm ERR! code E404

npm ERR! 404 Registry returned 404 for GET on https://registry.npmjs.org/grunt-karama
npm ERR! 404 
npm ERR! 404 'grunt-karama' is not in the npm registry.
npm ERR! 404 You should bug the author to publish it (or use the name yourself!)
npm ERR! 404 
npm ERR! 404 Note that you can also install from a
npm ERR! 404 tarball, folder, http url, or git url.

npm ERR! Please include the following file with any support request:
npm ERR!     /Volumes/Macintosh HD 2/OJH/npm-debug.log
Danniss-Mac-mini:OJH dannis_kenny1115$ clear

Danniss-Mac-mini:OJH dannis_kenny1115$ sudo npm install grunt-karma
/Volumes/Macintosh HD 2/OJH
├─┬ grunt-contrib-csslint@0.5.0
│ └── lodash@3.10.1 
├─┬ grunt-contrib-uglify@0.11.0
│ └── lodash@3.10.1 
├─┬ grunt-karma@0.12.1 
│ └── lodash@3.10.1 
└── UNMET PEER DEPENDENCY karma@^0.13.0 || >= 0.14.0-rc.0

npm WARN ENOENT ENOENT: no such file or directory, open '/Volumes/Macintosh HD 2/OJH/package.json'
npm WARN EPEERINVALID grunt-karma@0.12.1 requires a peer of karma@^0.13.0 || >= 0.14.0-rc.0 but none was installed.
npm WARN EPACKAGEJSON OJH No description
npm WARN EPACKAGEJSON OJH No repository field.
npm WARN EPACKAGEJSON OJH No README data
npm WARN EPACKAGEJSON OJH No license field.
Danniss-Mac-mini:OJH dannis_kenny1115$ sudo npm install karma@^0.13.0

> fsevents@1.0.6 install /Volumes/Macintosh HD 2/OJH/node_modules/fsevents
> node-pre-gyp install --fallback-to-build

[fsevents] Success: "/Volumes/Macintosh HD 2/OJH/node_modules/fsevents/lib/binding/Release/node-v47-darwin-x64/fse.node" is installed via remote
/Volumes/Macintosh HD 2/OJH
├─┬ grunt-contrib-csslint@0.5.0
│ └── lodash@3.10.1 
├─┬ grunt-contrib-jshint@0.11.3
│ └─┬ jshint@2.8.0
│   └── minimatch@2.0.10 
├─┬ grunt-contrib-uglify@0.11.0
│ ├── lodash@3.10.1 
│ └─┬ uglify-js@2.6.1
│   ├── async@0.2.10 
│   └── source-map@0.5.3 
├─┬ grunt-contrib-watch@0.6.1
│ ├── async@0.2.10 
│ └─┬ tiny-lr-fork@0.0.5
│   └── debug@0.7.4 
├─┬ grunt-forever@0.4.7
│ └─┬ forever@0.14.2
│   ├─┬ flatiron@0.4.3
│   │ └─┬ broadway@0.3.6
│   │   └─┬ winston@0.8.0
│   │     └── async@0.2.10 
│   ├─┬ utile@0.2.1
│   │ └── async@0.2.10 
│   └─┬ winston@0.8.3
│     └── async@0.2.10 
├─┬ grunt-karma@0.12.1
│ └── lodash@3.10.1 
└─┬ karma@0.13.19 
  ├── batch@0.5.3 
  ├── bluebird@2.10.2 
  ├─┬ body-parser@1.14.2 
  │ ├── bytes@2.2.0 
  │ ├── content-type@1.0.1 
  │ ├─┬ debug@2.2.0 
  │ │ └── ms@0.7.1 
  │ ├── depd@1.1.0 
  │ ├─┬ http-errors@1.3.1 
  │ │ └── statuses@1.2.1 
  │ ├── iconv-lite@0.4.13 
  │ ├─┬ on-finished@2.3.0 
  │ │ └── ee-first@1.1.1 
  │ ├── qs@5.2.0 
  │ ├─┬ raw-body@2.1.5 
  │ │ ├── iconv-lite@0.4.13 
  │ │ └── unpipe@1.0.0 
  │ └─┬ type-is@1.6.10 
  │   ├── media-typer@0.3.0 
  │   └─┬ mime-types@2.1.9 
  │     └── mime-db@1.21.0 
  ├─┬ chokidar@1.4.2 
  │ ├─┬ anymatch@1.3.0 
  │ │ ├── arrify@1.0.1 
  │ │ └─┬ micromatch@2.3.7 
  │ │   ├─┬ arr-diff@2.0.0 
  │ │   │ └── arr-flatten@1.0.1 
  │ │   ├─┬ braces@1.8.3 
  │ │   │ ├─┬ expand-range@1.8.1 
  │ │   │ │ └─┬ fill-range@2.2.3 
  │ │   │ │   ├─┬ is-number@2.1.0 
  │ │   │ │   │ └── kind-of@3.0.2 
  │ │   │ │   ├── isobject@2.0.0 
  │ │   │ │   └─┬ randomatic@1.1.5 
  │ │   │ │     └── kind-of@3.0.2 
  │ │   │ ├── preserve@0.2.0 
  │ │   │ └── repeat-element@1.1.2 
  │ │   ├── expand-brackets@0.1.4 
  │ │   ├─┬ extglob@0.3.1 
  │ │   │ ├─┬ ansi-green@0.1.1 
  │ │   │ │ └── ansi-wrap@0.1.0 
  │ │   │ └── success-symbol@0.1.0 
  │ │   ├── filename-regex@2.0.0 
  │ │   ├── kind-of@3.0.2 
  │ │   ├── normalize-path@2.0.1 
  │ │   ├─┬ object.omit@2.0.0 
  │ │   │ ├─┬ for-own@0.1.3 
  │ │   │ │ └── for-in@0.1.4 
  │ │   │ └── is-extendable@0.1.1 
  │ │   ├─┬ parse-glob@3.0.4 
  │ │   │ ├── glob-base@0.3.0 
  │ │   │ └── is-dotfile@1.0.2 
  │ │   └─┬ regex-cache@0.4.2 
  │ │     ├── is-equal-shallow@0.1.3 
  │ │     └── is-primitive@2.0.0 
  │ ├── async-each@0.1.6 
  │ ├─┬ fsevents@1.0.6 
  │ │ ├── nan@2.2.0 
  │ │ └─┬ node-pre-gyp@0.6.17
  │ │   ├─┬ request@2.67.0
  │ │   │ ├── extend@3.0.0 
  │ │   │ └── qs@5.2.0 
  │ │   ├─┬ rimraf@2.4.4
  │ │   │ └─┬ glob@5.0.15
  │ │   │   ├── inflight@1.0.4 
  │ │   │   ├── minimatch@3.0.0 
  │ │   │   └── path-is-absolute@1.0.0 
  │ │   └─┬ tar-pack@3.1.0
  │ │     ├── debug@0.7.4 
  │ │     ├─┬ fstream-ignore@1.0.3
  │ │     │ └── minimatch@3.0.0 
  │ │     └── readable-stream@1.0.33 
  │ ├── glob-parent@2.0.0 
  │ ├─┬ is-binary-path@1.0.1 
  │ │ └── binary-extensions@1.4.0 
  │ ├─┬ is-glob@2.0.1 
  │ │ └── is-extglob@1.0.0 
  │ ├── path-is-absolute@1.0.0 
  │ └─┬ readdirp@2.0.0 
  │   └── minimatch@2.0.10 
  ├── colors@1.1.2 
  ├─┬ connect@3.4.0 
  │ ├── debug@2.2.0 
  │ ├─┬ finalhandler@0.4.0 
  │ │ ├── debug@2.2.0 
  │ │ └── escape-html@1.0.2 
  │ ├── parseurl@1.3.0 
  │ └── utils-merge@1.0.0 
  ├── core-js@2.0.3 
  ├── di@0.0.1 
  ├─┬ dom-serialize@2.2.1 
  │ ├── custom-event@1.0.0 
  │ ├── ent@2.2.0 
  │ ├── extend@3.0.0 
  │ └── void-elements@2.0.1 
  ├─┬ expand-braces@0.1.2 
  │ ├── array-slice@0.2.3 
  │ ├── array-unique@0.2.1 
  │ └─┬ braces@0.1.5 
  │   └─┬ expand-range@0.1.1 
  │     ├── is-number@0.1.1 
  │     └── repeat-string@0.2.2 
  ├─┬ glob@6.0.4 
  │ └── inflight@1.0.4 
  ├─┬ http-proxy@1.12.0 
  │ ├── eventemitter3@1.1.1 
  │ └── requires-port@0.0.1 
  ├── lodash@3.10.1 
  ├─┬ log4js@0.6.29 
  │ ├── async@0.2.10 
  │ ├── readable-stream@1.0.33 
  │ ├── semver@4.3.6 
  │ └── underscore@1.8.2 
  ├── mime@1.3.4 
  ├── minimatch@3.0.0 
  ├── rimraf@2.5.0 
  ├─┬ socket.io@1.4.4 
  │ ├── debug@2.2.0 
  │ ├─┬ engine.io@1.6.7 
  │ │ ├─┬ accepts@1.1.4 
  │ │ │ ├─┬ mime-types@2.0.14 
  │ │ │ │ └── mime-db@1.12.0 
  │ │ │ └── negotiator@0.4.9 
  │ │ ├── base64id@0.1.0 
  │ │ ├── debug@2.2.0 
  │ │ ├─┬ engine.io-parser@1.2.4 
  │ │ │ ├── after@0.8.1 
  │ │ │ ├── arraybuffer.slice@0.0.6 
  │ │ │ ├── base64-arraybuffer@0.1.2 
  │ │ │ ├── blob@0.0.4 
  │ │ │ ├── has-binary@0.1.6 
  │ │ │ └── utf8@2.1.0 
  │ │ └─┬ ws@1.0.1 
  │ │   ├── options@0.0.6 
  │ │   └── ultron@1.0.2 
  │ ├── has-binary@0.1.7 
  │ ├─┬ socket.io-adapter@0.4.0 
  │ │ ├── debug@2.2.0 
  │ │ └─┬ socket.io-parser@2.2.2 
  │ │   ├── debug@0.7.4 
  │ │   └── json3@3.2.6 
  │ ├─┬ socket.io-client@1.4.4 
  │ │ ├── backo2@1.0.2 
  │ │ ├── component-bind@1.0.0 
  │ │ ├── component-emitter@1.2.0 
  │ │ ├── debug@2.2.0 
  │ │ ├─┬ engine.io-client@1.6.7 
  │ │ │ ├── component-inherit@0.0.3 
  │ │ │ ├── debug@2.2.0 
  │ │ │ ├── has-cors@1.1.0 
  │ │ │ ├── parsejson@0.0.1 
  │ │ │ ├── parseqs@0.0.2 
  │ │ │ ├── xmlhttprequest-ssl@1.5.1 
  │ │ │ └── yeast@0.1.2 
  │ │ ├── indexof@0.0.1 
  │ │ ├── object-component@0.0.3 
  │ │ ├─┬ parseuri@0.0.4 
  │ │ │ └─┬ better-assert@1.0.2 
  │ │ │   └── callsite@1.0.0 
  │ │ ├─┬ socket.io-parser@2.2.5 
  │ │ │ ├── component-emitter@1.1.2 
  │ │ │ └── json3@3.3.2 
  │ │ └── to-array@0.1.3 
  │ └─┬ socket.io-parser@2.2.6 
  │   ├── benchmark@1.0.0 
  │   ├── component-emitter@1.1.2 
  │   ├── debug@2.2.0 
  │   └── json3@3.3.2 
  ├── source-map@0.5.3 
  └─┬ useragent@2.1.8 
    └── lru-cache@2.2.4 

npm WARN ENOENT ENOENT: no such file or directory, open '/Volumes/Macintosh HD 2/OJH/package.json'
npm WARN EPACKAGEJSON OJH No description
npm WARN EPACKAGEJSON OJH No repository field.
npm WARN EPACKAGEJSON OJH No README data
npm WARN EPACKAGEJSON OJH No license field.
Danniss-Mac-mini:OJH dannis_kenny1115$ sudo npm install grunt-mocha-test
/Volumes/Macintosh HD 2/OJH
├── grunt-mocha-test@0.12.7 
└── UNMET PEER DEPENDENCY mocha@>=1.20.0

npm WARN ENOENT ENOENT: no such file or directory, open '/Volumes/Macintosh HD 2/OJH/package.json'
npm WARN EPEERINVALID grunt-mocha-test@0.12.7 requires a peer of mocha@>=1.20.0 but none was installed.
npm WARN EPACKAGEJSON OJH No description
npm WARN EPACKAGEJSON OJH No repository field.
npm WARN EPACKAGEJSON OJH No README data
npm WARN EPACKAGEJSON OJH No license field.
Danniss-Mac-mini:OJH dannis_kenny1115$ sudo npm install mocha
/Volumes/Macintosh HD 2/OJH
├─┬ grunt@0.4.5
│ └── minimatch@0.2.14 
├─┬ grunt-contrib-watch@0.6.1
│ └─┬ gaze@0.5.2
│   └─┬ globule@0.1.0
│     └── minimatch@0.2.14 
├─┬ grunt-mocha-test@0.12.7
│ └─┬ mkdirp@0.5.1
│   └── minimist@0.0.8 
├─┬ karma@0.13.19
│ ├─┬ body-parser@1.14.2
│ │ └── debug@2.2.0 
│ ├─┬ chokidar@1.4.2
│ │ └─┬ fsevents@1.0.6
│ │   └─┬ node-pre-gyp@0.6.17
│ │     └─┬ mkdirp@0.5.1
│ │       └── minimist@0.0.8 
│ ├─┬ connect@3.4.0
│ │ ├── debug@2.2.0 
│ │ └─┬ finalhandler@0.4.0
│ │   └── debug@2.2.0 
│ └─┬ socket.io@1.4.4
│   ├── debug@2.2.0 
│   ├─┬ engine.io@1.6.7
│   │ └── debug@2.2.0 
│   ├─┬ socket.io-adapter@0.4.0
│   │ └── debug@2.2.0 
│   ├─┬ socket.io-client@1.4.4
│   │ ├── debug@2.2.0 
│   │ └─┬ engine.io-client@1.6.7
│   │   └── debug@2.2.0 
│   └─┬ socket.io-parser@2.2.6
│     └── debug@2.2.0 
└─┬ mocha@2.3.4 
  ├── commander@2.3.0 
  ├── debug@2.2.0 
  ├── diff@1.4.0 
  ├── escape-string-regexp@1.0.2 
  ├─┬ glob@3.2.3 
  │ ├── graceful-fs@2.0.3 
  │ └── minimatch@0.2.14 
  ├── growl@1.8.1 
  ├─┬ jade@0.26.3 
  │ ├── commander@0.6.1 
  │ └── mkdirp@0.3.0 
  ├─┬ mkdirp@0.5.0 
  │ └── minimist@0.0.8 
  └── supports-color@1.2.0 

npm WARN ENOENT ENOENT: no such file or directory, open '/Volumes/Macintosh HD 2/OJH/package.json'
npm WARN EPACKAGEJSON OJH No description
npm WARN EPACKAGEJSON OJH No repository field.
npm WARN EPACKAGEJSON OJH No README data
npm WARN EPACKAGEJSON OJH No license field.
Danniss-Mac-mini:OJH dannis_kenny1115$ clear

Danniss-Mac-mini:OJH dannis_kenny1115$ sudo npm install grunt-ng-annotate
/Volumes/Macintosh HD 2/OJH
├─┬ grunt-contrib-uglify@0.11.0
│ └─┬ uglify-js@2.6.1
│   └── source-map@0.5.3 
├─┬ grunt-ng-annotate@1.0.1 
│ ├─┬ lodash.clonedeep@3.0.2 
│ │ ├─┬ lodash._baseclone@3.3.0 
│ │ │ ├── lodash._arraycopy@3.0.0 
│ │ │ ├── lodash._arrayeach@3.0.0 
│ │ │ ├─┬ lodash._baseassign@3.2.0 
│ │ │ │ └── lodash._basecopy@3.0.1 
│ │ │ ├── lodash._basefor@3.0.3 
│ │ │ ├── lodash.isarray@3.0.4 
│ │ │ └─┬ lodash.keys@3.1.2 
│ │ │   ├── lodash._getnative@3.9.1 
│ │ │   └── lodash.isarguments@3.0.5 
│ │ └── lodash._bindcallback@3.0.1 
│ └─┬ ng-annotate@1.2.1 
│   ├── acorn@2.6.4 
│   ├── alter@0.2.0 
│   ├── convert-source-map@1.1.3 
│   ├─┬ ordered-ast-traverse@1.1.1 
│   │ └── ordered-esprima-props@1.1.0 
│   ├── simple-fmt@0.1.0 
│   ├── simple-is@0.2.0 
│   ├── source-map@0.5.3 
│   ├── stable@0.1.5 
│   ├── stringmap@0.2.2 
│   ├── stringset@0.2.1 
│   └── tryor@0.1.2 
└─┬ karma@0.13.19
  └── source-map@0.5.3 

npm WARN ENOENT ENOENT: no such file or directory, open '/Volumes/Macintosh HD 2/OJH/package.json'
npm WARN EPACKAGEJSON OJH No description
npm WARN EPACKAGEJSON OJH No repository field.
npm WARN EPACKAGEJSON OJH No README data
npm WARN EPACKAGEJSON OJH No license field.
Danniss-Mac-mini:OJH dannis_kenny1115$ sudo npm install grunt-node-inspector
npm WARN lifecycle v8-debug@0.7.0~preinstall: cannot run in wd %s %s (wd=%s) v8-debug@0.7.0 node -e 'process.exit(0)' /Volumes/Macintosh HD 2/OJH/node_modules/.staging/v8-debug-56569c3244c2cc1954fb5e8f50cb9a77
npm WARN lifecycle v8-profiler@5.5.0~preinstall: cannot run in wd %s %s (wd=%s) v8-profiler@5.5.0 node -e 'process.exit(0)' /Volumes/Macintosh HD 2/OJH/node_modules/.staging/v8-profiler-b8e9774585189eee2f25a178714c3d76

> bufferutil@1.2.1 install /Volumes/Macintosh HD 2/OJH/node_modules/bufferutil
> node-gyp rebuild

  CXX(target) Release/obj.target/bufferutil/src/bufferutil.o
  SOLINK_MODULE(target) Release/bufferutil.node

> utf-8-validate@1.2.1 install /Volumes/Macintosh HD 2/OJH/node_modules/utf-8-validate
> node-gyp rebuild

  CXX(target) Release/obj.target/validation/src/validation.o
  SOLINK_MODULE(target) Release/validation.node

> v8-debug@0.7.0 install /Volumes/Macintosh HD 2/OJH/node_modules/v8-debug
> node-pre-gyp install --fallback-to-build

[v8-debug] Success: "/Volumes/Macintosh HD 2/OJH/node_modules/v8-debug/build/debug/v0.7.0/node-v47-darwin-x64/debug.node" already installed
Pass --update-binary to reinstall or --build-from-source to recompile

> v8-profiler@5.5.0 install /Volumes/Macintosh HD 2/OJH/node_modules/v8-profiler
> node-pre-gyp install --fallback-to-build

[v8-profiler] Success: "/Volumes/Macintosh HD 2/OJH/node_modules/v8-profiler/build/profiler/v5.5.0/node-v47-darwin-x64/profiler.node" already installed
Pass --update-binary to reinstall or --build-from-source to recompile
/Volumes/Macintosh HD 2/OJH
├─┬ grunt-contrib-csslint@0.5.0
│ └── lodash@3.10.1 
├─┬ grunt-contrib-uglify@0.11.0
│ └── lodash@3.10.1 
├─┬ grunt-karma@0.12.1
│ └── lodash@3.10.1 
├─┬ grunt-node-inspector@0.4.1 
│ └─┬ node-inspector@0.12.5 
│   ├── async@0.9.2 
│   ├─┬ biased-opener@0.2.7 
│   │ ├─┬ browser-launcher2@0.4.6 
│   │ │ ├── headless@0.1.7 
│   │ │ ├─┬ osenv@0.1.3 
│   │ │ │ ├── os-homedir@1.0.1 
│   │ │ │ └── os-tmpdir@1.0.1 
│   │ │ ├─┬ plist@1.2.0 
│   │ │ │ ├── base64-js@0.0.8 
│   │ │ │ ├─┬ xmlbuilder@4.0.0 
│   │ │ │ │ └── lodash@3.10.1 
│   │ │ │ └── xmldom@0.1.21 
│   │ │ ├── uid@0.0.2 
│   │ │ └─┬ win-detect-browsers@1.0.2 
│   │ │   ├── debug@2.2.0 
│   │ │   └── yargs@1.3.3 
│   │ └─┬ x-default-browser@0.3.1 
│   │   └─┬ default-browser-id@1.0.4 
│   │     ├─┬ bplist-parser@0.1.1 
│   │     │ └── big-integer@1.6.10 
│   │     └── untildify@2.1.0 
│   ├── debug@2.2.0 
│   ├─┬ express@4.13.3 
│   │ ├─┬ accepts@1.2.13 
│   │ │ └── negotiator@0.5.3 
│   │ ├── array-flatten@1.1.1 
│   │ ├── content-disposition@0.5.0 
│   │ ├── cookie@0.1.3 
│   │ ├── cookie-signature@1.0.6 
│   │ ├── debug@2.2.0 
│   │ ├── depd@1.0.1 
│   │ ├── etag@1.7.0 
│   │ ├── fresh@0.3.0 
│   │ ├── merge-descriptors@1.0.0 
│   │ ├── methods@1.1.1 
│   │ ├── path-to-regexp@0.1.7 
│   │ ├─┬ proxy-addr@1.0.10 
│   │ │ ├── forwarded@0.1.0 
│   │ │ └── ipaddr.js@1.0.5 
│   │ ├── qs@4.0.0 
│   │ ├── range-parser@1.0.3 
│   │ ├─┬ send@0.13.0 
│   │ │ ├── debug@2.2.0 
│   │ │ ├── depd@1.0.1 
│   │ │ └── destroy@1.0.3 
│   │ ├── serve-static@1.10.0 
│   │ └── vary@1.0.1 
│   ├─┬ glob@5.0.15 
│   │ └── minimatch@3.0.0 
│   ├─┬ rc@1.1.6 
│   │ └── deep-extend@0.4.0 
│   ├── semver@4.3.6 
│   ├── serve-favicon@2.3.0 
│   ├─┬ strong-data-uri@1.0.3 
│   │ └── truncate@1.0.5 
│   ├─┬ v8-debug@0.7.0 
│   │ └─┬ node-pre-gyp@0.6.19 
│   │   ├─┬ rc@1.1.6 
│   │   │ └── deep-extend@0.4.0 
│   │   ├─┬ rimraf@2.5.0
│   │   │ └─┬ glob@6.0.3
│   │   │   └── minimatch@3.0.0 
│   │   └─┬ tar-pack@3.1.2
│   │     ├─┬ fstream-ignore@1.0.3
│   │     │ └── minimatch@3.0.0 
│   │     └─┬ rimraf@2.4.5
│   │       └─┬ glob@6.0.3
│   │         └── minimatch@3.0.0 
│   ├─┬ v8-profiler@5.5.0 
│   │ └── nan@2.0.9 
│   ├─┬ which@1.2.1 
│   │ └─┬ is-absolute@0.1.7 
│   │   └── is-relative@0.1.3 
│   └─┬ ws@0.8.1 
│     ├─┬ bufferutil@1.2.1 
│     │ └── bindings@1.2.1 
│     └── utf-8-validate@1.2.1 
├─┬ karma@0.13.19
│ ├─┬ body-parser@1.14.2
│ │ └── debug@2.2.0 
│ ├─┬ chokidar@1.4.2
│ │ └─┬ fsevents@1.0.6
│ │   └─┬ node-pre-gyp@0.6.17
│ │     ├─┬ rc@1.1.5
│ │     │ └── deep-extend@0.4.0 
│ │     ├─┬ rimraf@2.4.4
│ │     │ └─┬ glob@5.0.15 
│ │     │   └── minimatch@3.0.0 
│ │     └─┬ tar-pack@3.1.0
│ │       └─┬ fstream-ignore@1.0.3
│ │         └── minimatch@3.0.0 
│ ├─┬ connect@3.4.0
│ │ ├── debug@2.2.0 
│ │ └─┬ finalhandler@0.4.0
│ │   └── debug@2.2.0 
│ ├── lodash@3.10.1 
│ ├─┬ log4js@0.6.29
│ │ └── semver@4.3.6 
│ ├── minimatch@3.0.0 
│ └─┬ socket.io@1.4.4
│   ├── debug@2.2.0 
│   ├─┬ engine.io@1.6.7
│   │ └── debug@2.2.0 
│   ├─┬ socket.io-adapter@0.4.0
│   │ └── debug@2.2.0 
│   ├─┬ socket.io-client@1.4.4
│   │ ├── debug@2.2.0 
│   │ └─┬ engine.io-client@1.6.7
│   │   └── debug@2.2.0 
│   └─┬ socket.io-parser@2.2.6
│     └── debug@2.2.0 
└─┬ mocha@2.3.4
  └── debug@2.2.0 

npm WARN ENOENT ENOENT: no such file or directory, open '/Volumes/Macintosh HD 2/OJH/package.json'
npm WARN EPACKAGEJSON OJH No description
npm WARN EPACKAGEJSON OJH No repository field.
npm WARN EPACKAGEJSON OJH No README data
npm WARN EPACKAGEJSON OJH No license field.
Danniss-Mac-mini:OJH dannis_kenny1115$ sudo npm install grunt-nodemon
Password:
/Volumes/Macintosh HD 2/OJH
├─┬ grunt-contrib-csslint@0.5.0
│ └─┬ chalk@1.1.1 
│   ├── ansi-styles@2.1.0 
│   ├─┬ has-ansi@2.0.0 
│   │ └── ansi-regex@2.0.0 
│   ├── strip-ansi@3.0.0 
│   └── supports-color@2.0.0 
├─┬ grunt-contrib-cssmin@0.14.0
│ ├─┬ chalk@1.1.1 
│ │ ├── ansi-styles@2.1.0 
│ │ ├─┬ has-ansi@2.0.0 
│ │ │ └── ansi-regex@2.0.0 
│ │ ├── strip-ansi@3.0.0 
│ │ └── supports-color@2.0.0 
│ └─┬ maxmin@1.1.0
│   └─┬ chalk@1.1.1 
│     ├── ansi-styles@2.1.0 
│     ├─┬ has-ansi@2.0.0 
│     │ └── ansi-regex@2.0.0 
│     ├── strip-ansi@3.0.0 
│     └── supports-color@2.0.0 
├─┬ grunt-contrib-uglify@0.11.0
│ └─┬ chalk@1.1.1 
│   ├── ansi-styles@2.1.0 
│   ├─┬ has-ansi@2.0.0 
│   │ └── ansi-regex@2.0.0 
│   ├── strip-ansi@3.0.0 
│   └── supports-color@2.0.0 
├─┬ grunt-node-inspector@0.4.1
│ └─┬ node-inspector@0.12.5
│   ├─┬ biased-opener@0.2.7
│   │ └─┬ browser-launcher2@0.4.6
│   │   └─┬ win-detect-browsers@1.0.2
│   │     └── debug@2.2.0 
│   ├── debug@2.2.0 
│   ├─┬ express@4.13.3
│   │ ├── debug@2.2.0 
│   │ └─┬ send@0.13.0
│   │   └── debug@2.2.0 
│   ├─┬ glob@5.0.15
│   │ └── minimatch@3.0.0 
│   └─┬ v8-debug@0.7.0
│     └─┬ node-pre-gyp@0.6.19
│       ├─┬ request@2.67.0
│       │ └─┬ har-validator@2.0.3
│       │   └─┬ chalk@1.1.1 
│       │     ├── ansi-styles@2.1.0 
│       │     ├─┬ has-ansi@2.0.0 
│       │     │ └── ansi-regex@2.0.0 
│       │     ├─┬ strip-ansi@3.0.0 
│       │     │ └── ansi-regex@2.0.0 
│       │     └── supports-color@2.0.0 
│       ├─┬ rimraf@2.5.0
│       │ └─┬ glob@6.0.3
│       │   └── minimatch@3.0.0 
│       └─┬ tar-pack@3.1.2
│         ├─┬ fstream-ignore@1.0.3
│         │ └── minimatch@3.0.0 
│         └─┬ rimraf@2.4.5
│           └─┬ glob@6.0.3
│             └── minimatch@3.0.0 
├─┬ grunt-nodemon@0.4.1 
│ └─┬ nodemon@1.8.1 
│   ├── debug@2.2.0 
│   ├── es6-promise@3.0.2 
│   ├─┬ lodash.defaults@3.1.2 
│   │ ├─┬ lodash.assign@3.2.0 
│   │ │ └─┬ lodash._createassigner@3.1.1 
│   │ │   └── lodash._isiterateecall@3.0.9 
│   │ └── lodash.restparam@3.6.1 
│   ├── minimatch@3.0.0 
│   ├─┬ ps-tree@1.0.1 
│   │ └─┬ event-stream@3.3.2 
│   │   ├── duplexer@0.1.1 
│   │   ├── from@0.1.3 
│   │   ├── map-stream@0.1.0 
│   │   ├── pause-stream@0.0.11 
│   │   ├── split@0.3.3 
│   │   ├── stream-combiner@0.0.4 
│   │   └── through@2.3.8 
│   ├── touch@1.0.0 
│   ├── undefsafe@0.0.3 
│   └─┬ update-notifier@0.5.0 
│     ├─┬ chalk@1.1.1 
│     │ ├── ansi-styles@2.1.0 
│     │ ├─┬ has-ansi@2.0.0 
│     │ │ └── ansi-regex@2.0.0 
│     │ ├── strip-ansi@3.0.0 
│     │ └── supports-color@2.0.0 
│     ├─┬ configstore@1.4.0 
│     │ ├── uuid@2.0.1 
│     │ ├─┬ write-file-atomic@1.1.4 
│     │ │ ├── imurmurhash@0.1.4 
│     │ │ └── slide@1.1.6 
│     │ └── xdg-basedir@2.0.0 
│     ├── is-npm@1.0.0 
│     ├─┬ latest-version@1.0.1 
│     │ └─┬ package-json@1.2.0 
│     │   ├─┬ got@3.3.1 
│     │   │ ├── infinity-agent@2.0.3 
│     │   │ ├── is-redirect@1.0.0 
│     │   │ ├── is-stream@1.0.1 
│     │   │ ├── lowercase-keys@1.0.0 
│     │   │ ├── nested-error-stacks@1.0.2 
│     │   │ ├── object-assign@3.0.0 
│     │   │ ├── prepend-http@1.0.3 
│     │   │ ├─┬ read-all-stream@3.0.1 
│     │   │ │ └─┬ pinkie-promise@1.0.0 
│     │   │ │   └── pinkie@1.0.0 
│     │   │ └── timed-out@2.0.0 
│     │   └── registry-url@3.0.3 
│     ├── repeating@1.1.3 
│     ├── semver-diff@2.1.0 
│     └─┬ string-length@1.0.1 
│       └─┬ strip-ansi@3.0.0 
│         └── ansi-regex@2.0.0 
├─┬ karma@0.13.19
│ ├─┬ body-parser@1.14.2
│ │ └── debug@2.2.0 
│ ├─┬ chokidar@1.4.2
│ │ └─┬ fsevents@1.0.6
│ │   └─┬ node-pre-gyp@0.6.17
│ │     ├─┬ request@2.67.0
│ │     │ └─┬ har-validator@2.0.3
│ │     │   └─┬ chalk@1.1.1 
│ │     │     ├── ansi-styles@2.1.0 
│ │     │     ├─┬ has-ansi@2.0.0 
│ │     │     │ └── ansi-regex@2.0.0 
│ │     │     ├── strip-ansi@3.0.0 
│ │     │     └── supports-color@2.0.0 
│ │     ├─┬ rimraf@2.4.4
│ │     │ └─┬ glob@5.0.15
│ │     │   └── minimatch@3.0.0 
│ │     └─┬ tar-pack@3.1.0
│ │       └─┬ fstream-ignore@1.0.3
│ │         └── minimatch@3.0.0 
│ ├─┬ connect@3.4.0
│ │ ├── debug@2.2.0 
│ │ └─┬ finalhandler@0.4.0
│ │   └── debug@2.2.0 
│ ├── minimatch@3.0.0 
│ └─┬ socket.io@1.4.4
│   ├── debug@2.2.0 
│   ├─┬ engine.io@1.6.7
│   │ └── debug@2.2.0 
│   ├─┬ socket.io-adapter@0.4.0
│   │ └── debug@2.2.0 
│   ├─┬ socket.io-client@1.4.4
│   │ ├── debug@2.2.0 
│   │ └─┬ engine.io-client@1.6.7
│   │   └── debug@2.2.0 
│   └─┬ socket.io-parser@2.2.6
│     └── debug@2.2.0 
└─┬ mocha@2.3.4
  └── debug@2.2.0 

npm WARN ENOENT ENOENT: no such file or directory, open '/Volumes/Macintosh HD 2/OJH/package.json'
npm WARN EPACKAGEJSON OJH No description
npm WARN EPACKAGEJSON OJH No repository field.
npm WARN EPACKAGEJSON OJH No README data
npm WARN EPACKAGEJSON OJH No license field.
Danniss-Mac-mini:OJH dannis_kenny1115$ sudo npm install grunt-text-replace
/Volumes/Macintosh HD 2/OJH
└── grunt-text-replace@0.4.0 

npm WARN ENOENT ENOENT: no such file or directory, open '/Volumes/Macintosh HD 2/OJH/package.json'
npm WARN EPACKAGEJSON OJH No description
npm WARN EPACKAGEJSON OJH No repository field.
npm WARN EPACKAGEJSON OJH No README data
npm WARN EPACKAGEJSON OJH No license field.
Danniss-Mac-mini:OJH dannis_kenny1115$ sudo npm install karma-chrom-launcher
npm ERR! Darwin 15.2.0
npm ERR! argv "/usr/local/bin/node" "/usr/local/bin/npm" "install" "karma-chrom-launcher"
npm ERR! node v5.4.1
npm ERR! npm  v3.3.12
npm ERR! code E404

npm ERR! 404 Registry returned 404 for GET on https://registry.npmjs.org/karma-chrom-launcher
npm ERR! 404 
npm ERR! 404 'karma-chrom-launcher' is not in the npm registry.
npm ERR! 404 You should bug the author to publish it (or use the name yourself!)
npm ERR! 404 
npm ERR! 404 Note that you can also install from a
npm ERR! 404 tarball, folder, http url, or git url.

npm ERR! Please include the following file with any support request:
npm ERR!     /Volumes/Macintosh HD 2/OJH/npm-debug.log
Danniss-Mac-mini:OJH dannis_kenny1115$ sudo npm install karma-chrome-launcher
/Volumes/Macintosh HD 2/OJH
└─┬ karma-chrome-launcher@0.2.2 
  └─┬ fs-access@1.0.0 
    └── null-check@1.0.0 

npm WARN ENOENT ENOENT: no such file or directory, open '/Volumes/Macintosh HD 2/OJH/package.json'
npm WARN EPACKAGEJSON OJH No description
npm WARN EPACKAGEJSON OJH No repository field.
npm WARN EPACKAGEJSON OJH No README data
npm WARN EPACKAGEJSON OJH No license field.
Danniss-Mac-mini:OJH dannis_kenny1115$ sudo npm install karma-coverage
/Volumes/Macintosh HD 2/OJH
├─┬ grunt-contrib-csslint@0.5.0
│ └── lodash@3.10.1 
├─┬ grunt-contrib-cssmin@0.14.0
│ └─┬ clean-css@3.4.9
│   └── source-map@0.4.4 
├─┬ grunt-contrib-jshint@0.11.3
│ └─┬ jshint@2.8.0
│   └── minimatch@2.0.10 
├─┬ grunt-contrib-uglify@0.11.0
│ ├── lodash@3.10.1 
│ └─┬ uglify-js@2.6.1
│   └── source-map@0.5.3 
├─┬ grunt-karma@0.12.1
│ └── lodash@3.10.1 
├─┬ grunt-ng-annotate@1.0.1
│ └─┬ ng-annotate@1.2.1
│   └── source-map@0.5.3 
├─┬ grunt-node-inspector@0.4.1
│ └─┬ node-inspector@0.12.5
│   ├─┬ biased-opener@0.2.7
│   │ └─┬ browser-launcher2@0.4.6
│   │   └─┬ plist@1.2.0
│   │     └─┬ xmlbuilder@4.0.0
│   │       └── lodash@3.10.1 
│   ├─┬ glob@5.0.15 
│   │ └── minimatch@3.0.0 
│   ├─┬ v8-debug@0.7.0
│   │ └─┬ node-pre-gyp@0.6.19
│   │   ├── nopt@3.0.6 
│   │   ├─┬ rimraf@2.5.0
│   │   │ └─┬ glob@6.0.3
│   │   │   └── minimatch@3.0.0 
│   │   └─┬ tar-pack@3.1.2
│   │     ├─┬ fstream-ignore@1.0.3
│   │     │ └── minimatch@3.0.0 
│   │     └─┬ rimraf@2.4.5
│   │       └─┬ glob@6.0.3
│   │         └── minimatch@3.0.0 
│   └── which@1.2.1 
├─┬ grunt-nodemon@0.4.1
│ └─┬ nodemon@1.8.1
│   └── minimatch@3.0.0 
├─┬ karma@0.13.19
│ ├─┬ chokidar@1.4.2
│ │ ├─┬ fsevents@1.0.6
│ │ │ └─┬ node-pre-gyp@0.6.17
│ │ │   ├── nopt@3.0.6 
│ │ │   ├─┬ rimraf@2.4.4
│ │ │   │ └─┬ glob@5.0.15 
│ │ │   │   └── minimatch@3.0.0 
│ │ │   └─┬ tar-pack@3.1.0
│ │ │     └─┬ fstream-ignore@1.0.3
│ │ │       └── minimatch@3.0.0 
│ │ └─┬ readdirp@2.0.0
│ │   └── minimatch@2.0.10 
│ ├── lodash@3.10.1 
│ ├── minimatch@3.0.0 
│ └── source-map@0.5.3 
└─┬ karma-coverage@0.5.3 
  ├── dateformat@1.0.12 
  ├─┬ istanbul@0.4.2 
  │ ├─┬ escodegen@1.7.1 
  │ │ ├── esprima@1.2.5 
  │ │ ├── estraverse@1.9.3 
  │ │ ├── esutils@2.0.2 
  │ │ ├─┬ optionator@0.5.0 
  │ │ │ ├── deep-is@0.1.3 
  │ │ │ ├── fast-levenshtein@1.0.7 
  │ │ │ ├── levn@0.2.5 
  │ │ │ ├── prelude-ls@1.1.2 
  │ │ │ └── type-check@0.3.2 
  │ │ └── source-map@0.2.0 
  │ ├── esprima@2.7.1 
  │ ├─┬ fileset@0.2.1 
  │ │ ├── glob@5.0.15 
  │ │ └── minimatch@2.0.10 
  │ ├─┬ handlebars@4.0.5 
  │ │ └── source-map@0.4.4 
  │ ├─┬ js-yaml@3.5.2 
  │ │ └─┬ argparse@1.0.3 
  │ │   ├── lodash@3.10.1 
  │ │   └── sprintf-js@1.0.3 
  │ ├── nopt@3.0.6 
  │ ├── resolve@1.1.6 
  │ ├─┬ supports-color@3.1.2 
  │ │ └── has-flag@1.0.0 
  │ ├── which@1.2.1 
  │ └── wordwrap@1.0.0 
  ├── minimatch@3.0.0 
  └── source-map@0.5.3 

npm WARN ENOENT ENOENT: no such file or directory, open '/Volumes/Macintosh HD 2/OJH/package.json'
npm WARN EPACKAGEJSON OJH No description
npm WARN EPACKAGEJSON OJH No repository field.
npm WARN EPACKAGEJSON OJH No README data
npm WARN EPACKAGEJSON OJH No license field.
Danniss-Mac-mini:OJH dannis_kenny1115$ sudo npm install karma-firefox-launcher
/Volumes/Macintosh HD 2/OJH
└── karma-firefox-launcher@0.1.7 

npm WARN ENOENT ENOENT: no such file or directory, open '/Volumes/Macintosh HD 2/OJH/package.json'
npm WARN EPACKAGEJSON OJH No description
npm WARN EPACKAGEJSON OJH No repository field.
npm WARN EPACKAGEJSON OJH No README data
npm WARN EPACKAGEJSON OJH No license field.
Danniss-Mac-mini:OJH dannis_kenny1115$ sudo npm install karma-jasmine
/Volumes/Macintosh HD 2/OJH
├── UNMET PEER DEPENDENCY jasmine-core@*
└── karma-jasmine@0.3.6 

npm WARN ENOENT ENOENT: no such file or directory, open '/Volumes/Macintosh HD 2/OJH/package.json'
npm WARN EPEERINVALID karma-jasmine@0.3.6 requires a peer of jasmine-core@* but none was installed.
npm WARN EPACKAGEJSON OJH No description
npm WARN EPACKAGEJSON OJH No repository field.
npm WARN EPACKAGEJSON OJH No README data
npm WARN EPACKAGEJSON OJH No license field.
Danniss-Mac-mini:OJH dannis_kenny1115$ sudo npm install jasmin-core
npm ERR! Darwin 15.2.0
npm ERR! argv "/usr/local/bin/node" "/usr/local/bin/npm" "install" "jasmin-core"
npm ERR! node v5.4.1
npm ERR! npm  v3.3.12
npm ERR! code E404

npm ERR! 404 Registry returned 404 for GET on https://registry.npmjs.org/jasmin-core
npm ERR! 404 
npm ERR! 404 'jasmin-core' is not in the npm registry.
npm ERR! 404 You should bug the author to publish it (or use the name yourself!)
npm ERR! 404 
npm ERR! 404 Note that you can also install from a
npm ERR! 404 tarball, folder, http url, or git url.

npm ERR! Please include the following file with any support request:
npm ERR!     /Volumes/Macintosh HD 2/OJH/npm-debug.log
Danniss-Mac-mini:OJH dannis_kenny1115$ sudo npm install jasmine-core
/Volumes/Macintosh HD 2/OJH
└── jasmine-core@2.4.1 

npm WARN ENOENT ENOENT: no such file or directory, open '/Volumes/Macintosh HD 2/OJH/package.json'
npm WARN EPACKAGEJSON OJH No description
npm WARN EPACKAGEJSON OJH No repository field.
npm WARN EPACKAGEJSON OJH No README data
npm WARN EPACKAGEJSON OJH No license field.
Danniss-Mac-mini:OJH dannis_kenny1115$ sudo npm install karma-phantomjs-launcher
/Volumes/Macintosh HD 2/OJH
├─┬ grunt-contrib-csslint@0.5.0
│ └── lodash@3.10.1 
├─┬ grunt-contrib-uglify@0.11.0
│ └── lodash@3.10.1 
├─┬ grunt-karma@0.12.1
│ └── lodash@3.10.1 
├─┬ grunt-node-inspector@0.4.1
│ └─┬ node-inspector@0.12.5
│   └─┬ biased-opener@0.2.7
│     └─┬ browser-launcher2@0.4.6
│       └─┬ plist@1.2.0
│         └─┬ xmlbuilder@4.0.0
│           └── lodash@3.10.1 
├─┬ karma@0.13.19
│ └── lodash@3.10.1 
├─┬ karma-coverage@0.5.3
│ └─┬ istanbul@0.4.2
│   └─┬ js-yaml@3.5.2
│     └─┬ argparse@1.0.3
│       └── lodash@3.10.1 
├─┬ karma-phantomjs-launcher@0.2.3 
│ └── lodash@3.10.1 
└── UNMET PEER DEPENDENCY phantomjs@>=1.9

npm WARN ENOENT ENOENT: no such file or directory, open '/Volumes/Macintosh HD 2/OJH/package.json'
npm WARN EPEERINVALID karma-phantomjs-launcher@0.2.3 requires a peer of phantomjs@>=1.9 but none was installed.
npm WARN EPACKAGEJSON OJH No description
npm WARN EPACKAGEJSON OJH No repository field.
npm WARN EPACKAGEJSON OJH No README data
npm WARN EPACKAGEJSON OJH No license field.
Danniss-Mac-mini:OJH dannis_kenny1115$ sudo npm install phantomjs
npm WARN deprecated npmconf@2.1.1: this package has been reintegrated into npm and is now out of date with respect to npm

> phantomjs@1.9.19 install /Volumes/Macintosh HD 2/OJH/node_modules/phantomjs
> node install.js

PhantomJS not found on PATH
Downloading https://bitbucket.org/ariya/phantomjs/downloads/phantomjs-1.9.8-macosx.zip
Saving to /Volumes/Macintosh HD 2/OJH/node_modules/phantomjs/phantomjs/phantomjs-1.9.8-macosx.zip
Receiving...
  [========================================] 99% 0.0s
Received 9187K total.
Extracting zip contents
Removing /Volumes/Macintosh HD 2/OJH/node_modules/phantomjs/lib/phantom
Copying extracted folder /Volumes/Macintosh HD 2/OJH/node_modules/phantomjs/phantomjs/phantomjs-1.9.8-macosx.zip-extract-1452703194961/phantomjs-1.9.8-macosx -> /Volumes/Macintosh HD 2/OJH/node_modules/phantomjs/lib/phantom
Writing location.js file
Done. Phantomjs binary available at /Volumes/Macintosh HD 2/OJH/node_modules/phantomjs/lib/phantom/bin/phantomjs
/Volumes/Macintosh HD 2/OJH
├─┬ grunt-node-inspector@0.4.1
│ └─┬ node-inspector@0.12.5
│   ├── async@0.9.2 
│   ├── semver@4.3.6 
│   └─┬ v8-debug@0.7.0
│     └─┬ node-pre-gyp@0.6.19
│       ├── nopt@3.0.6 
│       └─┬ request@2.67.0
│         ├─┬ http-signature@1.1.0
│         │ ├── assert-plus@0.1.5 
│         │ └─┬ sshpk@1.7.2
│         │   └─┬ dashdash@1.11.0
│         │     └── assert-plus@0.1.5 
│         ├── json-stringify-safe@5.0.1 
│         ├── node-uuid@1.4.7 
│         ├── stringstream@0.0.5 
│         ├── tough-cookie@2.2.1 
│         └── tunnel-agent@0.4.2 
├─┬ karma@0.13.19
│ ├─┬ chokidar@1.4.2
│ │ └─┬ fsevents@1.0.6
│ │   └─┬ node-pre-gyp@0.6.17
│ │     ├── nopt@3.0.6 
│ │     ├─┬ request@2.67.0
│ │     │ ├─┬ http-signature@1.1.0
│ │     │ │ └── assert-plus@0.1.5 
│ │     │ ├── json-stringify-safe@5.0.1 
│ │     │ ├── node-uuid@1.4.7 
│ │     │ ├── stringstream@0.0.5 
│ │     │ └── tough-cookie@2.2.1 
│ │     └─┬ tar-pack@3.1.0
│ │       └── readable-stream@1.0.33 
│ └─┬ log4js@0.6.29
│   ├── readable-stream@1.0.33 
│   └── semver@4.3.6 
├─┬ karma-coverage@0.5.3
│ └─┬ istanbul@0.4.2
│   └── nopt@3.0.6 
└─┬ phantomjs@1.9.19 
  ├── adm-zip@0.4.4 
  ├─┬ fs-extra@0.23.1 
  │ └── jsonfile@2.2.3 
  ├── kew@0.4.0 
  ├─┬ md5@2.0.0 
  │ ├── charenc@0.0.1 
  │ ├── crypt@0.0.1 
  │ └── is-buffer@1.0.2 
  ├─┬ npmconf@2.1.1 
  │ ├─┬ config-chain@1.1.9 
  │ │ └── proto-list@1.2.4 
  │ ├── nopt@3.0.6 
  │ ├── semver@4.3.6 
  │ └── uid-number@0.0.5 
  ├── progress@1.1.8 
  ├─┬ request@2.42.0 
  │ ├── aws-sign2@0.5.0 
  │ ├─┬ bl@0.9.4 
  │ │ └── readable-stream@1.0.33 
  │ ├── caseless@0.6.0 
  │ ├── forever-agent@0.5.2 
  │ ├─┬ form-data@0.1.4 
  │ │ ├── async@0.9.2 
  │ │ ├─┬ combined-stream@0.0.7 
  │ │ │ └── delayed-stream@0.0.5 
  │ │ └── mime@1.2.11 
  │ ├─┬ hawk@1.1.1 
  │ │ ├── boom@0.4.2 
  │ │ ├── cryptiles@0.2.2 
  │ │ ├── hoek@0.9.1 
  │ │ └── sntp@0.2.4 
  │ ├─┬ http-signature@0.10.1 
  │ │ ├── asn1@0.1.11 
  │ │ ├── assert-plus@0.1.5 
  │ │ └── ctype@0.5.3 
  │ ├── json-stringify-safe@5.0.1 
  │ ├── mime-types@1.0.2 
  │ ├── node-uuid@1.4.7 
  │ ├── oauth-sign@0.4.0 
  │ ├── qs@1.2.2 
  │ ├── stringstream@0.0.5 
  │ ├── tough-cookie@2.2.1 
  │ └── tunnel-agent@0.4.2 
  └─┬ request-progress@0.3.1 
    └── throttleit@0.0.2 

npm WARN ENOENT ENOENT: no such file or directory, open '/Volumes/Macintosh HD 2/OJH/package.json'
npm WARN EPACKAGEJSON OJH No description
npm WARN EPACKAGEJSON OJH No repository field.
npm WARN EPACKAGEJSON OJH No README data
npm WARN EPACKAGEJSON OJH No license field.
Danniss-Mac-mini:OJH dannis_kenny1115$ sudo npm install load-grunt-tasks
/Volumes/Macintosh HD 2/OJH
├─┬ grunt-node-inspector@0.4.1
│ └─┬ node-inspector@0.12.5
│   ├─┬ glob@5.0.15
│   │ └── minimatch@3.0.0 
│   └─┬ v8-debug@0.7.0
│     └─┬ node-pre-gyp@0.6.19
│       ├─┬ rimraf@2.5.0
│       │ └─┬ glob@6.0.3
│       │   └── minimatch@3.0.0 
│       └─┬ tar-pack@3.1.2
│         ├─┬ fstream-ignore@1.0.3
│         │ └── minimatch@3.0.0 
│         └─┬ rimraf@2.4.5
│           └─┬ glob@6.0.3
│             └── minimatch@3.0.0 
├─┬ grunt-nodemon@0.4.1
│ └─┬ nodemon@1.8.1
│   └── minimatch@3.0.0 
├─┬ karma@0.13.19
│ ├─┬ chokidar@1.4.2
│ │ └─┬ fsevents@1.0.6
│ │   └─┬ node-pre-gyp@0.6.17
│ │     ├─┬ rimraf@2.4.4
│ │     │ └─┬ glob@5.0.15
│ │     │   └── minimatch@3.0.0 
│ │     └─┬ tar-pack@3.1.0
│ │       └─┬ fstream-ignore@1.0.3
│ │         └── minimatch@3.0.0 
│ └── minimatch@3.0.0 
├─┬ karma-coverage@0.5.3
│ └── minimatch@3.0.0 
└─┬ load-grunt-tasks@3.4.0 
  ├─┬ multimatch@2.1.0 
  │ ├── array-differ@1.0.0 
  │ ├─┬ array-union@1.0.1 
  │ │ └── array-uniq@1.0.2 
  │ └── minimatch@3.0.0 
  ├── pkg-up@1.0.0 
  └─┬ resolve-pkg@0.1.0 
    └── resolve-from@2.0.0 

npm WARN ENOENT ENOENT: no such file or directory, open '/Volumes/Macintosh HD 2/OJH/package.json'
npm WARN EPACKAGEJSON OJH No description
npm WARN EPACKAGEJSON OJH No repository field.
npm WARN EPACKAGEJSON OJH No README data
npm WARN EPACKAGEJSON OJH No license field.
Danniss-Mac-mini:OJH dannis_kenny1115$ sudo npm install should
/Volumes/Macintosh HD 2/OJH
└─┬ should@8.1.1 
  ├── should-equal@0.6.0 
  ├── should-format@0.3.2 
  └── should-type@0.2.0 

npm WARN ENOENT ENOENT: no such file or directory, open '/Volumes/Macintosh HD 2/OJH/package.json'
npm WARN EPACKAGEJSON OJH No description
npm WARN EPACKAGEJSON OJH No repository field.
npm WARN EPACKAGEJSON OJH No README data
npm WARN EPACKAGEJSON OJH No license field.
Danniss-Mac-mini:OJH dannis_kenny1115$ sudo npm install supertest
/Volumes/Macintosh HD 2/OJH
├─┬ grunt-node-inspector@0.4.1
│ └─┬ node-inspector@0.12.5
│   ├── async@0.9.2 
│   ├─┬ biased-opener@0.2.7
│   │ └─┬ browser-launcher2@0.4.6
│   │   └─┬ win-detect-browsers@1.0.2
│   │     └── debug@2.2.0 
│   ├── debug@2.2.0 
│   └─┬ express@4.13.3
│     ├── debug@2.2.0 
│     └─┬ send@0.13.0
│       └── debug@2.2.0 
├─┬ grunt-nodemon@0.4.1
│ └─┬ nodemon@1.8.1
│   └── debug@2.2.0 
├─┬ karma@0.13.19
│ ├─┬ body-parser@1.14.2
│ │ └── debug@2.2.0 
│ ├─┬ connect@3.4.0
│ │ ├── debug@2.2.0 
│ │ └─┬ finalhandler@0.4.0
│ │   └── debug@2.2.0 
│ └─┬ socket.io@1.4.4
│   ├── debug@2.2.0 
│   ├─┬ engine.io@1.6.7
│   │ ├─┬ accepts@1.1.4
│   │ │ └─┬ mime-types@2.0.14 
│   │ │   └── mime-db@1.12.0 
│   │ └── debug@2.2.0 
│   ├─┬ socket.io-adapter@0.4.0
│   │ └── debug@2.2.0 
│   ├─┬ socket.io-client@1.4.4
│   │ ├── debug@2.2.0 
│   │ └─┬ engine.io-client@1.6.7
│   │   └── debug@2.2.0 
│   └─┬ socket.io-parser@2.2.6
│     └── debug@2.2.0 
├─┬ mocha@2.3.4
│ └── debug@2.2.0 
├─┬ phantomjs@1.9.19
│ └─┬ request@2.42.0
│   └─┬ form-data@0.1.4
│     └── async@0.9.2 
└─┬ supertest@1.1.0 
  └─┬ superagent@1.3.0 
    ├── cookiejar@2.0.1 
    ├── debug@2.2.0 
    ├── extend@1.2.1 
    ├─┬ form-data@0.2.0 
    │ ├── async@0.9.2 
    │ └─┬ mime-types@2.0.14 
    │   └── mime-db@1.12.0 
    ├── formidable@1.0.14 
    ├── methods@1.0.1 
    ├── qs@2.3.3 
    ├── readable-stream@1.0.27-1 
    └── reduce-component@1.0.1 

npm WARN ENOENT ENOENT: no such file or directory, open '/Volumes/Macintosh HD 2/OJH/package.json'
npm WARN EPACKAGEJSON OJH No description
npm WARN EPACKAGEJSON OJH No repository field.
npm WARN EPACKAGEJSON OJH No README data
npm WARN EPACKAGEJSON OJH No license field.
Danniss-Mac-mini:OJH dannis_kenny1115$ sudo npm install acl
/Volumes/Macintosh HD 2/OJH
├─┬ acl@0.4.9 
│ ├── async@1.4.2 
│ ├── lodash@3.10.1 
│ ├─┬ mongodb@2.1.4 
│ │ ├── UNMET PEER DEPENDENCY kerberos@~0.0
│ │ ├─┬ mongodb-core@1.2.32 
│ │ │ └── bson@0.4.21 
│ │ └── readable-stream@1.0.31 
│ └─┬ redis@2.4.2 
│   ├── double-ended-queue@2.1.0-0 
│   └── redis-commands@1.0.2 
├─┬ grunt-contrib-csslint@0.5.0
│ └── lodash@3.10.1 
├─┬ grunt-contrib-uglify@0.11.0
│ └── lodash@3.10.1 
├─┬ grunt-karma@0.12.1
│ └── lodash@3.10.1 
├─┬ grunt-node-inspector@0.4.1
│ └─┬ node-inspector@0.12.5
│   └─┬ biased-opener@0.2.7
│     └─┬ browser-launcher2@0.4.6
│       └─┬ plist@1.2.0
│         └─┬ xmlbuilder@4.0.0
│           └── lodash@3.10.1 
├─┬ karma@0.13.19
│ └── lodash@3.10.1 
├─┬ karma-coverage@0.5.3
│ └─┬ istanbul@0.4.2
│   └─┬ js-yaml@3.5.2
│     └─┬ argparse@1.0.3
│       └── lodash@3.10.1 
└─┬ karma-phantomjs-launcher@0.2.3
  └── lodash@3.10.1 

npm WARN ENOENT ENOENT: no such file or directory, open '/Volumes/Macintosh HD 2/OJH/package.json'
npm WARN EPEERINVALID mongodb-core@1.2.32 requires a peer of kerberos@~0.0 but none was installed.
npm WARN EPACKAGEJSON OJH No description
npm WARN EPACKAGEJSON OJH No repository field.
npm WARN EPACKAGEJSON OJH No README data
npm WARN EPACKAGEJSON OJH No license field.
Danniss-Mac-mini:OJH dannis_kenny1115$ sudo npm install kerberos

> kerberos@0.0.17 install /Volumes/Macintosh HD 2/OJH/node_modules/kerberos
> (node-gyp rebuild) || (exit 0)

  CXX(target) Release/obj.target/kerberos/lib/kerberos.o
./Release/.deps/Release/obj.target/kerberos/lib/kerberos.o.d.raw { dev: 16777219,
  mode: 33188,
  nlink: 1,
  uid: 501,
  gid: 20,
  rdev: 0,
  blksize: 4096,
  ino: 434761,
  size: 1500,
  blocks: 8,
  atime: Wed Jan 13 2016 17:44:11 GMT+0100 (CET),
  mtime: Wed Jan 13 2016 17:44:11 GMT+0100 (CET),
  ctime: Wed Jan 13 2016 17:44:11 GMT+0100 (CET),
  birthtime: Wed Jan 13 2016 17:44:11 GMT+0100 (CET) }
  CXX(target) Release/obj.target/kerberos/lib/worker.o
./Release/.deps/Release/obj.target/kerberos/lib/worker.o.d.raw { dev: 16777219,
  mode: 33188,
  nlink: 1,
  uid: 501,
  gid: 20,
  rdev: 0,
  blksize: 4096,
  ino: 434764,
  size: 1427,
  blocks: 8,
  atime: Wed Jan 13 2016 17:44:12 GMT+0100 (CET),
  mtime: Wed Jan 13 2016 17:44:12 GMT+0100 (CET),
  ctime: Wed Jan 13 2016 17:44:12 GMT+0100 (CET),
  birthtime: Wed Jan 13 2016 17:44:11 GMT+0100 (CET) }
  CC(target) Release/obj.target/kerberos/lib/kerberosgss.o
../lib/kerberosgss.c:509:13: warning: implicit declaration of function 'gss_acquire_cred_impersonate_name' is invalid in C99 [-Wimplicit-function-declaration]
        maj_stat = gss_acquire_cred_impersonate_name(&min_stat,
                   ^
1 warning generated.
./Release/.deps/Release/obj.target/kerberos/lib/kerberosgss.o.d.raw { dev: 16777219,
  mode: 33188,
  nlink: 1,
  uid: 501,
  gid: 20,
  rdev: 0,
  blksize: 4096,
  ino: 434767,
  size: 109,
  blocks: 8,
  atime: Wed Jan 13 2016 17:44:12 GMT+0100 (CET),
  mtime: Wed Jan 13 2016 17:44:12 GMT+0100 (CET),
  ctime: Wed Jan 13 2016 17:44:12 GMT+0100 (CET),
  birthtime: Wed Jan 13 2016 17:44:12 GMT+0100 (CET) }
  CC(target) Release/obj.target/kerberos/lib/base64.o
./Release/.deps/Release/obj.target/kerberos/lib/base64.o.d.raw { dev: 16777219,
  mode: 33188,
  nlink: 1,
  uid: 501,
  gid: 20,
  rdev: 0,
  blksize: 4096,
  ino: 434770,
  size: 74,
  blocks: 8,
  atime: Wed Jan 13 2016 17:44:13 GMT+0100 (CET),
  mtime: Wed Jan 13 2016 17:44:13 GMT+0100 (CET),
  ctime: Wed Jan 13 2016 17:44:13 GMT+0100 (CET),
  birthtime: Wed Jan 13 2016 17:44:13 GMT+0100 (CET) }
  CXX(target) Release/obj.target/kerberos/lib/kerberos_context.o
./Release/.deps/Release/obj.target/kerberos/lib/kerberos_context.o.d.raw { dev: 16777219,
  mode: 33188,
  nlink: 1,
  uid: 501,
  gid: 20,
  rdev: 0,
  blksize: 4096,
  ino: 434773,
  size: 1482,
  blocks: 8,
  atime: Wed Jan 13 2016 17:44:13 GMT+0100 (CET),
  mtime: Wed Jan 13 2016 17:44:13 GMT+0100 (CET),
  ctime: Wed Jan 13 2016 17:44:13 GMT+0100 (CET),
  birthtime: Wed Jan 13 2016 17:44:13 GMT+0100 (CET) }
  SOLINK_MODULE(target) Release/kerberos.node
/Volumes/Macintosh HD 2/OJH
├─┬ grunt-node-inspector@0.4.1
│ └─┬ node-inspector@0.12.5
│   └─┬ v8-profiler@5.5.0
│     └── nan@2.0.9 
└─┬ kerberos@0.0.17 
  └── nan@2.0.9 

npm WARN ENOENT ENOENT: no such file or directory, open '/Volumes/Macintosh HD 2/OJH/package.json'
npm WARN EPACKAGEJSON OJH No description
npm WARN EPACKAGEJSON OJH No repository field.
npm WARN EPACKAGEJSON OJH No README data
npm WARN EPACKAGEJSON OJH No license field.
Danniss-Mac-mini:OJH dannis_kenny1115$ sudo npm install async
/Volumes/Macintosh HD 2/OJH
└── async@1.5.2 

npm WARN ENOENT ENOENT: no such file or directory, open '/Volumes/Macintosh HD 2/OJH/package.json'
npm WARN EPACKAGEJSON OJH No description
npm WARN EPACKAGEJSON OJH No repository field.
npm WARN EPACKAGEJSON OJH No README data
npm WARN EPACKAGEJSON OJH No license field.
Danniss-Mac-mini:OJH dannis_kenny1115$ sudo npm install express
Password:
/Volumes/Macintosh HD 2/OJH
└── express@4.13.3 

npm WARN ENOENT ENOENT: no such file or directory, open '/Volumes/Macintosh HD 2/OJH/package.json'
npm WARN EPACKAGEJSON OJH No description
npm WARN EPACKAGEJSON OJH No repository field.
npm WARN EPACKAGEJSON OJH No README data
npm WARN EPACKAGEJSON OJH No license field.
Danniss-Mac-mini:OJH dannis_kenny1115$ sudo npm install body-parser
Password:
/Volumes/Macintosh HD 2/OJH
└── body-parser@1.14.2 

npm WARN ENOENT ENOENT: no such file or directory, open '/Volumes/Macintosh HD 2/OJH/package.json'
npm WARN EPACKAGEJSON OJH No description
npm WARN EPACKAGEJSON OJH No repository field.
npm WARN EPACKAGEJSON OJH No README data
npm WARN EPACKAGEJSON OJH No license field.
Danniss-Mac-mini:OJH dannis_kenny1115$ sudo npm install bower
/Volumes/Macintosh HD 2/OJH
└─┬ bower@1.7.2 
  └── semver-utils@1.1.1 

npm WARN ENOENT ENOENT: no such file or directory, open '/Volumes/Macintosh HD 2/OJH/package.json'
npm WARN EPACKAGEJSON OJH No description
npm WARN EPACKAGEJSON OJH No repository field.
npm WARN EPACKAGEJSON OJH No README data
npm WARN EPACKAGEJSON OJH No license field.
Danniss-Mac-mini:OJH dannis_kenny1115$ sudo npm install chalk
/Volumes/Macintosh HD 2/OJH
├─┬ bower@1.7.2
│ ├─┬ chalk@1.1.1 
│ │ ├── ansi-styles@2.1.0 
│ │ ├─┬ has-ansi@2.0.0 
│ │ │ └── ansi-regex@2.0.0 
│ │ ├─┬ strip-ansi@3.0.0 
│ │ │ └── ansi-regex@2.0.0 
│ │ └── supports-color@2.0.0 
│ ├─┬ inquirer@0.10.0
│ │ ├── ansi-regex@2.0.0 
│ │ └── strip-ansi@3.0.0 
│ └─┬ update-notifier@0.6.0
│   └─┬ string-length@1.0.1
│     └─┬ strip-ansi@3.0.0 
│       └── ansi-regex@2.0.0 
├─┬ chalk@1.1.1 
│ ├── ansi-styles@2.1.0 
│ ├─┬ has-ansi@2.0.0 
│ │ └── ansi-regex@2.0.0 
│ ├── strip-ansi@3.0.0 
│ └── supports-color@2.0.0 
├─┬ grunt-contrib-concat@0.5.1
│ └─┬ chalk@0.5.1 
│   ├── ansi-styles@1.1.0 
│   ├─┬ has-ansi@0.1.0 
│   │ └── ansi-regex@0.2.1 
│   ├── strip-ansi@0.3.0 
│   └── supports-color@0.2.0 
├─┬ grunt-contrib-csslint@0.5.0
│ └─┬ chalk@1.1.1 
│   ├── ansi-styles@2.1.0 
│   ├─┬ has-ansi@2.0.0 
│   │ └── ansi-regex@2.0.0 
│   ├── strip-ansi@3.0.0 
│   └── supports-color@2.0.0 
├─┬ grunt-contrib-cssmin@0.14.0
│ ├─┬ chalk@1.1.1 
│ │ ├── ansi-styles@2.1.0 
│ │ ├─┬ has-ansi@2.0.0 
│ │ │ └── ansi-regex@2.0.0 
│ │ ├── strip-ansi@3.0.0 
│ │ └── supports-color@2.0.0 
│ └─┬ maxmin@1.1.0
│   └─┬ chalk@1.1.1 
│     ├── ansi-styles@2.1.0 
│     ├─┬ has-ansi@2.0.0 
│     │ └── ansi-regex@2.0.0 
│     ├── strip-ansi@3.0.0 
│     └── supports-color@2.0.0 
├─┬ grunt-contrib-uglify@0.11.0
│ └─┬ chalk@1.1.1 
│   ├── ansi-styles@2.1.0 
│   ├─┬ has-ansi@2.0.0 
│   │ └── ansi-regex@2.0.0 
│   ├── strip-ansi@3.0.0 
│   └── supports-color@2.0.0 
├─┬ grunt-node-inspector@0.4.1
│ └─┬ node-inspector@0.12.5
│   └─┬ v8-debug@0.7.0
│     └─┬ node-pre-gyp@0.6.19
│       └─┬ request@2.67.0
│         └─┬ har-validator@2.0.3
│           └─┬ chalk@1.1.1 
│             ├── ansi-styles@2.1.0 
│             ├─┬ has-ansi@2.0.0 
│             │ └── ansi-regex@2.0.0 
│             ├─┬ strip-ansi@3.0.0 
│             │ └── ansi-regex@2.0.0 
│             └── supports-color@2.0.0 
├─┬ grunt-nodemon@0.4.1
│ └─┬ nodemon@1.8.1
│   └─┬ update-notifier@0.5.0
│     ├─┬ chalk@1.1.1 
│     │ ├── ansi-styles@2.1.0 
│     │ ├─┬ has-ansi@2.0.0 
│     │ │ └── ansi-regex@2.0.0 
│     │ ├── strip-ansi@3.0.0 
│     │ └── supports-color@2.0.0 
│     └─┬ string-length@1.0.1
│       └─┬ strip-ansi@3.0.0 
│         └── ansi-regex@2.0.0 
└─┬ karma@0.13.19
  └─┬ chokidar@1.4.2
    └─┬ fsevents@1.0.6
      └─┬ node-pre-gyp@0.6.17
        └─┬ request@2.67.0
          └─┬ har-validator@2.0.3
            └─┬ chalk@1.1.1 
              ├── ansi-styles@2.1.0 
              ├─┬ has-ansi@2.0.0 
              │ └── ansi-regex@2.0.0 
              ├── strip-ansi@3.0.0 
              └── supports-color@2.0.0 

npm WARN ENOENT ENOENT: no such file or directory, open '/Volumes/Macintosh HD 2/OJH/package.json'
npm WARN EPACKAGEJSON OJH No description
npm WARN EPACKAGEJSON OJH No repository field.
npm WARN EPACKAGEJSON OJH No README data
npm WARN EPACKAGEJSON OJH No license field.
Danniss-Mac-mini:OJH dannis_kenny1115$ sudo npm install compression
/Volumes/Macintosh HD 2/OJH
├─┬ body-parser@1.14.2
│ └── debug@2.2.0 
├─┬ compression@1.6.0 
│ ├─┬ accepts@1.3.0 
│ │ └── negotiator@0.6.0 
│ ├── bytes@2.1.0 
│ ├── compressible@2.0.6 
│ ├── debug@2.2.0 
│ ├── on-headers@1.0.1 
│ └── vary@1.1.0 
├─┬ express@4.13.3
│ ├── debug@2.2.0 
│ ├─┬ finalhandler@0.4.0
│ │ └── debug@2.2.0 
│ └─┬ send@0.13.0
│   └── debug@2.2.0 
├─┬ grunt-node-inspector@0.4.1
│ └─┬ node-inspector@0.12.5
│   ├─┬ biased-opener@0.2.7
│   │ └─┬ browser-launcher2@0.4.6
│   │   └─┬ win-detect-browsers@1.0.2
│   │     └── debug@2.2.0 
│   └── debug@2.2.0 
├─┬ grunt-nodemon@0.4.1
│ └─┬ nodemon@1.8.1
│   └── debug@2.2.0 
├─┬ karma@0.13.19
│ ├─┬ connect@3.4.0
│ │ └── debug@2.2.0 
│ └─┬ socket.io@1.4.4
│   ├── debug@2.2.0 
│   ├─┬ engine.io@1.6.7
│   │ └── debug@2.2.0 
│   ├─┬ socket.io-adapter@0.4.0
│   │ └── debug@2.2.0 
│   ├─┬ socket.io-client@1.4.4
│   │ ├── debug@2.2.0 
│   │ └─┬ engine.io-client@1.6.7
│   │   └── debug@2.2.0 
│   └─┬ socket.io-parser@2.2.6
│     └── debug@2.2.0 
├─┬ mocha@2.3.4
│ └── debug@2.2.0 
└─┬ supertest@1.1.0
  └─┬ superagent@1.3.0
    └── debug@2.2.0 

npm WARN ENOENT ENOENT: no such file or directory, open '/Volumes/Macintosh HD 2/OJH/package.json'
npm WARN EPACKAGEJSON OJH No description
npm WARN EPACKAGEJSON OJH No repository field.
npm WARN EPACKAGEJSON OJH No README data
npm WARN EPACKAGEJSON OJH No license field.
Danniss-Mac-mini:OJH dannis_kenny1115$ sudo npm install connect-flash
/Volumes/Macintosh HD 2/OJH
└── connect-flash@0.1.1 

npm WARN ENOENT ENOENT: no such file or directory, open '/Volumes/Macintosh HD 2/OJH/package.json'
npm WARN EPACKAGEJSON OJH No description
npm WARN EPACKAGEJSON OJH No repository field.
npm WARN EPACKAGEJSON OJH No README data
npm WARN EPACKAGEJSON OJH No license field.
Danniss-Mac-mini:OJH dannis_kenny1115$ sudo npm install connect-mongo
/Volumes/Macintosh HD 2/OJH
└── connect-mongo@1.1.0 

npm WARN ENOENT ENOENT: no such file or directory, open '/Volumes/Macintosh HD 2/OJH/package.json'
npm WARN EPACKAGEJSON OJH No description
npm WARN EPACKAGEJSON OJH No repository field.
npm WARN EPACKAGEJSON OJH No README data
npm WARN EPACKAGEJSON OJH No license field.
Danniss-Mac-mini:OJH dannis_kenny1115$ sudo npm install connect-multiparty
/Volumes/Macintosh HD 2/OJH
├─┬ connect-multiparty@2.0.0 
│ ├─┬ multiparty@4.1.2 
│ │ └─┬ fd-slicer@1.0.1 
│ │   └── pend@1.2.0 
│ └── qs@4.0.0 
└─┬ express@4.13.3
  └── qs@4.0.0 

npm WARN ENOENT ENOENT: no such file or directory, open '/Volumes/Macintosh HD 2/OJH/package.json'
npm WARN EPACKAGEJSON OJH No description
npm WARN EPACKAGEJSON OJH No repository field.
npm WARN EPACKAGEJSON OJH No README data
npm WARN EPACKAGEJSON OJH No license field.
Danniss-Mac-mini:OJH dannis_kenny1115$ sudo npm install consolidate
/Volumes/Macintosh HD 2/OJH
└── consolidate@0.13.1 

npm WARN ENOENT ENOENT: no such file or directory, open '/Volumes/Macintosh HD 2/OJH/package.json'
npm WARN EPACKAGEJSON OJH No description
npm WARN EPACKAGEJSON OJH No repository field.
npm WARN EPACKAGEJSON OJH No README data
npm WARN EPACKAGEJSON OJH No license field.
Danniss-Mac-mini:OJH dannis_kenny1115$ sudo npm install cookie-parser
/Volumes/Macintosh HD 2/OJH
└─┬ cookie-parser@1.4.1 
  └── cookie@0.2.3 

npm WARN ENOENT ENOENT: no such file or directory, open '/Volumes/Macintosh HD 2/OJH/package.json'
npm WARN EPACKAGEJSON OJH No description
npm WARN EPACKAGEJSON OJH No repository field.
npm WARN EPACKAGEJSON OJH No README data
npm WARN EPACKAGEJSON OJH No license field.
Danniss-Mac-mini:OJH dannis_kenny1115$ sudo npm install date-utils
/Volumes/Macintosh HD 2/OJH
└── date-utils@1.2.17 

npm WARN ENOENT ENOENT: no such file or directory, open '/Volumes/Macintosh HD 2/OJH/package.json'
npm WARN EPACKAGEJSON OJH No description
npm WARN EPACKAGEJSON OJH No repository field.
npm WARN EPACKAGEJSON OJH No README data
npm WARN EPACKAGEJSON OJH No license field.
Danniss-Mac-mini:OJH dannis_kenny1115$ sudo npm install express-minify
/Volumes/Macintosh HD 2/OJH
└─┬ express-minify@0.1.6 
  └── cssmin@0.4.3 

npm WARN ENOENT ENOENT: no such file or directory, open '/Volumes/Macintosh HD 2/OJH/package.json'
npm WARN EPACKAGEJSON OJH No description
npm WARN EPACKAGEJSON OJH No repository field.
npm WARN EPACKAGEJSON OJH No README data
npm WARN EPACKAGEJSON OJH No license field.
Danniss-Mac-mini:OJH dannis_kenny1115$ sudo npm install express-session
/Volumes/Macintosh HD 2/OJH
├─┬ body-parser@1.14.2
│ └── debug@2.2.0 
├─┬ compression@1.6.0
│ └── debug@2.2.0 
├─┬ cookie-parser@1.4.1
│ └── cookie@0.2.3 
├─┬ express@4.13.3
│ ├── debug@2.2.0 
│ ├─┬ finalhandler@0.4.0
│ │ └── debug@2.2.0 
│ └─┬ send@0.13.0
│   └── debug@2.2.0 
├─┬ express-session@1.13.0 
│ ├── cookie@0.2.3 
│ ├── crc@3.4.0 
│ ├── debug@2.2.0 
│ └─┬ uid-safe@2.0.0 
│   └── base64-url@1.2.1 
├─┬ grunt-node-inspector@0.4.1
│ └─┬ node-inspector@0.12.5
│   ├─┬ biased-opener@0.2.7
│   │ └─┬ browser-launcher2@0.4.6
│   │   └─┬ win-detect-browsers@1.0.2
│   │     └── debug@2.2.0 
│   └── debug@2.2.0 
├─┬ grunt-nodemon@0.4.1
│ └─┬ nodemon@1.8.1
│   └── debug@2.2.0 
├─┬ karma@0.13.19
│ ├─┬ connect@3.4.0
│ │ └── debug@2.2.0 
│ └─┬ socket.io@1.4.4
│   ├── debug@2.2.0 
│   ├─┬ engine.io@1.6.7
│   │ └── debug@2.2.0 
│   ├─┬ socket.io-adapter@0.4.0
│   │ └── debug@2.2.0 
│   ├─┬ socket.io-client@1.4.4
│   │ ├── debug@2.2.0 
│   │ └─┬ engine.io-client@1.6.7
│   │   └── debug@2.2.0 
│   └─┬ socket.io-parser@2.2.6
│     └── debug@2.2.0 
├─┬ mocha@2.3.4
│ └── debug@2.2.0 
└─┬ supertest@1.1.0
  └─┬ superagent@1.3.0
    └── debug@2.2.0 

npm WARN ENOENT ENOENT: no such file or directory, open '/Volumes/Macintosh HD 2/OJH/package.json'
npm WARN EPACKAGEJSON OJH No description
npm WARN EPACKAGEJSON OJH No repository field.
npm WARN EPACKAGEJSON OJH No README data
npm WARN EPACKAGEJSON OJH No license field.
Danniss-Mac-mini:OJH dannis_kenny1115$ sudo npm install file-type
/Volumes/Macintosh HD 2/OJH
└── file-type@3.4.0 

npm WARN ENOENT ENOENT: no such file or directory, open '/Volumes/Macintosh HD 2/OJH/package.json'
npm WARN EPACKAGEJSON OJH No description
npm WARN EPACKAGEJSON OJH No repository field.
npm WARN EPACKAGEJSON OJH No README data
npm WARN EPACKAGEJSON OJH No license field.
Danniss-Mac-mini:OJH dannis_kenny1115$ sudo npm install forever
- minimist@0.0.10 node_modules/flatiron/node_modules/minimist
- minimist@0.0.10 node_modules/nconf/node_modules/minimist
- minimist@0.0.10 node_modules/optimist/node_modules/minimist
/Volumes/Macintosh HD 2/OJH
├─┬ bower@1.7.2
│ ├─┬ bower-config@1.3.0
│ │ └─┬ optimist@0.6.1
│ │   ├── minimist@0.0.10 
│ │   └── wordwrap@0.0.3 
│ ├─┬ fstream-ignore@1.0.3
│ │ └── minimatch@3.0.0 
│ ├─┬ glob@4.5.3
│ │ └── minimatch@2.0.10 
│ ├─┬ handlebars@2.0.0
│ │ └─┬ optimist@0.3.7
│ │   └── wordwrap@0.0.3 
│ ├─┬ insight@0.7.0
│ │ └─┬ os-name@1.0.3
│ │   └─┬ osx-release@1.1.0
│ │     └── minimist@1.2.0 
│ ├─┬ rimraf@2.5.0 
│ │ └─┬ glob@6.0.3
│ │   └── minimatch@3.0.0 
│ ├─┬ shell-quote@1.4.3
│ │ └── jsonify@0.0.0 
│ └─┬ update-notifier@0.6.0
│   └─┬ latest-version@2.0.0
│     └─┬ package-json@2.3.0
│       └─┬ rc@1.1.6
│         └── minimist@1.2.0 
├─┬ express-minify@0.1.6
│ └─┬ uglify-js@2.6.1
│   └─┬ yargs@3.10.0
│     └─┬ cliui@2.1.0
│       └── wordwrap@0.0.2 
├─┬ forever@0.15.1 
│ ├── clone@1.0.2 
│ ├─┬ forever-monitor@1.6.0 
│ │ └── minimatch@2.0.10 
│ ├── object-assign@3.0.0 
│ ├─┬ optimist@0.6.1
│ │ ├── minimist@0.0.10 
│ │ └── wordwrap@0.0.3 
│ ├─┬ prettyjson@1.1.3 
│ │ ├── colors@1.1.2 
│ │ └── minimist@1.2.0 
│ ├─┬ shush@1.0.0 
│ │ ├─┬ caller@0.0.1 
│ │ │ └─┬ tape@2.3.3 
│ │ │   ├── deep-equal@0.1.2 
│ │ │   ├── defined@0.0.0 
│ │ │   ├── jsonify@0.0.0 
│ │ │   └── resumer@0.0.0 
│ │ └── strip-json-comments@0.1.3 
│ └─┬ utile@0.2.1
│   └─┬ rimraf@2.5.0 
│     └─┬ glob@6.0.4 
│       └── minimatch@3.0.0 
├─┬ grunt@0.4.5
│ └── rimraf@2.2.8 
├─┬ grunt-concurrent@2.1.0
│ └─┬ pad-stream@1.2.0
│   └─┬ meow@3.7.0
│     └── minimist@1.2.0 
├─┬ grunt-contrib-jshint@0.11.3
│ └─┬ jshint@2.8.0
│   └── minimatch@2.0.10 
├─┬ grunt-forever@0.4.7
│ └─┬ forever@0.14.2 
│   └─┬ forever-monitor@1.5.2 
│     ├── minimatch@1.0.0 
│     └─┬ watch@0.13.0
│       └── minimist@1.2.0 
├─┬ grunt-node-inspector@0.4.1
│ └─┬ node-inspector@0.12.5
│   ├─┬ biased-opener@0.2.7
│   │ ├─┬ browser-launcher2@0.4.6
│   │ │ └── rimraf@2.2.8 
│   │ └── minimist@1.2.0 
│   ├─┬ glob@5.0.15
│   │ └── minimatch@3.0.0 
│   ├─┬ rc@1.1.6
│   │ └── minimist@1.2.0 
│   └─┬ v8-debug@0.7.0
│     └─┬ node-pre-gyp@0.6.19
│       ├─┬ rc@1.1.6
│       │ └── minimist@1.2.0 
│       ├─┬ rimraf@2.5.0 
│       │ └─┬ glob@6.0.3
│       │   └── minimatch@3.0.0 
│       └─┬ tar-pack@3.1.2
│         ├─┬ fstream-ignore@1.0.3
│         │ └── minimatch@3.0.0 
│         └─┬ rimraf@2.4.5
│           └─┬ glob@6.0.3
│             └── minimatch@3.0.0 
├─┬ grunt-nodemon@0.4.1
│ └─┬ nodemon@1.8.1
│   ├── minimatch@3.0.0 
│   └─┬ update-notifier@0.5.0
│     └─┬ latest-version@1.0.1
│       └─┬ package-json@1.2.0
│         └─┬ got@3.3.1
│           └── object-assign@3.0.0 
├─┬ karma@0.13.19
│ ├─┬ chokidar@1.4.2
│ │ ├─┬ fsevents@1.0.6
│ │ │ └─┬ node-pre-gyp@0.6.17
│ │ │   ├─┬ rc@1.1.5
│ │ │   │ └── minimist@1.2.0 
│ │ │   ├─┬ rimraf@2.4.4
│ │ │   │ └─┬ glob@5.0.15
│ │ │   │   └── minimatch@3.0.0 
│ │ │   └─┬ tar-pack@3.1.0
│ │ │     ├─┬ fstream-ignore@1.0.3
│ │ │     │ └── minimatch@3.0.0 
│ │ │     └── rimraf@2.2.8 
│ │ └─┬ readdirp@2.0.0
│ │   └── minimatch@2.0.10 
│ ├── colors@1.1.2 
│ ├── glob@6.0.4 
│ ├── minimatch@3.0.0 
│ └── rimraf@2.5.0 
├─┬ karma-coverage@0.5.3
│ ├─┬ istanbul@0.4.2
│ │ └─┬ fileset@0.2.1
│ │   └── minimatch@2.0.10 
│ └── minimatch@3.0.0 
└─┬ load-grunt-tasks@3.4.0
  └─┬ multimatch@2.1.0
    └── minimatch@3.0.0 

npm WARN ENOENT ENOENT: no such file or directory, open '/Volumes/Macintosh HD 2/OJH/package.json'
npm WARN EPACKAGEJSON OJH No description
npm WARN EPACKAGEJSON OJH No repository field.
npm WARN EPACKAGEJSON OJH No README data
npm WARN EPACKAGEJSON OJH No license field.
Danniss-Mac-mini:OJH dannis_kenny1115$ sudo npm install glob
/Volumes/Macintosh HD 2/OJH
├─┬ bower@1.7.2
│ ├─┬ fstream-ignore@1.0.3
│ │ └── minimatch@3.0.0 
│ └─┬ rimraf@2.5.0
│   └─┬ glob@6.0.3
│     └── minimatch@3.0.0 
├─┬ forever@0.15.1
│ └─┬ utile@0.2.1
│   └─┬ rimraf@2.5.0
│     └─┬ glob@6.0.4 
│       └── minimatch@3.0.0 
├─┬ glob@6.0.4 
│ └── minimatch@3.0.0 
├─┬ grunt@0.4.5
│ └─┬ findup-sync@0.1.3
│   └─┬ glob@3.2.11 
│     └── minimatch@0.3.0 
├─┬ grunt-contrib-jshint@0.11.3
│ └─┬ jshint@2.8.0
│   └─┬ cli@0.6.6
│     └─┬ glob@3.2.11 
│       └── minimatch@0.3.0 
├─┬ grunt-node-inspector@0.4.1
│ └─┬ node-inspector@0.12.5
│   ├─┬ glob@5.0.15
│   │ └── minimatch@3.0.0 
│   └─┬ v8-debug@0.7.0
│     └─┬ node-pre-gyp@0.6.19
│       ├─┬ rimraf@2.5.0
│       │ └─┬ glob@6.0.3
│       │   └── minimatch@3.0.0 
│       └─┬ tar-pack@3.1.2
│         ├─┬ fstream-ignore@1.0.3
│         │ └── minimatch@3.0.0 
│         └─┬ rimraf@2.4.5
│           └─┬ glob@6.0.3
│             └── minimatch@3.0.0 
├─┬ grunt-nodemon@0.4.1
│ └─┬ nodemon@1.8.1
│   └── minimatch@3.0.0 
├─┬ karma@0.13.19
│ ├─┬ chokidar@1.4.2
│ │ └─┬ fsevents@1.0.6
│ │   └─┬ node-pre-gyp@0.6.17
│ │     ├─┬ rimraf@2.4.4
│ │     │ └─┬ glob@5.0.15
│ │     │   └── minimatch@3.0.0 
│ │     └─┬ tar-pack@3.1.0
│ │       └─┬ fstream-ignore@1.0.3
│ │         └── minimatch@3.0.0 
│ ├── glob@6.0.4 
│ └── minimatch@3.0.0 
├─┬ karma-coverage@0.5.3
│ └── minimatch@3.0.0 
└─┬ load-grunt-tasks@3.4.0
  └─┬ multimatch@2.1.0
    └── minimatch@3.0.0 

npm WARN ENOENT ENOENT: no such file or directory, open '/Volumes/Macintosh HD 2/OJH/package.json'
npm WARN EPACKAGEJSON OJH No description
npm WARN EPACKAGEJSON OJH No repository field.
npm WARN EPACKAGEJSON OJH No README data
npm WARN EPACKAGEJSON OJH No license field.
Danniss-Mac-mini:OJH dannis_kenny1115$ sudo npm install hat
/Volumes/Macintosh HD 2/OJH
└── hat@0.0.3 

npm WARN ENOENT ENOENT: no such file or directory, open '/Volumes/Macintosh HD 2/OJH/package.json'
npm WARN EPACKAGEJSON OJH No description
npm WARN EPACKAGEJSON OJH No repository field.
npm WARN EPACKAGEJSON OJH No README data
npm WARN EPACKAGEJSON OJH No license field.
Danniss-Mac-mini:OJH dannis_kenny1115$ sudo npm install helmet
/Volumes/Macintosh HD 2/OJH
└─┬ helmet@1.1.0 
  ├── dns-prefetch-control@0.1.0 
  ├── dont-sniff-mimetype@1.0.0 
  ├─┬ frameguard@1.0.0 
  │ └── lodash.isstring@3.0.1 
  ├─┬ helmet-csp@1.0.3 
  │ ├── camelize@1.0.0 
  │ ├─┬ content-security-policy-builder@1.0.0 
  │ │ └── dashify@0.2.1 
  │ ├── lodash.isfunction@3.0.6 
  │ ├─┬ lodash.reduce@3.1.2 
  │ │ ├─┬ lodash._basecallback@3.3.1 
  │ │ │ ├─┬ lodash._baseisequal@3.0.7 
  │ │ │ │ └── lodash.istypedarray@3.0.3 
  │ │ │ └── lodash.pairs@3.0.1 
  │ │ ├── lodash._baseeach@3.0.4 
  │ │ └── lodash._basereduce@3.0.1 
  │ ├── lodash.some@3.2.3 
  │ └── platform@1.3.0 
  ├── hide-powered-by@1.0.0 
  ├── hpkp@1.0.0 
  ├── hsts@1.0.0 
  ├── ienoopen@1.0.0 
  ├── nocache@1.0.0 
  └── x-xss-protection@1.0.0 

npm WARN ENOENT ENOENT: no such file or directory, open '/Volumes/Macintosh HD 2/OJH/package.json'
npm WARN EPACKAGEJSON OJH No description
npm WARN EPACKAGEJSON OJH No repository field.
npm WARN EPACKAGEJSON OJH No README data
npm WARN EPACKAGEJSON OJH No license field.
Danniss-Mac-mini:OJH dannis_kenny1115$ sudo npm install imagemagick-native

> imagemagick-native@1.8.0 install /Volumes/Macintosh HD 2/OJH/node_modules/imagemagick-native
> node-gyp rebuild

/bin/sh: Magick++-config: command not found
gyp: Call to 'Magick++-config --ldflags --libs' returned exit status 127. while trying to load binding.gyp
gyp ERR! configure error 
gyp ERR! stack Error: `gyp` failed with exit code: 1
gyp ERR! stack     at ChildProcess.onCpExit (/usr/local/lib/node_modules/npm/node_modules/node-gyp/lib/configure.js:355:16)
gyp ERR! stack     at emitTwo (events.js:87:13)
gyp ERR! stack     at ChildProcess.emit (events.js:172:7)
gyp ERR! stack     at Process.ChildProcess._handle.onexit (internal/child_process.js:200:12)
gyp ERR! System Darwin 15.2.0
gyp ERR! command "/usr/local/bin/node" "/usr/local/lib/node_modules/npm/node_modules/node-gyp/bin/node-gyp.js" "rebuild"
gyp ERR! cwd /Volumes/Macintosh HD 2/OJH/node_modules/imagemagick-native
gyp ERR! node -v v5.4.1
gyp ERR! node-gyp -v v3.0.3
gyp ERR! not ok 
npm WARN ENOENT ENOENT: no such file or directory, open '/Volumes/Macintosh HD 2/OJH/package.json'
npm WARN EPACKAGEJSON OJH No description
npm WARN EPACKAGEJSON OJH No repository field.
npm WARN EPACKAGEJSON OJH No README data
npm WARN EPACKAGEJSON OJH No license field.
npm ERR! Darwin 15.2.0
npm ERR! argv "/usr/local/bin/node" "/usr/local/bin/npm" "install" "imagemagick-native"
npm ERR! node v5.4.1
npm ERR! npm  v3.3.12
npm ERR! code ELIFECYCLE

npm ERR! imagemagick-native@1.8.0 install: `node-gyp rebuild`
npm ERR! Exit status 1
npm ERR! 
npm ERR! Failed at the imagemagick-native@1.8.0 install script 'node-gyp rebuild'.
npm ERR! Make sure you have the latest version of node.js and npm installed.
npm ERR! If you do, this is most likely a problem with the imagemagick-native package,
npm ERR! not with npm itself.
npm ERR! Tell the author that this fails on your system:
npm ERR!     node-gyp rebuild
npm ERR! You can get their info via:
npm ERR!     npm owner ls imagemagick-native
npm ERR! There is likely additional logging output above.

npm ERR! Please include the following file with any support request:
npm ERR!     /Volumes/Macintosh HD 2/OJH/npm-debug.log
Danniss-Mac-mini:OJH dannis_kenny1115$ sudo npm owner ls imagemagic-native
npm ERR! owner ls Couldn't get owner data imagemagic-native
npm ERR! Darwin 15.2.0
npm ERR! argv "/usr/local/bin/node" "/usr/local/bin/npm" "owner" "ls" "imagemagic-native"
npm ERR! node v5.4.1
npm ERR! npm  v3.3.12
npm ERR! code E404

npm ERR! 404 Registry returned 404 for GET on https://registry.npmjs.org/imagemagic-native
npm ERR! 404 
npm ERR! 404 'imagemagic-native' is not in the npm registry.
npm ERR! 404 You should bug the author to publish it (or use the name yourself!)
npm ERR! 404 
npm ERR! 404 Note that you can also install from a
npm ERR! 404 tarball, folder, http url, or git url.

npm ERR! Please include the following file with any support request:
npm ERR!     /Volumes/Macintosh HD 2/OJH/npm-debug.log
Danniss-Mac-mini:OJH dannis_kenny1115$ sudo npm install lodash
/Volumes/Macintosh HD 2/OJH
├─┬ grunt@0.4.5
│ ├─┬ findup-sync@0.1.3
│ │ └── lodash@2.4.2 
│ └─┬ grunt-legacy-log@0.1.3
│   ├─┬ grunt-legacy-log-utils@0.1.1
│   │ └── lodash@2.4.2 
│   └── lodash@2.4.2 
├─┬ grunt-contrib-watch@0.6.1
│ └── lodash@2.4.2 
├─┬ grunt-env@0.4.4
│ └── lodash@2.4.2 
├─┬ grunt-node-inspector@0.4.1
│ └─┬ node-inspector@0.12.5
│   └─┬ biased-opener@0.2.7
│     └─┬ browser-launcher2@0.4.6
│       └── lodash@2.4.2 
└── lodash@4.0.0 

npm WARN ENOENT ENOENT: no such file or directory, open '/Volumes/Macintosh HD 2/OJH/package.json'
npm WARN EPACKAGEJSON OJH No description
npm WARN EPACKAGEJSON OJH No repository field.
npm WARN EPACKAGEJSON OJH No README data
npm WARN EPACKAGEJSON OJH No license field.
Danniss-Mac-mini:OJH dannis_kenny1115$ sudo npm install mean-seo
/Volumes/Macintosh HD 2/OJH
├─┬ bower@1.7.2
│ └─┬ bower-registry-client@1.0.0
│   └── mkdirp@0.3.5 
├─┬ grunt@0.4.5
│ ├─┬ findup-sync@0.1.3
│ │ └── lodash@2.4.2 
│ └─┬ grunt-legacy-log@0.1.3
│   ├─┬ grunt-legacy-log-utils@0.1.1
│   │ └── lodash@2.4.2 
│   └── lodash@2.4.2 
├─┬ grunt-contrib-watch@0.6.1
│ └── lodash@2.4.2 
├─┬ grunt-env@0.4.4
│ └── lodash@2.4.2 
├─┬ grunt-node-inspector@0.4.1
│ └─┬ node-inspector@0.12.5
│   └─┬ biased-opener@0.2.7
│     └─┬ browser-launcher2@0.4.6
│       └── lodash@2.4.2 
└─┬ mean-seo@0.0.8 
  ├── lodash@2.4.2 
  ├── mkdirp@0.3.5 
  └── redis@0.10.3 

npm WARN ENOENT ENOENT: no such file or directory, open '/Volumes/Macintosh HD 2/OJH/package.json'
npm WARN EPACKAGEJSON OJH No description
npm WARN EPACKAGEJSON OJH No repository field.
npm WARN EPACKAGEJSON OJH No README data
npm WARN EPACKAGEJSON OJH No license field.
Danniss-Mac-mini:OJH dannis_kenny1115$ sudo npm install method-override
/Volumes/Macintosh HD 2/OJH
├─┬ body-parser@1.14.2
│ └── debug@2.2.0 
├─┬ compression@1.6.0
│ └── debug@2.2.0 
├─┬ express@4.13.3
│ ├── debug@2.2.0 
│ ├─┬ finalhandler@0.4.0
│ │ └── debug@2.2.0 
│ └─┬ send@0.13.0
│   └── debug@2.2.0 
├─┬ express-session@1.13.0
│ └── debug@2.2.0 
├─┬ grunt-node-inspector@0.4.1
│ └─┬ node-inspector@0.12.5
│   ├─┬ biased-opener@0.2.7
│   │ └─┬ browser-launcher2@0.4.6
│   │   └─┬ win-detect-browsers@1.0.2
│   │     └── debug@2.2.0 
│   └── debug@2.2.0 
├─┬ grunt-nodemon@0.4.1
│ └─┬ nodemon@1.8.1
│   └── debug@2.2.0 
├─┬ helmet@1.1.0
│ └─┬ connect@3.4.0
│   └── debug@2.2.0 
├─┬ karma@0.13.19
│ └─┬ socket.io@1.4.4
│   ├── debug@2.2.0 
│   ├─┬ engine.io@1.6.7
│   │ └── debug@2.2.0 
│   ├─┬ socket.io-adapter@0.4.0
│   │ └── debug@2.2.0 
│   ├─┬ socket.io-client@1.4.4
│   │ ├── debug@2.2.0 
│   │ └─┬ engine.io-client@1.6.7
│   │   └── debug@2.2.0 
│   └─┬ socket.io-parser@2.2.6
│     └── debug@2.2.0 
├─┬ method-override@2.3.5 
│ └── debug@2.2.0 
├─┬ mocha@2.3.4
│ └── debug@2.2.0 
└─┬ supertest@1.1.0
  └─┬ superagent@1.3.0
    └── debug@2.2.0 

npm WARN ENOENT ENOENT: no such file or directory, open '/Volumes/Macintosh HD 2/OJH/package.json'
npm WARN EPACKAGEJSON OJH No description
npm WARN EPACKAGEJSON OJH No repository field.
npm WARN EPACKAGEJSON OJH No README data
npm WARN EPACKAGEJSON OJH No license field.
Danniss-Mac-mini:OJH dannis_kenny1115$ sudo npm install mongoose
/Volumes/Macintosh HD 2/OJH
├─┬ acl@0.4.9
│ └─┬ mongodb@2.1.4
│   ├─┬ mongodb-core@1.2.32
│   │ └── bson@0.4.21 
│   └── readable-stream@1.0.31 
├─┬ body-parser@1.14.2
│ └── debug@2.2.0 
├─┬ compression@1.6.0
│ └── debug@2.2.0 
├─┬ express@4.13.3
│ ├── debug@2.2.0 
│ ├─┬ finalhandler@0.4.0
│ │ └── debug@2.2.0 
│ └─┬ send@0.13.0
│   └── debug@2.2.0 
├─┬ express-session@1.13.0
│ └── debug@2.2.0 
├─┬ grunt-node-inspector@0.4.1
│ └─┬ node-inspector@0.12.5
│   ├─┬ biased-opener@0.2.7
│   │ └─┬ browser-launcher2@0.4.6
│   │   └─┬ win-detect-browsers@1.0.2
│   │     └── debug@2.2.0 
│   └── debug@2.2.0 
├─┬ grunt-nodemon@0.4.1
│ └─┬ nodemon@1.8.1
│   └── debug@2.2.0 
├─┬ helmet@1.1.0
│ └─┬ connect@3.4.0
│   └── debug@2.2.0 
├─┬ karma@0.13.19
│ └─┬ socket.io@1.4.4
│   ├── debug@2.2.0 
│   ├─┬ engine.io@1.6.7
│   │ └── debug@2.2.0 
│   ├─┬ socket.io-adapter@0.4.0
│   │ └── debug@2.2.0 
│   ├─┬ socket.io-client@1.4.4
│   │ ├── debug@2.2.0 
│   │ └─┬ engine.io-client@1.6.7
│   │   └── debug@2.2.0 
│   └─┬ socket.io-parser@2.2.6
│     └── debug@2.2.0 
├─┬ method-override@2.3.5
│ └── debug@2.2.0 
├─┬ mocha@2.3.4
│ └── debug@2.2.0 
├─┬ mongoose@4.3.5 
│ ├── async@0.9.0 
│ ├── bson@0.4.19 
│ ├── hooks-fixed@1.1.0 
│ ├── kareem@1.0.1 
│ ├─┬ mongodb@2.1.2 
│ │ ├─┬ mongodb-core@1.2.30 
│ │ │ └── bson@0.4.21 
│ │ └── readable-stream@1.0.31 
│ ├── mpath@0.1.1 
│ ├── mpromise@0.5.4 
│ ├─┬ mquery@1.6.3 
│ │ ├── bluebird@2.9.26 
│ │ └── debug@2.2.0 
│ ├── muri@1.0.0 
│ ├── regexp-clone@0.0.1 
│ └── sliced@0.0.5 
└─┬ supertest@1.1.0
  └─┬ superagent@1.3.0
    └── debug@2.2.0 

npm WARN ENOENT ENOENT: no such file or directory, open '/Volumes/Macintosh HD 2/OJH/package.json'
npm WARN EPACKAGEJSON OJH No description
npm WARN EPACKAGEJSON OJH No repository field.
npm WARN EPACKAGEJSON OJH No README data
npm WARN EPACKAGEJSON OJH No license field.
Danniss-Mac-mini:OJH dannis_kenny1115$ sudo npm install morgan
/Volumes/Macintosh HD 2/OJH
├─┬ body-parser@1.14.2
│ └── debug@2.2.0 
├─┬ compression@1.6.0
│ └── debug@2.2.0 
├─┬ express@4.13.3
│ ├── debug@2.2.0 
│ ├── depd@1.0.1 
│ ├─┬ finalhandler@0.4.0
│ │ └── debug@2.2.0 
│ └─┬ send@0.13.0
│   ├── debug@2.2.0 
│   └── depd@1.0.1 
├─┬ express-session@1.13.0
│ └── debug@2.2.0 
├─┬ grunt-node-inspector@0.4.1
│ └─┬ node-inspector@0.12.5
│   ├─┬ biased-opener@0.2.7
│   │ └─┬ browser-launcher2@0.4.6
│   │   └─┬ win-detect-browsers@1.0.2
│   │     └── debug@2.2.0 
│   └── debug@2.2.0 
├─┬ grunt-nodemon@0.4.1
│ └─┬ nodemon@1.8.1
│   └── debug@2.2.0 
├─┬ helmet@1.1.0
│ └─┬ connect@3.4.0
│   └── debug@2.2.0 
├─┬ karma@0.13.19
│ └─┬ socket.io@1.4.4
│   ├── debug@2.2.0 
│   ├─┬ engine.io@1.6.7
│   │ └── debug@2.2.0 
│   ├─┬ socket.io-adapter@0.4.0
│   │ └── debug@2.2.0 
│   ├─┬ socket.io-client@1.4.4
│   │ ├── debug@2.2.0 
│   │ └─┬ engine.io-client@1.6.7
│   │   └── debug@2.2.0 
│   └─┬ socket.io-parser@2.2.6
│     └── debug@2.2.0 
├─┬ method-override@2.3.5
│ └── debug@2.2.0 
├─┬ mocha@2.3.4
│ └── debug@2.2.0 
├─┬ mongoose@4.3.5
│ └─┬ mquery@1.6.3
│   └── debug@2.2.0 
├─┬ morgan@1.6.1 
│ ├── basic-auth@1.0.3 
│ ├── debug@2.2.0 
│ └── depd@1.0.1 
└─┬ supertest@1.1.0
  └─┬ superagent@1.3.0
    └── debug@2.2.0 

npm WARN ENOENT ENOENT: no such file or directory, open '/Volumes/Macintosh HD 2/OJH/package.json'
npm WARN EPACKAGEJSON OJH No description
npm WARN EPACKAGEJSON OJH No repository field.
npm WARN EPACKAGEJSON OJH No README data
npm WARN EPACKAGEJSON OJH No license field.
Danniss-Mac-mini:OJH dannis_kenny1115$ sudo npm install nodemailer
/Volumes/Macintosh HD 2/OJH
├─┬ body-parser@1.14.2
│ ├── iconv-lite@0.4.13 
│ └─┬ raw-body@2.1.5
│   └── iconv-lite@0.4.13 
└─┬ nodemailer@2.0.0 
  ├─┬ libmime@2.0.0 
  │ ├── iconv-lite@0.4.13 
  │ ├── libbase64@0.1.0 
  │ └── libqp@1.1.0 
  ├─┬ mailcomposer@3.0.1 
  │ └─┬ buildmail@3.0.1 
  │   └── addressparser@0.3.2 
  ├─┬ nodemailer-direct-transport@2.0.0 
  │ └── smtp-connection@2.0.0 
  ├─┬ nodemailer-shared@1.0.2 
  │ └── nodemailer-fetch@1.0.0 
  ├─┬ nodemailer-smtp-pool@2.0.0 
  │ └── nodemailer-wellknown@0.1.7 
  └── nodemailer-smtp-transport@2.0.0 

npm WARN ENOENT ENOENT: no such file or directory, open '/Volumes/Macintosh HD 2/OJH/package.json'
npm WARN EPACKAGEJSON OJH No description
npm WARN EPACKAGEJSON OJH No repository field.
npm WARN EPACKAGEJSON OJH No README data
npm WARN EPACKAGEJSON OJH No license field.
Danniss-Mac-mini:OJH dannis_kenny1115$ sudo npm install passport
/Volumes/Macintosh HD 2/OJH
└─┬ passport@0.3.2 
  ├── passport-strategy@1.0.0 
  └── pause@0.0.1 

npm WARN ENOENT ENOENT: no such file or directory, open '/Volumes/Macintosh HD 2/OJH/package.json'
npm WARN EPACKAGEJSON OJH No description
npm WARN EPACKAGEJSON OJH No repository field.
npm WARN EPACKAGEJSON OJH No README data
npm WARN EPACKAGEJSON OJH No license field.
Danniss-Mac-mini:OJH dannis_kenny1115$ sudo npm install passport-facebook
/Volumes/Macintosh HD 2/OJH
└─┬ passport-facebook@2.0.0 
  └─┬ passport-oauth2@1.1.2 
    ├── oauth@0.9.14 
    └── uid2@0.0.3 

npm WARN ENOENT ENOENT: no such file or directory, open '/Volumes/Macintosh HD 2/OJH/package.json'
npm WARN EPACKAGEJSON OJH No description
npm WARN EPACKAGEJSON OJH No repository field.
npm WARN EPACKAGEJSON OJH No README data
npm WARN EPACKAGEJSON OJH No license field.
Danniss-Mac-mini:OJH dannis_kenny1115$ sudo npm install passport-github
/Volumes/Macintosh HD 2/OJH
└── passport-github@1.0.0 

npm WARN ENOENT ENOENT: no such file or directory, open '/Volumes/Macintosh HD 2/OJH/package.json'
npm WARN EPACKAGEJSON OJH No description
npm WARN EPACKAGEJSON OJH No repository field.
npm WARN EPACKAGEJSON OJH No README data
npm WARN EPACKAGEJSON OJH No license field.
Danniss-Mac-mini:OJH dannis_kenny1115$ sudo npm install passport-google-oauth
/Volumes/Macintosh HD 2/OJH
└─┬ passport-google-oauth@0.2.0 
  └─┬ passport-oauth@1.0.0 
    └── passport-oauth1@1.0.1 

npm WARN ENOENT ENOENT: no such file or directory, open '/Volumes/Macintosh HD 2/OJH/package.json'
npm WARN EPACKAGEJSON OJH No description
npm WARN EPACKAGEJSON OJH No repository field.
npm WARN EPACKAGEJSON OJH No README data
npm WARN EPACKAGEJSON OJH No license field.
Danniss-Mac-mini:OJH dannis_kenny1115$ sudo npm install passport-linkedin
/Volumes/Macintosh HD 2/OJH
└── passport-linkedin@1.0.0 

npm WARN ENOENT ENOENT: no such file or directory, open '/Volumes/Macintosh HD 2/OJH/package.json'
npm WARN EPACKAGEJSON OJH No description
npm WARN EPACKAGEJSON OJH No repository field.
npm WARN EPACKAGEJSON OJH No README data
npm WARN EPACKAGEJSON OJH No license field.
Danniss-Mac-mini:OJH dannis_kenny1115$ sudo npm install passport-local
/Volumes/Macintosh HD 2/OJH
└── passport-local@1.0.0 

npm WARN ENOENT ENOENT: no such file or directory, open '/Volumes/Macintosh HD 2/OJH/package.json'
npm WARN EPACKAGEJSON OJH No description
npm WARN EPACKAGEJSON OJH No repository field.
npm WARN EPACKAGEJSON OJH No README data
npm WARN EPACKAGEJSON OJH No license field.
Danniss-Mac-mini:OJH dannis_kenny1115$ sudo npm install passport-token-auth
npm WARN deprecated passport-token-auth@0.1.2: Shutdown project
/Volumes/Macintosh HD 2/OJH
└── passport-token-auth@0.1.2 

npm WARN ENOENT ENOENT: no such file or directory, open '/Volumes/Macintosh HD 2/OJH/package.json'
npm WARN EPACKAGEJSON OJH No description
npm WARN EPACKAGEJSON OJH No repository field.
npm WARN EPACKAGEJSON OJH No README data
npm WARN EPACKAGEJSON OJH No license field.
Danniss-Mac-mini:OJH dannis_kenny1115$ sudo npm install passport-twitter
/Volumes/Macintosh HD 2/OJH
└─┬ passport-twitter@1.0.3 
  └── xtraverse@0.1.0 

npm WARN ENOENT ENOENT: no such file or directory, open '/Volumes/Macintosh HD 2/OJH/package.json'
npm WARN EPACKAGEJSON OJH No description
npm WARN EPACKAGEJSON OJH No repository field.
npm WARN EPACKAGEJSON OJH No README data
npm WARN EPACKAGEJSON OJH No license field.
Danniss-Mac-mini:OJH dannis_kenny1115$ sudo npm install promise
/Volumes/Macintosh HD 2/OJH
└─┬ promise@7.1.1 
  └── asap@2.0.3 

npm WARN ENOENT ENOENT: no such file or directory, open '/Volumes/Macintosh HD 2/OJH/package.json'
npm WARN EPACKAGEJSON OJH No description
npm WARN EPACKAGEJSON OJH No repository field.
npm WARN EPACKAGEJSON OJH No README data
npm WARN EPACKAGEJSON OJH No license field.
Danniss-Mac-mini:OJH dannis_kenny1115$ sudo npm install read-chunk
/Volumes/Macintosh HD 2/OJH
└── read-chunk@1.0.1 

npm WARN ENOENT ENOENT: no such file or directory, open '/Volumes/Macintosh HD 2/OJH/package.json'
npm WARN EPACKAGEJSON OJH No description
npm WARN EPACKAGEJSON OJH No repository field.
npm WARN EPACKAGEJSON OJH No README data
npm WARN EPACKAGEJSON OJH No license field.
Danniss-Mac-mini:OJH dannis_kenny1115$ sudo npm install swig
/Volumes/Macintosh HD 2/OJH
├─┬ bower@1.7.2
│ ├─┬ bower-registry-client@1.0.0
│ │ └── async@0.2.10 
│ └─┬ handlebars@2.0.0
│   └─┬ uglify-js@2.3.6
│     └── async@0.2.10 
├─┬ express-minify@0.1.6
│ └─┬ uglify-js@2.6.1
│   ├── async@0.2.10 
│   └─┬ yargs@3.10.0
│     ├── camelcase@1.2.1 
│     └─┬ cliui@2.1.0
│       └── wordwrap@0.0.2 
├─┬ forever@0.15.1
│ ├─┬ flatiron@0.4.3
│ │ └─┬ broadway@0.3.6
│ │   └─┬ winston@0.8.0
│ │     └── async@0.2.10 
│ ├─┬ utile@0.2.1
│ │ └── async@0.2.10 
│ └─┬ winston@0.8.3
│   └── async@0.2.10 
├─┬ grunt-contrib-watch@0.6.1
│ └── async@0.2.10 
├─┬ karma@0.13.19
│ └─┬ log4js@0.6.29
│   └── async@0.2.10 
└─┬ swig@1.4.2 
  └─┬ uglify-js@2.4.24 
    ├── async@0.2.10 
    ├── source-map@0.1.34 
    └─┬ yargs@3.5.4 
      ├── camelcase@1.2.1 
      └── wordwrap@0.0.2 

npm WARN ENOENT ENOENT: no such file or directory, open '/Volumes/Macintosh HD 2/OJH/package.json'
npm WARN EPACKAGEJSON OJH No description
npm WARN EPACKAGEJSON OJH No repository field.
npm WARN EPACKAGEJSON OJH No README data
npm WARN EPACKAGEJSON OJH No license field.
Danniss-Mac-mini:OJH dannis_kenny1115$ sudo npm install uuid
/Volumes/Macintosh HD 2/OJH
├─┬ bower@1.7.2
│ ├─┬ configstore@0.3.2
│ │ └── uuid@2.0.1 
│ ├─┬ insight@0.7.0
│ │ └─┬ configstore@1.4.0
│ │   └── uuid@2.0.1 
│ └─┬ update-notifier@0.6.0
│   └─┬ configstore@1.4.0
│     └── uuid@2.0.1 
└── uuid@2.0.1 

npm WARN ENOENT ENOENT: no such file or directory, open '/Volumes/Macintosh HD 2/OJH/package.json'
npm WARN EPACKAGEJSON OJH No description
npm WARN EPACKAGEJSON OJH No repository field.
npm WARN EPACKAGEJSON OJH No README data
npm WARN EPACKAGEJSON OJH No license field.
Danniss-Mac-mini:OJH dannis_kenny1115$ sudo apt-get install build-essential g++
Password:
sudo: apt-get: command not found
Danniss-Mac-mini:OJH dannis_kenny1115$ sudo port install ImageMagick
sudo: port: command not found
Danniss-Mac-mini:OJH dannis_kenny1115$ sudo yum install ImageMagick-c++ ImageMagick-c++devel
Password:
Sorry, try again.
Password:
sudo: yum: command not found
Danniss-Mac-mini:OJH dannis_kenny1115$ brew
-bash: brew: command not found
Danniss-Mac-mini:OJH dannis_kenny1115$ brew install wget
-bash: brew: command not found
Danniss-Mac-mini:OJH dannis_kenny1115$ imagemagick-c++config
-bash: imagemagick-c++config: command not found
Danniss-Mac-mini:OJH dannis_kenny1115$ ImageMagick
-bash: ImageMagick: command not found
Danniss-Mac-mini:OJH dannis_kenny1115$ sudo npm install imagemagick-native
Password:

> imagemagick-native@1.8.0 install /Volumes/Macintosh HD 2/OJH/node_modules/imagemagick-native
> node-gyp rebuild

/bin/sh: Magick++-config: command not found
gyp: Call to 'Magick++-config --ldflags --libs' returned exit status 127. while trying to load binding.gyp
gyp ERR! configure error 
gyp ERR! stack Error: `gyp` failed with exit code: 1
gyp ERR! stack     at ChildProcess.onCpExit (/usr/local/lib/node_modules/npm/node_modules/node-gyp/lib/configure.js:355:16)
gyp ERR! stack     at emitTwo (events.js:87:13)
gyp ERR! stack     at ChildProcess.emit (events.js:172:7)
gyp ERR! stack     at Process.ChildProcess._handle.onexit (internal/child_process.js:200:12)
gyp ERR! System Darwin 15.2.0
gyp ERR! command "/usr/local/bin/node" "/usr/local/lib/node_modules/npm/node_modules/node-gyp/bin/node-gyp.js" "rebuild"
gyp ERR! cwd /Volumes/Macintosh HD 2/OJH/node_modules/imagemagick-native
gyp ERR! node -v v5.4.1
gyp ERR! node-gyp -v v3.0.3
gyp ERR! not ok 
npm WARN ENOENT ENOENT: no such file or directory, open '/Volumes/Macintosh HD 2/OJH/package.json'
npm WARN EPACKAGEJSON OJH No description
npm WARN EPACKAGEJSON OJH No repository field.
npm WARN EPACKAGEJSON OJH No README data
npm WARN EPACKAGEJSON OJH No license field.
npm ERR! Darwin 15.2.0
npm ERR! argv "/usr/local/bin/node" "/usr/local/bin/npm" "install" "imagemagick-native"
npm ERR! node v5.4.1
npm ERR! npm  v3.3.12
npm ERR! code ELIFECYCLE

npm ERR! imagemagick-native@1.8.0 install: `node-gyp rebuild`
npm ERR! Exit status 1
npm ERR! 
npm ERR! Failed at the imagemagick-native@1.8.0 install script 'node-gyp rebuild'.
npm ERR! Make sure you have the latest version of node.js and npm installed.
npm ERR! If you do, this is most likely a problem with the imagemagick-native package,
npm ERR! not with npm itself.
npm ERR! Tell the author that this fails on your system:
npm ERR!     node-gyp rebuild
npm ERR! You can get their info via:
npm ERR!     npm owner ls imagemagick-native
npm ERR! There is likely additional logging output above.

npm ERR! Please include the following file with any support request:
npm ERR!     /Volumes/Macintosh HD 2/OJH/npm-debug.log
Danniss-Mac-mini:OJH dannis_kenny1115$ env
TERM_PROGRAM=Apple_Terminal
SHELL=/bin/bash
TERM=xterm-256color
TMPDIR=/var/folders/n9/pc_cwv390njf4ldpbnz84q2w0000gn/T/
Apple_PubSub_Socket_Render=/private/tmp/com.apple.launchd.7URhgRt1sX/Render
TERM_PROGRAM_VERSION=361.1
TERM_SESSION_ID=99CE48F2-59AF-4E0F-855A-147FEC2993BF
USER=dannis_kenny1115
SSH_AUTH_SOCK=/private/tmp/com.apple.launchd.tw41OJu7cA/Listeners
__CF_USER_TEXT_ENCODING=0x1F5:0x0:0x0
PATH=/usr/local/bin:/usr/bin:/bin:/usr/sbin:/sbin
PWD=/Volumes/Macintosh HD 2/OJH
LANG=en_US.UTF-8
XPC_FLAGS=0x0
XPC_SERVICE_NAME=0
SHLVL=1
HOME=/Users/dannis_kenny1115
LOGNAME=dannis_kenny1115
_=/usr/bin/env
OLDPWD=/Volumes/Macintosh HD 2
Danniss-Mac-mini:OJH dannis_kenny1115$ clear

Danniss-Mac-mini:OJH dannis_kenny1115$ env | grep PATH
PATH=/usr/local/bin:/usr/bin:/bin:/usr/sbin:/sbin
Danniss-Mac-mini:OJH dannis_kenny1115$ /opt/ImageMagick/
-bash: /opt/ImageMagick/: is a directory
Danniss-Mac-mini:OJH dannis_kenny1115$ cd /opt/ImageMagick/
Danniss-Mac-mini:ImageMagick dannis_kenny1115$ ls
bin	etc	include	lib	share
Danniss-Mac-mini:ImageMagick dannis_kenny1115$ cd bin/
Danniss-Mac-mini:bin dannis_kenny1115$ ls
Magick++-config		MagickCore-config	Wand-config		compare			conjure			display			import			montage
Magick-config		MagickWand-config	animate			composite		convert			identify		mogrify			stream
Danniss-Mac-mini:bin dannis_kenny1115$ ls -l
total 304
-rwxr-xr-x  1 root  wheel  1269 Mar 22  2015 Magick++-config
-rwxr-xr-x  1 root  wheel  1235 Mar 22  2015 Magick-config
-rwxr-xr-x  1 root  wheel  1243 Mar 22  2015 MagickCore-config
-rwxr-xr-x  1 root  wheel  1483 Mar 22  2015 MagickWand-config
-rwxr-xr-x  1 root  wheel  1230 Mar 22  2015 Wand-config
-rwxr-xr-x  1 root  wheel  8868 Mar 22  2015 animate
-rwxr-xr-x  1 root  wheel  8988 Mar 22  2015 compare
-rwxr-xr-x  1 root  wheel  8876 Mar 22  2015 composite
-rwxr-xr-x  1 root  wheel  8868 Mar 22  2015 conjure
-rwxr-xr-x  1 root  wheel  8868 Mar 22  2015 convert
-rwxr-xr-x  1 root  wheel  8868 Mar 22  2015 display
-rwxr-xr-x  1 root  wheel  8916 Mar 22  2015 identify
-rwxr-xr-x  1 root  wheel  8868 Mar 22  2015 import
-rwxr-xr-x  1 root  wheel  8868 Mar 22  2015 mogrify
-rwxr-xr-x  1 root  wheel  8868 Mar 22  2015 montage
-rwxr-xr-x  1 root  wheel  8868 Mar 22  2015 stream
Danniss-Mac-mini:bin dannis_kenny1115$ env | grep PATH
PATH=/usr/local/bin:/usr/bin:/bin:/usr/sbin:/sbin
Danniss-Mac-mini:bin dannis_kenny1115$ export PATH=$PATH:/opt/ImageMagick/
Danniss-Mac-mini:bin dannis_kenny1115$ pwd
/opt/ImageMagick/bin
Danniss-Mac-mini:bin dannis_kenny1115$ ls -l
total 304
-rwxr-xr-x  1 root  wheel  1269 Mar 22  2015 Magick++-config
-rwxr-xr-x  1 root  wheel  1235 Mar 22  2015 Magick-config
-rwxr-xr-x  1 root  wheel  1243 Mar 22  2015 MagickCore-config
-rwxr-xr-x  1 root  wheel  1483 Mar 22  2015 MagickWand-config
-rwxr-xr-x  1 root  wheel  1230 Mar 22  2015 Wand-config
-rwxr-xr-x  1 root  wheel  8868 Mar 22  2015 animate
-rwxr-xr-x  1 root  wheel  8988 Mar 22  2015 compare
-rwxr-xr-x  1 root  wheel  8876 Mar 22  2015 composite
-rwxr-xr-x  1 root  wheel  8868 Mar 22  2015 conjure
-rwxr-xr-x  1 root  wheel  8868 Mar 22  2015 convert
-rwxr-xr-x  1 root  wheel  8868 Mar 22  2015 display
-rwxr-xr-x  1 root  wheel  8916 Mar 22  2015 identify
-rwxr-xr-x  1 root  wheel  8868 Mar 22  2015 import
-rwxr-xr-x  1 root  wheel  8868 Mar 22  2015 mogrify
-rwxr-xr-x  1 root  wheel  8868 Mar 22  2015 montage
-rwxr-xr-x  1 root  wheel  8868 Mar 22  2015 stream
Danniss-Mac-mini:bin dannis_kenny1115$ ./Magick++-config 
Usage: Magick++-config [--cppflags] [--cxxflags] [--exec-prefix] [--ldflags] [--libs] [--prefix] [--version]

 For example, "magick.cpp" may be compiled to produce "magick" as follows:

  "c++ -o magick magick.cpp `Magick++-config --cppflags --cxxflags --ldflags --libs`"
Danniss-Mac-mini:bin dannis_kenny1115$ clear

Danniss-Mac-mini:bin dannis_kenny1115$ env | grep PATH
PATH=/usr/local/bin:/usr/bin:/bin:/usr/sbin:/sbin:/opt/ImageMagick/
Danniss-Mac-mini:bin dannis_kenny1115$ export PATH=/usr/local/bin:/usr/bin:/bin:/usr/sbin:/sbin:/opt/ImageMagick/bin/
Danniss-Mac-mini:bin dannis_kenny1115$ M
Magick++-config    Magick-config      MagickCore-config  MagickWand-config  MergePef           MvMac              
Danniss-Mac-mini:bin dannis_kenny1115$ M
Magick++-config    Magick-config      MagickCore-config  MagickWand-config  MergePef           MvMac              
Danniss-Mac-mini:bin dannis_kenny1115$ Magick++-config 
Usage: Magick++-config [--cppflags] [--cxxflags] [--exec-prefix] [--ldflags] [--libs] [--prefix] [--version]

 For example, "magick.cpp" may be compiled to produce "magick" as follows:

  "c++ -o magick magick.cpp `Magick++-config --cppflags --cxxflags --ldflags --libs`"
Danniss-Mac-mini:bin dannis_kenny1115$ ls
Magick++-config		MagickCore-config	Wand-config		compare			conjure			display			import			montage
Magick-config		MagickWand-config	animate			composite		convert			identify		mogrify			stream
Danniss-Mac-mini:bin dannis_kenny1115$ cd ..
Danniss-Mac-mini:ImageMagick dannis_kenny1115$ cd ..
Danniss-Mac-mini:opt dannis_kenny1115$ ls
ImageMagick
Danniss-Mac-mini:opt dannis_kenny1115$ cd ..
Danniss-Mac-mini:/ dannis_kenny1115$ ls
Applications			EFIROOTDIR			Users				dev				net				tmp
Clover_Install_Log.txt		Library				Volumes				etc				opt				usr
EFI				Network				bin				home				private				var
EFI-Backups			System				cores				installer.failurerequests	sbin
Danniss-Mac-mini:/ dannis_kenny1115$ cd ..
Danniss-Mac-mini:/ dannis_kenny1115$ ls
Applications			EFIROOTDIR			Users				dev				net				tmp
Clover_Install_Log.txt		Library				Volumes				etc				opt				usr
EFI				Network				bin				home				private				var
EFI-Backups			System				cores				installer.failurerequests	sbin
Danniss-Mac-mini:/ dannis_kenny1115$ cd ..
Danniss-Mac-mini:/ dannis_kenny1115$ ls
Applications			EFIROOTDIR			Users				dev				net				tmp
Clover_Install_Log.txt		Library				Volumes				etc				opt				usr
EFI				Network				bin				home				private				var
EFI-Backups			System				cores				installer.failurerequests	sbin
Danniss-Mac-mini:/ dannis_kenny1115$ cd home
Danniss-Mac-mini:home dannis_kenny1115$ ls
Danniss-Mac-mini:home dannis_kenny1115$ cd ..
Danniss-Mac-mini:/ dannis_kenny1115$ cd usr
Danniss-Mac-mini:usr dannis_kenny1115$ ls
X11		X11R6		adic		bin		lib		libexec		local		sbin		share		standalone
Danniss-Mac-mini:usr dannis_kenny1115$ cd ..
Danniss-Mac-mini:/ dannis_kenny1115$ cd Users
Danniss-Mac-mini:Users dannis_kenny1115$ ls
Deleted Users		Shared			dannis_kenny1115	mini			mini (Deleted)
Danniss-Mac-mini:Users dannis_kenny1115$ cd /Volumes/Macintosh\ HD\ 2/
Danniss-Mac-mini:Macintosh HD 2 dannis_kenny1115$ ls -l
total 0
drwxr-xr-x  4 dannis_kenny1115  staff  136 Jan 13 22:10 OJH
drwxr-xr-x  4 dannis_kenny1115  staff  136 Nov  4 14:06 Programs
drwxr-xr-x  8 dannis_kenny1115  staff  272 Dec 24 03:03 Sandroworks
drwxr-xr-x  6 dannis_kenny1115  staff  204 Dec 17 21:50 Source
Danniss-Mac-mini:Macintosh HD 2 dannis_kenny1115$ ls
OJH		Programs	Sandroworks	Source
Danniss-Mac-mini:Macintosh HD 2 dannis_kenny1115$ cd OJH
Danniss-Mac-mini:OJH dannis_kenny1115$ sudo npm install imagemagick-native
Password:

> imagemagick-native@1.8.0 install /Volumes/Macintosh HD 2/OJH/node_modules/imagemagick-native
> node-gyp rebuild

/opt/ImageMagick/bin/Magick++-config: line 53: pkg-config: command not found
/opt/ImageMagick/bin/Magick++-config: line 56: pkg-config: command not found
gyp: Call to 'Magick++-config --ldflags --libs' returned exit status 0. while trying to load binding.gyp
gyp ERR! configure error 
gyp ERR! stack Error: `gyp` failed with exit code: 1
gyp ERR! stack     at ChildProcess.onCpExit (/usr/local/lib/node_modules/npm/node_modules/node-gyp/lib/configure.js:355:16)
gyp ERR! stack     at emitTwo (events.js:87:13)
gyp ERR! stack     at ChildProcess.emit (events.js:172:7)
gyp ERR! stack     at Process.ChildProcess._handle.onexit (internal/child_process.js:200:12)
gyp ERR! System Darwin 15.2.0
gyp ERR! command "/usr/local/bin/node" "/usr/local/lib/node_modules/npm/node_modules/node-gyp/bin/node-gyp.js" "rebuild"
gyp ERR! cwd /Volumes/Macintosh HD 2/OJH/node_modules/imagemagick-native
gyp ERR! node -v v5.4.1
gyp ERR! node-gyp -v v3.0.3
gyp ERR! not ok 
npm WARN ENOENT ENOENT: no such file or directory, open '/Volumes/Macintosh HD 2/OJH/package.json'
npm WARN EPACKAGEJSON OJH No description
npm WARN EPACKAGEJSON OJH No repository field.
npm WARN EPACKAGEJSON OJH No README data
npm WARN EPACKAGEJSON OJH No license field.
npm ERR! Darwin 15.2.0
npm ERR! argv "/usr/local/bin/node" "/usr/local/bin/npm" "install" "imagemagick-native"
npm ERR! node v5.4.1
npm ERR! npm  v3.3.12
npm ERR! code ELIFECYCLE

npm ERR! imagemagick-native@1.8.0 install: `node-gyp rebuild`
npm ERR! Exit status 1
npm ERR! 
npm ERR! Failed at the imagemagick-native@1.8.0 install script 'node-gyp rebuild'.
npm ERR! Make sure you have the latest version of node.js and npm installed.
npm ERR! If you do, this is most likely a problem with the imagemagick-native package,
npm ERR! not with npm itself.
npm ERR! Tell the author that this fails on your system:
npm ERR!     node-gyp rebuild
npm ERR! You can get their info via:
npm ERR!     npm owner ls imagemagick-native
npm ERR! There is likely additional logging output above.

npm ERR! Please include the following file with any support request:
npm ERR!     /Volumes/Macintosh HD 2/OJH/npm-debug.log
Danniss-Mac-mini:OJH dannis_kenny1115$ sudo port install pkgconfig
sudo: port: command not found
Danniss-Mac-mini:OJH dannis_kenny1115$ sudo port install pkgconfig
Password:
sudo: port: command not found
Danniss-Mac-mini:OJH dannis_kenny1115$ port
-bash: port: command not found
Danniss-Mac-mini:OJH dannis_kenny1115$ whereis port
Danniss-Mac-mini:OJH dannis_kenny1115$ clear

Danniss-Mac-mini:OJH dannis_kenny1115$ whereis port
Danniss-Mac-mini:OJH dannis_kenny1115$ man port
No manual entry for port
Danniss-Mac-mini:OJH dannis_kenny1115$ man portpwd
No manual entry for portpwd
Danniss-Mac-mini:OJH dannis_kenny1115$ pwd
/Volumes/Macintosh HD 2/OJH
Danniss-Mac-mini:OJH dannis_kenny1115$ cd /opt/
ImageMagick/ local/       
Danniss-Mac-mini:OJH dannis_kenny1115$ cd /opt/local/
bin/     etc/     include/ lib/     libexec/ man/     sbin/    share/   var/     
Danniss-Mac-mini:OJH dannis_kenny1115$ cd /opt/local/bin/
daemondo    port        port-tclsh  portf       portindex   portmirror  
Danniss-Mac-mini:OJH dannis_kenny1115$ cd /opt/local/bin
Danniss-Mac-mini:bin dannis_kenny1115$ ./port
MacPorts 2.3.4
Entering interactive mode... ("help" for help, "quit" to quit)
[local/bin] > quit
Goodbye
Danniss-Mac-mini:bin dannis_kenny1115$ clear



































Danniss-Mac-mini:bin dannis_kenny1115$ pwd
/opt/local/bin
Danniss-Mac-mini:bin dannis_kenny1115$ env | grep PATH
PATH=/usr/local/bin:/usr/bin:/bin:/usr/sbin:/sbin:/opt/ImageMagick/bin/
Danniss-Mac-mini:bin dannis_kenny1115$ export PATH=$PATH:/opt/local/bin/
Danniss-Mac-mini:bin dannis_kenny1115$ env | grep PATH
PATH=/usr/local/bin:/usr/bin:/bin:/usr/sbin:/sbin:/opt/ImageMagick/bin/:/opt/local/bin/
Danniss-Mac-mini:bin dannis_kenny1115$ port
MacPorts 2.3.4
Entering interactive mode... ("help" for help, "quit" to quit)
[local/bin] > quit
Goodbye
Danniss-Mac-mini:bin dannis_kenny1115$ clear










































Danniss-Mac-mini:bin dannis_kenny1115$ ls
daemondo	port		port-tclsh	portf		portindex	portmirror
Danniss-Mac-mini:bin dannis_kenny1115$ cd /Volumes/Macintosh\ HD\ 2/
Danniss-Mac-mini:Macintosh HD 2 dannis_kenny1115$ ls
OJH		Programs	Sandroworks	Source
Danniss-Mac-mini:Macintosh HD 2 dannis_kenny1115$ cd OJH
Danniss-Mac-mini:OJH dannis_kenny1115$ sudo npm install imagemagic-native
Password:
npm ERR! Darwin 15.2.0
npm ERR! argv "/usr/local/bin/node" "/usr/local/bin/npm" "install" "imagemagic-native"
npm ERR! node v5.4.1
npm ERR! npm  v3.3.12
npm ERR! code E404

npm ERR! 404 Registry returned 404 for GET on https://registry.npmjs.org/imagemagic-native
npm ERR! 404 
npm ERR! 404 'imagemagic-native' is not in the npm registry.
npm ERR! 404 You should bug the author to publish it (or use the name yourself!)
npm ERR! 404 
npm ERR! 404 Note that you can also install from a
npm ERR! 404 tarball, folder, http url, or git url.

npm ERR! Please include the following file with any support request:
npm ERR!     /Volumes/Macintosh HD 2/OJH/npm-debug.log
Danniss-Mac-mini:OJH dannis_kenny1115$ sudo npm install imagemagick-native

> imagemagick-native@1.8.0 install /Volumes/Macintosh HD 2/OJH/node_modules/imagemagick-native
> node-gyp rebuild

/opt/ImageMagick/bin/Magick++-config: line 53: pkg-config: command not found
/opt/ImageMagick/bin/Magick++-config: line 56: pkg-config: command not found
gyp: Call to 'Magick++-config --ldflags --libs' returned exit status 0. while trying to load binding.gyp
gyp ERR! configure error 
gyp ERR! stack Error: `gyp` failed with exit code: 1
gyp ERR! stack     at ChildProcess.onCpExit (/usr/local/lib/node_modules/npm/node_modules/node-gyp/lib/configure.js:355:16)
gyp ERR! stack     at emitTwo (events.js:87:13)
gyp ERR! stack     at ChildProcess.emit (events.js:172:7)
gyp ERR! stack     at Process.ChildProcess._handle.onexit (internal/child_process.js:200:12)
gyp ERR! System Darwin 15.2.0
gyp ERR! command "/usr/local/bin/node" "/usr/local/lib/node_modules/npm/node_modules/node-gyp/bin/node-gyp.js" "rebuild"
gyp ERR! cwd /Volumes/Macintosh HD 2/OJH/node_modules/imagemagick-native
gyp ERR! node -v v5.4.1
gyp ERR! node-gyp -v v3.0.3
gyp ERR! not ok 
npm WARN ENOENT ENOENT: no such file or directory, open '/Volumes/Macintosh HD 2/OJH/package.json'
npm WARN EPACKAGEJSON OJH No description
npm WARN EPACKAGEJSON OJH No repository field.
npm WARN EPACKAGEJSON OJH No README data
npm WARN EPACKAGEJSON OJH No license field.
npm ERR! Darwin 15.2.0
npm ERR! argv "/usr/local/bin/node" "/usr/local/bin/npm" "install" "imagemagick-native"
npm ERR! node v5.4.1
npm ERR! npm  v3.3.12
npm ERR! code ELIFECYCLE

npm ERR! imagemagick-native@1.8.0 install: `node-gyp rebuild`
npm ERR! Exit status 1
npm ERR! 
npm ERR! Failed at the imagemagick-native@1.8.0 install script 'node-gyp rebuild'.
npm ERR! Make sure you have the latest version of node.js and npm installed.
npm ERR! If you do, this is most likely a problem with the imagemagick-native package,
npm ERR! not with npm itself.
npm ERR! Tell the author that this fails on your system:
npm ERR!     node-gyp rebuild
npm ERR! You can get their info via:
npm ERR!     npm owner ls imagemagick-native
npm ERR! There is likely additional logging output above.

npm ERR! Please include the following file with any support request:
npm ERR!     /Volumes/Macintosh HD 2/OJH/npm-debug.log
Danniss-Mac-mini:OJH dannis_kenny1115$ sudo port install pkgconfig
Error: Port pkgconfig not found
Danniss-Mac-mini:OJH dannis_kenny1115$ sudo port install pkgconfig
Error: Port pkgconfig not found
Danniss-Mac-mini:OJH dannis_kenny1115$ sudo port -d install pkgconfig
DEBUG: Copying /Users/dannis_kenny1115/Library/Preferences/com.apple.dt.Xcode.plist to /opt/local/var/macports/home/Library/Preferences
Error: Port pkgconfig not found
Danniss-Mac-mini:OJH dannis_kenny1115$ cd /Downloads
-bash: cd: /Downloads: No such file or directory
Danniss-Mac-mini:OJH dannis_kenny1115$ ls
node_modules	npm-debug.log
Danniss-Mac-mini:OJH dannis_kenny1115$ cd ..
Danniss-Mac-mini:Macintosh HD 2 dannis_kenny1115$ cd ..
Danniss-Mac-mini:Volumes dannis_kenny1115$ cd ..
Danniss-Mac-mini:/ dannis_kenny1115$ ls
Applications			EFIROOTDIR			Users				dev				net				tmp
Clover_Install_Log.txt		Library				Volumes				etc				opt				usr
EFI				Network				bin				home				private				var
EFI-Backups			System				cores				installer.failurerequests	sbin
Danniss-Mac-mini:/ dannis_kenny1115$ cd ..
Danniss-Mac-mini:/ dannis_kenny1115$ ls
Applications			EFIROOTDIR			Users				dev				net				tmp
Clover_Install_Log.txt		Library				Volumes				etc				opt				usr
EFI				Network				bin				home				private				var
EFI-Backups			System				cores				installer.failurerequests	sbin
Danniss-Mac-mini:/ dannis_kenny1115$ cd Users
Danniss-Mac-mini:Users dannis_kenny1115$ ls
Deleted Users		Shared			dannis_kenny1115	mini			mini (Deleted)
Danniss-Mac-mini:Users dannis_kenny1115$ cd dannis_kenny1115
Danniss-Mac-mini:~ dannis_kenny1115$ ls
AndroidStudioProjects	Desktop			Downloads		Movies			Pictures		Public			podfile
Applications		Documents		Library			Music			Pods			genymotion-log.zip	true
Danniss-Mac-mini:~ dannis_kenny1115$ cd Downloads
Danniss-Mac-mini:Downloads dannis_kenny1115$ cd pkg-config-0.18.1
Danniss-Mac-mini:pkg-config-0.18.1 dannis_kenny1115$ ls
AUTHORS		INSTALL		NEWS		acconfig.h	config.guess	configure	findme.c	install-sh	missing		parse.h		pkg-config.1	pkg.m4		poptconfig.c	poptparse.c
COPYING		Makefile.am	README		aclocal.m4	config.h.in	configure.in	findme.h	ltmain.sh	mkinstalldirs	partial-glib.c	pkg.c		popt.c		popthelp.c
ChangeLog	Makefile.in	README.win32	check		config.sub	depcomp		glib-1.2.8	main.c		parse.c		partial-glib.h	pkg.h		popt.h		poptint.h
Danniss-Mac-mini:pkg-config-0.18.1 dannis_kenny1115$ ./configure --prefix=/usr
checking for a BSD-compatible install... /usr/bin/install -c
checking whether build environment is sane... yes
checking for gawk... no
checking for mawk... no
checking for nawk... no
checking for awk... awk
checking whether make sets $(MAKE)... yes
checking whether to enable maintainer-specific portions of Makefiles... no
checking build system type... i686-apple-darwin15.2.0
checking host system type... i686-apple-darwin15.2.0
checking for style of include used by make... GNU
checking for gcc... gcc
--with-inchecking for C compiler default output file name... ta.out
checking whether the C compiler works... yes
checking whether we are cross compiling... no
checking for suffix of executables... 
checking for suffix of object files... o
checking whether we are using the GNU C compiler... yes
checking whether gcc accepts -g... eryes
checking for gcc option to accept ANSI C... nalnone needed
checking dependency style of gcc... gcc3
checking for a sed that does not truncate output... /usr/bin/sed
checking for egrep... grep -E
checking for ld used by gcc... /Applications/Xcode.app/Contents/Developer/Toolchains/XcodeDefault.xctoolchain/usr/bin/ld
checking if the linker (/Applications/Xcode.app/Contents/Developer/Toolchains/XcodeDefault.xctoolchain/usr/bin/ld) is GNU ld... no
checking for /Applications/Xcode.app/Contents/Developer/Toolchains/XcodeDefault.xctoolchain/usr/bin/ld option to reload object files... -r
checking for BSD-compatible nm... /usr/bin/nm
checking whether ln -s works... yes
checking how to recognise dependent libraries... pass_all
checking how to run the C preprocessor... gcc -E
checking for ANSI C header files... yes
checking for sys/types.h... yes
checking for sys/stat.h... yes
checking for stdlib.h... yes
checking for string.h... yes
checking for memory.h... yes
checking for strings.h... yes
checking for inttypes.h... yes
checking for stdint.h... yes
checking for unistd.h... yes
checking dlfcn.h usability... yes
checking dlfcn.h presence... yes
checking for dlfcn.h... yes
checking for g++... g++
checking whether we are using the GNU C++ compiler... yes
checking whether g++ accepts -g... yes
rm: conftest.dSYM: is a directory
checking dependency style of g++... gcc3
checking how to run the C++ preprocessor... g++ -E
checking for g77... no
checking for f77... no
checking for xlf... no
checking for frt... no
checking for pgf77... no
checking for fort77... no
checking for fl32... no
checking for af77... no
checking for f90... no
checking for xlf90... no
checking for pgf90... no
checking for epcf90... no
checking for f95... no
checking for fort... no
checking for xlf95... no
checking for ifc... no
checking for efc... no
checking for pgf95... no
checking for lf95... no
checking for gfortran... no
checking whether we are using the GNU Fortran 77 compiler... no
checking whether  accepts -g... no
checking the maximum length of command line arguments... 65536
checking command to parse /usr/bin/nm output from gcc object... rm: conftest.dSYM: is a directory
rm: conftest.dSYM: is a directory
rm: conftest.dSYM: is a directory
rm: conftest.dSYM: is a directory
ok
checking for objdir... .libs
checking for ar... ar
checking for ranlib... ranlib
checking for strip... strip
checking if gcc static flag  works... rm: conftest.dSYM: is a directory
yes
checking if gcc supports -fno-rtti -fno-exceptions... rm: conftest.dSYM: is a directory
yes
checking for gcc option to produce PIC... -fno-common
checking if gcc PIC flag -fno-common works... rm: conftest.dSYM: is a directory
yes
checking if gcc supports -c -o file.o... rm: conftest.dSYM: is a directory
yes
checking whether the gcc linker (/Applications/Xcode.app/Contents/Developer/Toolchains/XcodeDefault.xctoolchain/usr/bin/ld) supports shared libraries... yes
checking dynamic linker characteristics... darwin15.2.0 dyld
checking how to hardcode library paths into programs... immediate
checking whether stripping libraries is possible... yes
checking if libtool supports shared libraries... yes
checking whether to build shared libraries... yes
checking whether to build static libraries... yes
configure: creating libtool
appending configuration tag "CXX" to libtool
checking for ld used by g++... /Applications/Xcode.app/Contents/Developer/Toolchains/XcodeDefault.xctoolchain/usr/bin/ld
checking if the linker (/Applications/Xcode.app/Contents/Developer/Toolchains/XcodeDefault.xctoolchain/usr/bin/ld) is GNU ld... no
checking whether the g++ linker (/Applications/Xcode.app/Contents/Developer/Toolchains/XcodeDefault.xctoolchain/usr/bin/ld) supports shared libraries... yes
checking for g++ option to produce PIC... -fno-common
checking if g++ PIC flag -fno-common works... rm: conftest.dSYM: is a directory
yes
checking if g++ supports -c -o file.o... rm: conftest.dSYM: is a directory
yes
checking whether the g++ linker (/Applications/Xcode.app/Contents/Developer/Toolchains/XcodeDefault.xctoolchain/usr/bin/ld) supports shared libraries... yes
checking dynamic linker characteristics... darwin15.2.0 dyld
checking how to hardcode library paths into programs... immediate
checking whether stripping libraries is possible... yes
appending configuration tag "F77" to libtool
checking for gcc... (cached) gcc
checking whether we are using the GNU C compiler... (cached) yes
checking whether gcc accepts -g... (cached) yes
checking for gcc option to accept ANSI C... (cached) none needed
checking dependency style of gcc... (cached) gcc3
checking whether to list both direct and indirect dependencies... no
checking for Win32... no
checking for working alloca.h... yes
checking for alloca... yes
checking for setresuid... no
checking for setreuid... yes
checking dirent.h usability... yes
checking dirent.h presence... yes
checking for dirent.h... yes
checking for unistd.h... (cached) yes
checking sys/wait.h usability... yes
checking sys/wait.h presence... yes
checking for sys/wait.h... yes
configure: creating ./config.status
config.status: creating Makefile
config.status: creating check/Makefile
config.status: creating config.h
config.status: executing depfiles commands
configure: configuring in glib-1.2.8
configure: running /bin/sh './configure' --prefix=/usr  '--prefix=/usr' --cache-file=/dev/null --srcdir=.
checking for a BSD-compatible install... /usr/bin/install -c
checking whether build environment is sane... yes
checking for gawk... no
checking for mawk... no
checking for nawk... no
checking for awk... awk
checking whether make sets $(MAKE)... yes
checking build system type... i686-apple-darwin15.2.0
checking host system type... i686-apple-darwin15.2.0
checking for style of include used by make... GNU
checking for gcc... gcc
checking for C compiler default output file name... a.out
checking whether the C compiler works... yes
checking whether we are cross compiling... no
checking for suffix of executables... 
checking for suffix of object files... o
checking whether we are using the GNU C compiler... yes
checking whether gcc accepts -g... yes
checking for gcc option to accept ANSI C... none needed
checking dependency style of gcc... gcc3
checking for a sed that does not truncate output... /usr/bin/sed
checking for egrep... grep -E
checking for ld used by gcc... /Applications/Xcode.app/Contents/Developer/Toolchains/XcodeDefault.xctoolchain/usr/bin/ld
checking if the linker (/Applications/Xcode.app/Contents/Developer/Toolchains/XcodeDefault.xctoolchain/usr/bin/ld) is GNU ld... no
checking for /Applications/Xcode.app/Contents/Developer/Toolchains/XcodeDefault.xctoolchain/usr/bin/ld option to reload object files... -r
checking for BSD-compatible nm... /usr/bin/nm
checking whether ln -s works... yes
checking how to recognise dependent libraries... pass_all
checking how to run the C preprocessor... gcc -E
checking for ANSI C header files... yes
checking for sys/types.h... yes
checking for sys/stat.h... yes
checking for stdlib.h... yes
checking for string.h... yes
checking for memory.h... yes
checking for strings.h... yes
checking for inttypes.h... yes
checking for stdint.h... yes
checking for unistd.h... yes
checking dlfcn.h usability... yes
checking dlfcn.h presence... yes
checking for dlfcn.h... yes
checking for g++... g++
checking whether we are using the GNU C++ compiler... yes
checking whether g++ accepts -g... yes
rm: conftest.dSYM: is a directory
checking dependency style of g++... gcc3
checking how to run the C++ preprocessor... g++ -E
checking for g77... no
checking for f77... no
checking for xlf... no
checking for frt... no
checking for pgf77... no
checking for fort77... no
checking for fl32... no
checking for af77... no
checking for f90... no
checking for xlf90... no
checking for pgf90... no
checking for epcf90... no
checking for f95... no
checking for fort... no
checking for xlf95... no
checking for ifc... no
checking for efc... no
checking for pgf95... no
checking for lf95... no
checking for gfortran... no
checking whether we are using the GNU Fortran 77 compiler... no
checking whether  accepts -g... no
checking the maximum length of command line arguments... 65536
checking command to parse /usr/bin/nm output from gcc object... rm: conftest.dSYM: is a directory
rm: conftest.dSYM: is a directory
rm: conftest.dSYM: is a directory
rm: conftest.dSYM: is a directory
ok
checking for objdir... .libs
checking for ar... ar
checking for ranlib... ranlib
checking for strip... strip
checking if gcc static flag  works... rm: conftest.dSYM: is a directory
yes
checking if gcc supports -fno-rtti -fno-exceptions... rm: conftest.dSYM: is a directory
yes
checking for gcc option to produce PIC... -fno-common
checking if gcc PIC flag -fno-common works... rm: conftest.dSYM: is a directory
yes
checking if gcc supports -c -o file.o... rm: conftest.dSYM: is a directory
yes
checking whether the gcc linker (/Applications/Xcode.app/Contents/Developer/Toolchains/XcodeDefault.xctoolchain/usr/bin/ld) supports shared libraries... yes
checking dynamic linker characteristics... darwin15.2.0 dyld
checking how to hardcode library paths into programs... immediate
checking whether stripping libraries is possible... yes
checking if libtool supports shared libraries... yes
checking whether to build shared libraries... yes
checking whether to build static libraries... yes
configure: creating libtool
appending configuration tag "CXX" to libtool
checking for ld used by g++... /Applications/Xcode.app/Contents/Developer/Toolchains/XcodeDefault.xctoolchain/usr/bin/ld
checking if the linker (/Applications/Xcode.app/Contents/Developer/Toolchains/XcodeDefault.xctoolchain/usr/bin/ld) is GNU ld... no
checking whether the g++ linker (/Applications/Xcode.app/Contents/Developer/Toolchains/XcodeDefault.xctoolchain/usr/bin/ld) supports shared libraries... yes
checking for g++ option to produce PIC... -fno-common
checking if g++ PIC flag -fno-common works... rm: conftest.dSYM: is a directory
yes
checking if g++ supports -c -o file.o... rm: conftest.dSYM: is a directory
yes
checking whether the g++ linker (/Applications/Xcode.app/Contents/Developer/Toolchains/XcodeDefault.xctoolchain/usr/bin/ld) supports shared libraries... yes
checking dynamic linker characteristics... darwin15.2.0 dyld
checking how to hardcode library paths into programs... immediate
checking whether stripping libraries is possible... yes
appending configuration tag "F77" to libtool
checking whether to enable maintainer-specific portions of Makefiles... no
checking whether to enable memory checking... no
checking whether to enable memory profiling... no
checking for gcc... (cached) gcc
checking whether we are using the GNU C compiler... (cached) yes
checking whether gcc accepts -g... (cached) yes
checking for gcc option to accept ANSI C... (cached) none needed
checking dependency style of gcc... (cached) gcc3
checking for gcc option to accept ANSI C... none needed
checking for a BSD-compatible install... /usr/bin/install -c
checking for extra flags to get ANSI library prototypes... none needed
checking for extra flags for POSIX compliance... none needed
checking for ANSI C header files... (cached) yes
checking for vprintf... yes
checking for _doprnt... no
checking for atexit... yes
checking for on_exit... no
checking for char... yes
checking size of char... 1
checking for short... yes
checking size of short... 2
checking for long... yes
checking size of long... 8
checking for int... yes
checking size of int... 4
checking for void *... yes
checking size of void *... 8
checking for long long... yes
checking size of long long... 8
checking for an ANSI C-conforming const... yes
checking for __inline... yes
checking for __inline__... yes
checking for inline... yes
checking whether byte ordering is bigendian... no
checking float.h usability... yes
checking float.h presence... yes
checking for float.h... yes
checking limits.h usability... yes
checking limits.h presence... yes
checking for limits.h... yes
checking pwd.h usability... yes
checking pwd.h presence... yes
checking for pwd.h... yes
checking sys/param.h usability... yes
checking sys/param.h presence... yes
checking for sys/param.h... yes
checking sys/poll.h usability... yes
checking sys/poll.h presence... yes
checking for sys/poll.h... yes
checking sys/select.h usability... yes
checking sys/select.h presence... yes
checking for sys/select.h... yes
checking sys/time.h usability... yes
checking sys/time.h presence... yes
checking for sys/time.h... yes
checking sys/times.h usability... yes
checking sys/times.h presence... yes
checking for sys/times.h... yes
checking for unistd.h... (cached) yes
checking values.h usability... no
checking values.h presence... no
checking for values.h... no
checking for lstat... yes
checking for strerror... yes
checking for strsignal... yes
checking for memmove... yes
checking for vsnprintf... yes
checking for strcasecmp... yes
checking for strncasecmp... yes
checking for poll... yes
checking for sys_errlist... yes
checking for sys_siglist... yes
checking for sys_siglist declaration... yes
checking for fd_set... yes, found in sys/types.h
checking for wchar.h... yes
checking for wctype.h... yes
checking for iswalnum... yes
checking if iswalnum() and friends are properly defined... no
checking whether realloc (NULL,) will work... yes
checking for an implementation of va_copy()... yes
checking for an implementation of __va_copy()... yes
checking whether va_lists can be copied by value... no
checking for dlopen... yes
checking for dlsym... yes
checking for preceeding underscore in symbols... no
checking for dlerror... yes
checking pthread.h usability... yes
checking pthread.h presence... yes
checking for pthread.h... yes
checking for thread implementation... posix
checking for pthread_attr_init in -lpthread... yes
checking necessary linker options... -lpthread
rm: conftest.dSYM: is a directory
checking necessary compiler options...  -D_REENTRANT
checking for localtime_r... yes
checking for rand_r... yes
checking for getpwuid_r... yes
checking whether getpwuid_r is posix like... yes
checking whether pthread_getspecific is posix like... yes
checking whether pthread_mutex_trylock is posix like... yes
checking whether pthread_cond_timedwait is posix like... yes
checking size of pthread_mutex_t... 64
checking byte contents of pthread_mutex_t... -89,-85,-86,50,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
checking system definitions for POLLIN POLLOUT POLLPRI POLLERR POLLHUP POLLNVAL... done
configure: creating ./config.status
config.status: creating glib.spec
config.status: creating Makefile
config.status: creating glib-config
config.status: creating gmodule/gmoduleconf.h
config.status: creating gmodule/Makefile
config.status: creating gthread/Makefile
config.status: creating docs/Makefile
config.status: creating docs/glib-config.1
config.status: creating tests/Makefile
config.status: creating config.h
config.status: executing depfiles commands
config.status: executing default-1 commands
creating glibconfig.h
config.status: executing default commands
Danniss-Mac-mini:pkg-config-0.18.1 dannis_kenny1115$ pkg
pkgbuild  pkgutil   
Danniss-Mac-mini:pkg-config-0.18.1 dannis_kenny1115$ pkg
pkgbuild  pkgutil   
Danniss-Mac-mini:pkg-config-0.18.1 dannis_kenny1115$ pkg
pkgbuild  pkgutil   
Danniss-Mac-mini:pkg-config-0.18.1 dannis_kenny1115$ sudo npm install imagemagick-native
Password:

> imagemagick-native@1.8.0 install /Users/dannis_kenny1115/Downloads/pkg-config-0.18.1/node_modules/imagemagick-native
> node-gyp rebuild

/opt/ImageMagick/bin/Magick++-config: line 53: pkg-config: command not found
/opt/ImageMagick/bin/Magick++-config: line 56: pkg-config: command not found
gyp: Call to 'Magick++-config --ldflags --libs' returned exit status 0. while trying to load binding.gyp
gyp ERR! configure error 
gyp ERR! stack Error: `gyp` failed with exit code: 1
gyp ERR! stack     at ChildProcess.onCpExit (/usr/local/lib/node_modules/npm/node_modules/node-gyp/lib/configure.js:355:16)
gyp ERR! stack     at emitTwo (events.js:87:13)
gyp ERR! stack     at ChildProcess.emit (events.js:172:7)
gyp ERR! stack     at Process.ChildProcess._handle.onexit (internal/child_process.js:200:12)
gyp ERR! System Darwin 15.2.0
gyp ERR! command "/usr/local/bin/node" "/usr/local/lib/node_modules/npm/node_modules/node-gyp/bin/node-gyp.js" "rebuild"
gyp ERR! cwd /Users/dannis_kenny1115/Downloads/pkg-config-0.18.1/node_modules/imagemagick-native
gyp ERR! node -v v5.4.1
gyp ERR! node-gyp -v v3.0.3
gyp ERR! not ok 
npm WARN ENOENT ENOENT: no such file or directory, open '/Users/dannis_kenny1115/Downloads/pkg-config-0.18.1/package.json'
npm WARN EPACKAGEJSON pkg-config-0.18.1 No description
npm WARN EPACKAGEJSON pkg-config-0.18.1 No repository field.
npm WARN EPACKAGEJSON pkg-config-0.18.1 No README data
npm WARN EPACKAGEJSON pkg-config-0.18.1 No license field.
npm ERR! Darwin 15.2.0
npm ERR! argv "/usr/local/bin/node" "/usr/local/bin/npm" "install" "imagemagick-native"
npm ERR! node v5.4.1
npm ERR! npm  v3.3.12
npm ERR! code ELIFECYCLE

npm ERR! imagemagick-native@1.8.0 install: `node-gyp rebuild`
npm ERR! Exit status 1
npm ERR! 
npm ERR! Failed at the imagemagick-native@1.8.0 install script 'node-gyp rebuild'.
npm ERR! Make sure you have the latest version of node.js and npm installed.
npm ERR! If you do, this is most likely a problem with the imagemagick-native package,
npm ERR! not with npm itself.
npm ERR! Tell the author that this fails on your system:
npm ERR!     node-gyp rebuild
npm ERR! You can get their info via:
npm ERR!     npm owner ls imagemagick-native
npm ERR! There is likely additional logging output above.

npm ERR! Please include the following file with any support request:
npm ERR!     /Users/dannis_kenny1115/Downloads/pkg-config-0.18.1/npm-debug.log
Danniss-Mac-mini:pkg-config-0.18.1 dannis_kenny1115$ pkg-config
-bash: pkg-config: command not found
Danniss-Mac-mini:pkg-config-0.18.1 dannis_kenny1115$ sudo port install pkgconfig
Password:
Error: Port pkgconfig not found
Danniss-Mac-mini:pkg-config-0.18.1 dannis_kenny1115$ clear

Danniss-Mac-mini:pkg-config-0.18.1 dannis_kenny1115$ 
Danniss-Mac-mini:pkg-config-0.18.1 dannis_kenny1115$ 
Danniss-Mac-mini:pkg-config-0.18.1 dannis_kenny1115$ pkg-config
Must specify package names on the command line
Danniss-Mac-mini:pkg-config-0.18.1 dannis_kenny1115$ pkg-config --version
0.29
Danniss-Mac-mini:pkg-config-0.18.1 dannis_kenny1115$ clear
















































Danniss-Mac-mini:pkg-config-0.18.1 dannis_kenny1115$ pwd
/Users/dannis_kenny1115/Downloads/pkg-config-0.18.1
Danniss-Mac-mini:pkg-config-0.18.1 dannis_kenny1115$ cd /Volumes/Macintosh\ HD\ 2/
Danniss-Mac-mini:Macintosh HD 2 dannis_kenny1115$ ls
OJH		Programs	Sandroworks	Source
Danniss-Mac-mini:Macintosh HD 2 dannis_kenny1115$ cd OJH/
Danniss-Mac-mini:OJH dannis_kenny1115$ ls
node_modules	npm-debug.log
Danniss-Mac-mini:OJH dannis_kenny1115$ clear














































Danniss-Mac-mini:OJH dannis_kenny1115$ ls -l
total 504
drwxr-xr-x  615 root  staff   20910 Jan 13 22:40 node_modules
-rw-r--r--    1 root  staff  257424 Jan 13 22:40 npm-debug.log
Danniss-Mac-mini:OJH dannis_kenny1115$ ls
node_modules	npm-debug.log
Danniss-Mac-mini:OJH dannis_kenny1115$ sudo npm install imagemagick-native
Password:

> imagemagick-native@1.8.0 install /Volumes/Macintosh HD 2/OJH/node_modules/imagemagick-native
> node-gyp rebuild

Package Magick++ was not found in the pkg-config search path.
Perhaps you should add the directory containing `Magick++.pc'
to the PKG_CONFIG_PATH environment variable
No package 'Magick++' found
Package Magick++ was not found in the pkg-config search path.
Perhaps you should add the directory containing `Magick++.pc'
to the PKG_CONFIG_PATH environment variable
No package 'Magick++' found
gyp: Call to 'Magick++-config --ldflags --libs' returned exit status 0. while trying to load binding.gyp
gyp ERR! configure error 
gyp ERR! stack Error: `gyp` failed with exit code: 1
gyp ERR! stack     at ChildProcess.onCpExit (/usr/local/lib/node_modules/npm/node_modules/node-gyp/lib/configure.js:355:16)
gyp ERR! stack     at emitTwo (events.js:87:13)
gyp ERR! stack     at ChildProcess.emit (events.js:172:7)
gyp ERR! stack     at Process.ChildProcess._handle.onexit (internal/child_process.js:200:12)
gyp ERR! System Darwin 15.2.0
gyp ERR! command "/usr/local/bin/node" "/usr/local/lib/node_modules/npm/node_modules/node-gyp/bin/node-gyp.js" "rebuild"
gyp ERR! cwd /Volumes/Macintosh HD 2/OJH/node_modules/imagemagick-native
gyp ERR! node -v v5.4.1
gyp ERR! node-gyp -v v3.0.3
gyp ERR! not ok 
npm WARN ENOENT ENOENT: no such file or directory, open '/Volumes/Macintosh HD 2/OJH/package.json'
npm WARN EPACKAGEJSON OJH No description
npm WARN EPACKAGEJSON OJH No repository field.
npm WARN EPACKAGEJSON OJH No README data
npm WARN EPACKAGEJSON OJH No license field.
npm ERR! Darwin 15.2.0
npm ERR! argv "/usr/local/bin/node" "/usr/local/bin/npm" "install" "imagemagick-native"
npm ERR! node v5.4.1
npm ERR! npm  v3.3.12
npm ERR! code ELIFECYCLE

npm ERR! imagemagick-native@1.8.0 install: `node-gyp rebuild`
npm ERR! Exit status 1
npm ERR! 
npm ERR! Failed at the imagemagick-native@1.8.0 install script 'node-gyp rebuild'.
npm ERR! Make sure you have the latest version of node.js and npm installed.
npm ERR! If you do, this is most likely a problem with the imagemagick-native package,
npm ERR! not with npm itself.
npm ERR! Tell the author that this fails on your system:
npm ERR!     node-gyp rebuild
npm ERR! You can get their info via:
npm ERR!     npm owner ls imagemagick-native
npm ERR! There is likely additional logging output above.

npm ERR! Please include the following file with any support request:
npm ERR!     /Volumes/Macintosh HD 2/OJH/npm-debug.log
Danniss-Mac-mini:OJH dannis_kenny1115$ env | grep
usage: grep [-abcDEFGHhIiJLlmnOoqRSsUVvwxZ] [-A num] [-B num] [-C[num]]
	[-e pattern] [-f file] [--binary-files=value] [--color=when]
	[--context[=num]] [--directories=action] [--label] [--line-buffered]
	[--null] [pattern] [file ...]
Danniss-Mac-mini:OJH dannis_kenny1115$ env
TERM_PROGRAM=Apple_Terminal
SHELL=/bin/bash
TERM=xterm-256color
TMPDIR=/var/folders/n9/pc_cwv390njf4ldpbnz84q2w0000gn/T/
Apple_PubSub_Socket_Render=/private/tmp/com.apple.launchd.7URhgRt1sX/Render
TERM_PROGRAM_VERSION=361.1
OLDPWD=/Volumes/Macintosh HD 2
TERM_SESSION_ID=99CE48F2-59AF-4E0F-855A-147FEC2993BF
USER=dannis_kenny1115
SSH_AUTH_SOCK=/private/tmp/com.apple.launchd.tw41OJu7cA/Listeners
__CF_USER_TEXT_ENCODING=0x1F5:0x0:0x0
PATH=/usr/local/bin:/usr/bin:/bin:/usr/sbin:/sbin:/opt/ImageMagick/bin/:/opt/local/bin/
PWD=/Volumes/Macintosh HD 2/OJH
LANG=en_US.UTF-8
XPC_FLAGS=0x0
XPC_SERVICE_NAME=0
SHLVL=1
HOME=/Users/dannis_kenny1115
LOGNAME=dannis_kenny1115
_=/usr/bin/env
Danniss-Mac-mini:OJH dannis_kenny1115$ export PKG_CONFIG_PATH=/opt/ImageMagick/lib/pkgconfig
Danniss-Mac-mini:OJH dannis_kenny1115$ env
TERM_PROGRAM=Apple_Terminal
SHELL=/bin/bash
TERM=xterm-256color
TMPDIR=/var/folders/n9/pc_cwv390njf4ldpbnz84q2w0000gn/T/
Apple_PubSub_Socket_Render=/private/tmp/com.apple.launchd.7URhgRt1sX/Render
TERM_PROGRAM_VERSION=361.1
OLDPWD=/Volumes/Macintosh HD 2
TERM_SESSION_ID=99CE48F2-59AF-4E0F-855A-147FEC2993BF
USER=dannis_kenny1115
SSH_AUTH_SOCK=/private/tmp/com.apple.launchd.tw41OJu7cA/Listeners
__CF_USER_TEXT_ENCODING=0x1F5:0x0:0x0
PATH=/usr/local/bin:/usr/bin:/bin:/usr/sbin:/sbin:/opt/ImageMagick/bin/:/opt/local/bin/
PWD=/Volumes/Macintosh HD 2/OJH
LANG=en_US.UTF-8
XPC_FLAGS=0x0
XPC_SERVICE_NAME=0
SHLVL=1
HOME=/Users/dannis_kenny1115
LOGNAME=dannis_kenny1115
PKG_CONFIG_PATH=/opt/ImageMagick/lib/pkgconfig
_=/usr/bin/env
Danniss-Mac-mini:OJH dannis_kenny1115$ sudo npm install imagemagick-native
Password:

> imagemagick-native@1.8.0 install /Volumes/Macintosh HD 2/OJH/node_modules/imagemagick-native
> node-gyp rebuild

Package Magick++ was not found in the pkg-config search path.
Perhaps you should add the directory containing `Magick++.pc'
to the PKG_CONFIG_PATH environment variable
No package 'Magick++' found
Package Magick++ was not found in the pkg-config search path.
Perhaps you should add the directory containing `Magick++.pc'
to the PKG_CONFIG_PATH environment variable
No package 'Magick++' found
gyp: Call to 'Magick++-config --ldflags --libs' returned exit status 0. while trying to load binding.gyp
gyp ERR! configure error 
gyp ERR! stack Error: `gyp` failed with exit code: 1
gyp ERR! stack     at ChildProcess.onCpExit (/usr/local/lib/node_modules/npm/node_modules/node-gyp/lib/configure.js:355:16)
gyp ERR! stack     at emitTwo (events.js:87:13)
gyp ERR! stack     at ChildProcess.emit (events.js:172:7)
gyp ERR! stack     at Process.ChildProcess._handle.onexit (internal/child_process.js:200:12)
gyp ERR! System Darwin 15.2.0
gyp ERR! command "/usr/local/bin/node" "/usr/local/lib/node_modules/npm/node_modules/node-gyp/bin/node-gyp.js" "rebuild"
gyp ERR! cwd /Volumes/Macintosh HD 2/OJH/node_modules/imagemagick-native
gyp ERR! node -v v5.4.1
gyp ERR! node-gyp -v v3.0.3
gyp ERR! not ok 
npm WARN ENOENT ENOENT: no such file or directory, open '/Volumes/Macintosh HD 2/OJH/package.json'
npm WARN EPACKAGEJSON OJH No description
npm WARN EPACKAGEJSON OJH No repository field.
npm WARN EPACKAGEJSON OJH No README data
npm WARN EPACKAGEJSON OJH No license field.
npm ERR! Darwin 15.2.0
npm ERR! argv "/usr/local/bin/node" "/usr/local/bin/npm" "install" "imagemagick-native"
npm ERR! node v5.4.1
npm ERR! npm  v3.3.12
npm ERR! code ELIFECYCLE

npm ERR! imagemagick-native@1.8.0 install: `node-gyp rebuild`
npm ERR! Exit status 1
npm ERR! 
npm ERR! Failed at the imagemagick-native@1.8.0 install script 'node-gyp rebuild'.
npm ERR! Make sure you have the latest version of node.js and npm installed.
npm ERR! If you do, this is most likely a problem with the imagemagick-native package,
npm ERR! not with npm itself.
npm ERR! Tell the author that this fails on your system:
npm ERR!     node-gyp rebuild
npm ERR! You can get their info via:
npm ERR!     npm owner ls imagemagick-native
npm ERR! There is likely additional logging output above.

npm ERR! Please include the following file with any support request:
npm ERR!     /Volumes/Macintosh HD 2/OJH/npm-debug.log
Danniss-Mac-mini:OJH dannis_kenny1115$ env
TERM_PROGRAM=Apple_Terminal
SHELL=/bin/bash
TERM=xterm-256color
TMPDIR=/var/folders/n9/pc_cwv390njf4ldpbnz84q2w0000gn/T/
Apple_PubSub_Socket_Render=/private/tmp/com.apple.launchd.7URhgRt1sX/Render
TERM_PROGRAM_VERSION=361.1
OLDPWD=/Volumes/Macintosh HD 2
TERM_SESSION_ID=99CE48F2-59AF-4E0F-855A-147FEC2993BF
USER=dannis_kenny1115
SSH_AUTH_SOCK=/private/tmp/com.apple.launchd.tw41OJu7cA/Listeners
__CF_USER_TEXT_ENCODING=0x1F5:0x0:0x0
PATH=/usr/local/bin:/usr/bin:/bin:/usr/sbin:/sbin:/opt/ImageMagick/bin/:/opt/local/bin/
PWD=/Volumes/Macintosh HD 2/OJH
LANG=en_US.UTF-8
XPC_FLAGS=0x0
XPC_SERVICE_NAME=0
SHLVL=1
HOME=/Users/dannis_kenny1115
LOGNAME=dannis_kenny1115
PKG_CONFIG_PATH=/opt/ImageMagick/lib/pkgconfig
_=/usr/bin/env
Danniss-Mac-mini:OJH dannis_kenny1115$ Magic++
-bash: Magic++: command not found
Danniss-Mac-mini:OJH dannis_kenny1115$ Magick++
-bash: Magick++: command not found
Danniss-Mac-mini:OJH dannis_kenny1115$ echo PKG_CONFIG_PATH
PKG_CONFIG_PATH
Danniss-Mac-mini:OJH dannis_kenny1115$ echo $PKG_CONFIG_PATH
/opt/ImageMagick/lib/pkgconfig
Danniss-Mac-mini:OJH dannis_kenny1115$ sudo npm install imagemagick-native
Password:
Sorry, try again.
Password:

> imagemagick-native@1.8.0 install /Volumes/Macintosh HD 2/OJH/node_modules/imagemagick-native
> node-gyp rebuild

Package Magick++ was not found in the pkg-config search path.
Perhaps you should add the directory containing `Magick++.pc'
to the PKG_CONFIG_PATH environment variable
No package 'Magick++' found
Package Magick++ was not found in the pkg-config search path.
Perhaps you should add the directory containing `Magick++.pc'
to the PKG_CONFIG_PATH environment variable
No package 'Magick++' found
gyp: Call to 'Magick++-config --ldflags --libs' returned exit status 0. while trying to load binding.gyp
gyp ERR! configure error 
gyp ERR! stack Error: `gyp` failed with exit code: 1
gyp ERR! stack     at ChildProcess.onCpExit (/usr/local/lib/node_modules/npm/node_modules/node-gyp/lib/configure.js:355:16)
gyp ERR! stack     at emitTwo (events.js:87:13)
gyp ERR! stack     at ChildProcess.emit (events.js:172:7)
gyp ERR! stack     at Process.ChildProcess._handle.onexit (internal/child_process.js:200:12)
gyp ERR! System Darwin 15.2.0
gyp ERR! command "/usr/local/bin/node" "/usr/local/lib/node_modules/npm/node_modules/node-gyp/bin/node-gyp.js" "rebuild"
gyp ERR! cwd /Volumes/Macintosh HD 2/OJH/node_modules/imagemagick-native
gyp ERR! node -v v5.4.1
gyp ERR! node-gyp -v v3.0.3
gyp ERR! not ok 
npm WARN ENOENT ENOENT: no such file or directory, open '/Volumes/Macintosh HD 2/OJH/package.json'
npm WARN EPACKAGEJSON OJH No description
npm WARN EPACKAGEJSON OJH No repository field.
npm WARN EPACKAGEJSON OJH No README data
npm WARN EPACKAGEJSON OJH No license field.
npm ERR! Darwin 15.2.0
npm ERR! argv "/usr/local/bin/node" "/usr/local/bin/npm" "install" "imagemagick-native"
npm ERR! node v5.4.1
npm ERR! npm  v3.3.12
npm ERR! code ELIFECYCLE

npm ERR! imagemagick-native@1.8.0 install: `node-gyp rebuild`
npm ERR! Exit status 1
npm ERR! 
npm ERR! Failed at the imagemagick-native@1.8.0 install script 'node-gyp rebuild'.
npm ERR! Make sure you have the latest version of node.js and npm installed.
npm ERR! If you do, this is most likely a problem with the imagemagick-native package,
npm ERR! not with npm itself.
npm ERR! Tell the author that this fails on your system:
npm ERR!     node-gyp rebuild
npm ERR! You can get their info via:
npm ERR!     npm owner ls imagemagick-native
npm ERR! There is likely additional logging output above.

npm ERR! Please include the following file with any support request:
npm ERR!     /Volumes/Macintosh HD 2/OJH/npm-debug.log
Danniss-Mac-mini:OJH dannis_kenny1115$ PKG_CONFIG_PATH='pwd'/opt/ImageMagick/lib/pkgconfig
Danniss-Mac-mini:OJH dannis_kenny1115$ export PKG_CONFIG_PATH
Danniss-Mac-mini:OJH dannis_kenny1115$ echo $PKG_CONFIG_PATH
pwd/opt/ImageMagick/lib/pkgconfig
Danniss-Mac-mini:OJH dannis_kenny1115$ sudo npm install imagemagick-native
Password:

> imagemagick-native@1.8.0 install /Volumes/Macintosh HD 2/OJH/node_modules/imagemagick-native
> node-gyp rebuild

Package Magick++ was not found in the pkg-config search path.
Perhaps you should add the directory containing `Magick++.pc'
to the PKG_CONFIG_PATH environment variable
No package 'Magick++' found
Package Magick++ was not found in the pkg-config search path.
Perhaps you should add the directory containing `Magick++.pc'
to the PKG_CONFIG_PATH environment variable
No package 'Magick++' found
gyp: Call to 'Magick++-config --ldflags --libs' returned exit status 0. while trying to load binding.gyp
gyp ERR! configure error 
gyp ERR! stack Error: `gyp` failed with exit code: 1
gyp ERR! stack     at ChildProcess.onCpExit (/usr/local/lib/node_modules/npm/node_modules/node-gyp/lib/configure.js:355:16)
gyp ERR! stack     at emitTwo (events.js:87:13)
gyp ERR! stack     at ChildProcess.emit (events.js:172:7)
gyp ERR! stack     at Process.ChildProcess._handle.onexit (internal/child_process.js:200:12)
gyp ERR! System Darwin 15.2.0
gyp ERR! command "/usr/local/bin/node" "/usr/local/lib/node_modules/npm/node_modules/node-gyp/bin/node-gyp.js" "rebuild"
gyp ERR! cwd /Volumes/Macintosh HD 2/OJH/node_modules/imagemagick-native
gyp ERR! node -v v5.4.1
gyp ERR! node-gyp -v v3.0.3
gyp ERR! not ok 
npm WARN ENOENT ENOENT: no such file or directory, open '/Volumes/Macintosh HD 2/OJH/package.json'
npm WARN EPACKAGEJSON OJH No description
npm WARN EPACKAGEJSON OJH No repository field.
npm WARN EPACKAGEJSON OJH No README data
npm WARN EPACKAGEJSON OJH No license field.
npm ERR! Darwin 15.2.0
npm ERR! argv "/usr/local/bin/node" "/usr/local/bin/npm" "install" "imagemagick-native"
npm ERR! node v5.4.1
npm ERR! npm  v3.3.12
npm ERR! code ELIFECYCLE

npm ERR! imagemagick-native@1.8.0 install: `node-gyp rebuild`
npm ERR! Exit status 1
npm ERR! 
npm ERR! Failed at the imagemagick-native@1.8.0 install script 'node-gyp rebuild'.
npm ERR! Make sure you have the latest version of node.js and npm installed.
npm ERR! If you do, this is most likely a problem with the imagemagick-native package,
npm ERR! not with npm itself.
npm ERR! Tell the author that this fails on your system:
npm ERR!     node-gyp rebuild
npm ERR! You can get their info via:
npm ERR!     npm owner ls imagemagick-native
npm ERR! There is likely additional logging output above.

npm ERR! Please include the following file with any support request:
npm ERR!     /Volumes/Macintosh HD 2/OJH/npm-debug.log
Danniss-Mac-mini:OJH dannis_kenny1115$ locate Magick++.pc

WARNING: The locate database (/var/db/locate.database) does not exist.
To create the database, run the following command:

  sudo launchctl load -w /System/Library/LaunchDaemons/com.apple.locate.plist

Please be aware that the database can take some time to generate; once
the database has been created, this message will no longer appear.

Danniss-Mac-mini:OJH dannis_kenny1115$ cd ..
Danniss-Mac-mini:Macintosh HD 2 dannis_kenny1115$ pwd
/Volumes/Macintosh HD 2
Danniss-Mac-mini:Macintosh HD 2 dannis_kenny1115$ ls
OJH		Programs	Sandroworks	Source
Danniss-Mac-mini:Macintosh HD 2 dannis_kenny1115$ 
Danniss-Mac-mini:Macintosh HD 2 dannis_kenny1115$ 
Danniss-Mac-mini:Macintosh HD 2 dannis_kenny1115$ cd ..
Danniss-Mac-mini:Volumes dannis_kenny1115$ 
Danniss-Mac-mini:Volumes dannis_kenny1115$ cd ..
Danniss-Mac-mini:/ dannis_kenny1115$ cd ..
Danniss-Mac-mini:/ dannis_kenny1115$ cd ..
Danniss-Mac-mini:/ dannis_kenny1115$ ls
Applications			EFIROOTDIR			Users				dev				net				tmp
Clover_Install_Log.txt		Library				Volumes				etc				opt				usr
EFI				Network				bin				home				private				var
EFI-Backups			System				cores				installer.failurerequests	sbin
Danniss-Mac-mini:/ dannis_kenny1115$ localte Magick++.pc
-bash: localte: command not found
Danniss-Mac-mini:/ dannis_kenny1115$ locate Magick++.pc

WARNING: The locate database (/var/db/locate.database) does not exist.
To create the database, run the following command:

  sudo launchctl load -w /System/Library/LaunchDaemons/com.apple.locate.plist

Please be aware that the database can take some time to generate; once
the database has been created, this message will no longer appear.

Danniss-Mac-mini:/ dannis_kenny1115$ npm install imagemagick-native
npm WARN checkPermissions Missing write access to /
/
└─┬ imagemagick-native@1.8.0 
  ├── nan@1.7.0 
  └─┬ readable-stream@2.0.5 
    ├── core-util-is@1.0.2 
    ├── inherits@2.0.1 
    ├── isarray@0.0.1 
    ├── process-nextick-args@1.0.6 
    ├── string_decoder@0.10.31 
    └── util-deprecate@1.0.2 

npm WARN ENOENT ENOENT: no such file or directory, open '/package.json'
npm WARN EPACKAGEJSON !invalid#1 No description
npm WARN EPACKAGEJSON !invalid#1 No repository field.
npm WARN EPACKAGEJSON !invalid#1 No README data
npm WARN EPACKAGEJSON !invalid#1 No license field.
npm ERR! Darwin 15.2.0
npm ERR! argv "/usr/local/bin/node" "/usr/local/bin/npm" "install" "imagemagick-native"
npm ERR! node v5.4.1
npm ERR! npm  v3.3.12
npm ERR! path /
npm ERR! code EACCES
npm ERR! errno -13
npm ERR! syscall access

npm ERR! Error: EACCES: permission denied, access '/'
npm ERR!     at Error (native)
npm ERR!  { [Error: EACCES: permission denied, access '/'] errno: -13, code: 'EACCES', syscall: 'access', path: '/' }
npm ERR! 
npm ERR! Please try running this command again as root/Administrator.
npm ERR! Darwin 15.2.0
npm ERR! argv "/usr/local/bin/node" "/usr/local/bin/npm" "install" "imagemagick-native"
npm ERR! node v5.4.1
npm ERR! npm  v3.3.12
npm ERR! path npm-debug.log.6fa17377ff67a0af90a7885d9f50ff06
npm ERR! code EACCES
npm ERR! errno -13
npm ERR! syscall open

npm ERR! Error: EACCES: permission denied, open 'npm-debug.log.6fa17377ff67a0af90a7885d9f50ff06'
npm ERR!     at Error (native)
npm ERR!  { [Error: EACCES: permission denied, open 'npm-debug.log.6fa17377ff67a0af90a7885d9f50ff06']
npm ERR!   errno: -13,
npm ERR!   code: 'EACCES',
npm ERR!   syscall: 'open',
npm ERR!   path: 'npm-debug.log.6fa17377ff67a0af90a7885d9f50ff06' }
npm ERR! 
npm ERR! Please try running this command again as root/Administrator.

npm ERR! Please include the following file with any support request:
npm ERR!     /npm-debug.log
Danniss-Mac-mini:/ dannis_kenny1115$ ls
Applications			EFIROOTDIR			Users				dev				net				tmp
Clover_Install_Log.txt		Library				Volumes				etc				opt				usr
EFI				Network				bin				home				private				var
EFI-Backups			System				cores				installer.failurerequests	sbin
Danniss-Mac-mini:/ dannis_kenny1115$ cd ..
Danniss-Mac-mini:/ dannis_kenny1115$ ls
Applications			EFIROOTDIR			Users				dev				net				tmp
Clover_Install_Log.txt		Library				Volumes				etc				opt				usr
EFI				Network				bin				home				private				var
EFI-Backups			System				cores				installer.failurerequests	sbin
Danniss-Mac-mini:/ dannis_kenny1115$ cd ..
Danniss-Mac-mini:/ dannis_kenny1115$ cd ..
Danniss-Mac-mini:/ dannis_kenny1115$ ls
Applications			EFIROOTDIR			Users				dev				net				tmp
Clover_Install_Log.txt		Library				Volumes				etc				opt				usr
EFI				Network				bin				home				private				var
EFI-Backups			System				cores				installer.failurerequests	sbin
Danniss-Mac-mini:/ dannis_kenny1115$ USers
dannis_kenny1115
Danniss-Mac-mini:/ dannis_kenny1115$ cd Users
Danniss-Mac-mini:Users dannis_kenny1115$ ls
Deleted Users		Shared			dannis_kenny1115	mini			mini (Deleted)
Danniss-Mac-mini:Users dannis_kenny1115$ cd dannis_kenny1115
Danniss-Mac-mini:~ dannis_kenny1115$ ls
AndroidStudioProjects	Desktop			Downloads		Movies			Pictures		Public			podfile
Applications		Documents		Library			Music			Pods			genymotion-log.zip	true
Danniss-Mac-mini:~ dannis_kenny1115$ cd Downloads
Danniss-Mac-mini:Downloads dannis_kenny1115$ cd app
Danniss-Mac-mini:app dannis_kenny1115$ ls
Dockerfile		Procfile		app			config			generate-ssl-certs.sh	karma.conf.js		public			var
LICENSE.md		README.md		bower.json		fig.yml			gruntfile.js		package.json		server.js
Danniss-Mac-mini:app dannis_kenny1115$ node
> 
(To exit, press ^C again or type .exit)
> 
Danniss-Mac-mini:app dannis_kenny1115$ node servr.js
module.js:327
    throw err;
    ^

Error: Cannot find module '/Users/dannis_kenny1115/Downloads/app/servr.js'
    at Function.Module._resolveFilename (module.js:325:15)
    at Function.Module._load (module.js:276:25)
    at Function.Module.runMain (module.js:429:10)
    at startup (node.js:139:18)
    at node.js:999:3
Danniss-Mac-mini:app dannis_kenny1115$ grunt
grunt-cli: The grunt command line interface. (v0.1.13)

Fatal error: Unable to find local grunt.

If you're seeing this message, either a Gruntfile wasn't found or grunt
hasn't been installed locally to your project. For more information about
installing and configuring grunt, please see the Getting Started guide:

http://gruntjs.com/getting-started
Danniss-Mac-mini:app dannis_kenny1115$ npm install grunt
npm WARN deprecated lodash@0.9.2: lodash@<2.0.0 is no longer maintained. Upgrade to lodash@^3.0.0
Portal-Site@0.0.3 /Users/dannis_kenny1115/Downloads/app
└── grunt@0.4.5  extraneous

npm WARN EPACKAGEJSON Portal-Site@0.0.3 No repository field.
npm WARN EPACKAGEJSON Portal-Site@0.0.3 No license field.
Danniss-Mac-mini:app dannis_kenny1115$ grunt
Loading "gruntfile.js" tasks...ERROR
>> Error: Cannot find module 'promise'
Warning: Task "default" not found. Use --force to continue.

Aborted due to warnings.
Danniss-Mac-mini:app dannis_kenny1115$ grunt
Running "jshint:all" (jshint) task


     33 |                if ($scope.about.author != null) {
                                                   ^ Expected '!==' and instead saw '!='.
    101 |            if (about.author != null) {
                                        ^ Expected '!==' and instead saw '!='.

     33 |                if ($scope.post.author != null) {
                                                  ^ Expected '!==' and instead saw '!='.
    104 |            if (post.author != null) {
                                       ^ Expected '!==' and instead saw '!='.

     74 |                if ($scope.book.author != null) $scope.book.author = $scope.book.author.value;
                                                  ^ Expected '!==' and instead saw '!='.
    193 |                        }).error(function (data, status) {
                                  ^ Don't make functions within a loop.
    195 |                        });
                                  ^ Don't make functions within a loop.
    261 |                        }).error(function (data, status) {
                                  ^ Don't make functions within a loop.
    263 |                        });
                                  ^ Don't make functions within a loop.

    357 |            if (video.author != null) {
                                        ^ Expected '!==' and instead saw '!='.

>> 10 errors in 145 files
Warning: Task "jshint:all" failed. Used --force, continuing.

Running "csslint:all" (csslint) task
>> 9 files lint free.

Running "concurrent:default" (concurrent) task
    Running "watch" task
    Waiting...
    Running "nodemon:dev" (nodemon) task
    [nodemon] 1.8.1
    [nodemon] to restart at any time, enter `rs`
    [nodemon] watching: app/views/**/*.* gruntfile.js server.js config/**/*.js app/**/*.js
    [nodemon] starting `node --debug server.js`
    Debugger listening on port 5858
    /Users/dannis_kenny1115/Downloads/app/node_modules/glob/glob.js:70
          throw new TypeError('callback provided to sync glob')
          ^
    
    TypeError: callback provided to sync glob
        at glob (/Users/dannis_kenny1115/Downloads/app/node_modules/glob/glob.js:70:13)
        at module.exports (/Users/dannis_kenny1115/Downloads/app/config/init.js:17:2)
        at Object.<anonymous> (/Users/dannis_kenny1115/Downloads/app/server.js:5:36)
        at Module._compile (module.js:397:26)
        at Object.Module._extensions..js (module.js:404:10)
        at Module.load (module.js:343:32)
        at Function.Module._load (module.js:300:12)
        at Function.Module.runMain (module.js:429:10)
        at startup (node.js:139:18)
        at node.js:999:3
    [nodemon] app crashed - waiting for file changes before starting...
^A^A    >> File "untitled folder" added.
    Running "jshint:all" (jshint) task
    
    
         33 |                if ($scope.about.author != null) {
                                                       ^ Expected '!==' and instead saw '!='.
        101 |            if (about.author != null) {
                                            ^ Expected '!==' and instead saw '!='.
    
         33 |                if ($scope.post.author != null) {
                                                      ^ Expected '!==' and instead saw '!='.
        104 |            if (post.author != null) {
                                           ^ Expected '!==' and instead saw '!='.
    
         74 |                if ($scope.book.author != null) $scope.book.author = $scope.book.author.value;
                                                      ^ Expected '!==' and instead saw '!='.
        193 |                        }).error(function (data, status) {
                                      ^ Don't make functions within a loop.
        195 |                        });
                                      ^ Don't make functions within a loop.
        261 |                        }).error(function (data, status) {
                                      ^ Don't make functions within a loop.
        263 |                        });
                                      ^ Don't make functions within a loop.
    
        357 |            if (video.author != null) {
                                            ^ Expected '!==' and instead saw '!='.
    
    >> 10 errors in 145 files
    Warning: Task "jshint:all" failed. Used --force, continuing.
    
    Done, but with warnings.
    Completed in 12.288s at Thu Jan 14 2016 03:02:34 GMT+0100 (CET) - Waiting...

